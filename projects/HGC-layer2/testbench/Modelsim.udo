if { ! [batch_mode] } {
  delete wave *
  config wave -signalnamewidth 1
  noview structure
  noview signals
  noview sim
  noview "Memory List"

  force sim:/debugging/TimeStamp [clock format [clock second]]
  
  add wave -dec sim:/debugging/SimulationClockCounter
  add wave -divider

  # add wave -dec sim:/top/HgcProcessorInstance/linksIn
  # add wave -dec sim:/top/HgcProcessorInstance/InputPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/DistributedPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/WeightedPositionPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/LocalCellPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramCellPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/SmearedCell1dPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/NormalizedCell1dPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/SmearedCell2dPipe(0)
# #  add wave -dec sim:/top/HgcProcessorInstance/Maxima1dPipe(0)
# #  add wave -dec sim:/top/HgcProcessorInstance/Maxima2dPipe(0)
# #  add wave -dec sim:/top/HgcProcessorInstance/MaximaInterleavedPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/SimpleMaximaPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaCentrePipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/IndexedMaximaPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/DoiPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/ClusteredLocalCellPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterPipe(0)
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterSumPipe(0)
  # add wave -divider

 
  # add wave -divider "Cell Distribution"
  # add wave -position end sim:/top/HgcProcessorInstance/TriggerCellDistributionInstance/g2(0)/g2a(1)/TriggerCellDistributionServerInstance/*
  
 
  # add wave -divider "Histogramming"
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/clk
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/CellPipeIn(0)
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/phases
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/read_phase
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/AddressPipe0(0)
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/RamOutput0
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/AddressPipe1(0)
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/RamOutput1
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/read_phase_del
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/Updated
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/write_phase
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/RamInput0
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/AddressPipe0(3)
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/WriteEnable0
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/RamInput1
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/AddressPipe1(3)
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/WriteEnable1
  # add wave -divider
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/ReadOutIndex
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/g1(0)/ReadOut
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/Output
  # add wave -dec sim:/top/HgcProcessorInstance/HistogramInstance/Debug  


  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel2DInstance/CellPipeIn
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel2DInstance/*
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel2DInstance/g1(0)/* 
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel2DInstance/Output
  
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/CellsIn(46) 
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/SumHalfLeft(46) 
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/SumHalfRight(46) 
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/LatchedLeft(46) 
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/LatchedRight(46) 

  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/g1(46)/SumWidth 
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/g1(46)/LeftInput
  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/g1(46)/RightInput 

  # add wave -dec sim:/top/HgcProcessorInstance/ExponentialSmearingKernel1DInstance/Output(46)
  
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/CellsIn(65).DataValid
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/CellsIn(65).SortKey
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/CellsIn(65).Accumulator
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/MaximaLeft(65).Accumulator
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/MaximaRight(65).Accumulator
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/LatchedLeft(65).Accumulator
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/LatchedRight(65).Accumulator

  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/Output(65).DataValid
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/Output(65).SortKey
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/Output(65).Accumulator

  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/g1(65)/MaximaWidth
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/g1(65)/LeftInput.Accumulator
  # add wave -dec sim:/top/HgcProcessorInstance/MaximaFinder1DInstance/g1(65)/RightInput.Accumulator

  # add wave -dec sim:/top/HgcProcessorInstance/DomainOfInfluence2DInstance/CellPipeIn(0)
  # add wave -dec sim:/top/HgcProcessorInstance/DomainOfInfluence2DInstance/LatchedLeft1
  # add wave -dec sim:/top/HgcProcessorInstance/DomainOfInfluence2DInstance/LatchedRight1
  # add wave -dec sim:/top/HgcProcessorInstance/DomainOfInfluence2DInstance/CompareLpipe
  # add wave -dec sim:/top/HgcProcessorInstance/DomainOfInfluence2DInstance/CompareHpipe
  # add wave -dec sim:/top/HgcProcessorInstance/DomainOfInfluence2DInstance/Output1

  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/LocalCellPipeIn(0)
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/DoiPipeIn(0)
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/*
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/g1(59)/*
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/DoiRamPipe
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/TcRamPipe
  # add wave -dec sim:/top/HgcProcessorInstance/ClusterizerInstance/LocalCellPipeOut(0)  


  add wave -dec sim:/top/HgcProcessorInstance/ClusterSumInstance/ClusterPipeIn(0)
  add wave -dec sim:/top/HgcProcessorInstance/ClusterSumInstance/g1(59)/*  
  add wave -dec sim:/top/HgcProcessorInstance/ClusterSumInstance/ClustersOut
  
}