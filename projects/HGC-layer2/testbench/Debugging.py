import copy
import numpy as np
import matplotlib.pyplot as plt

class TriggerCell:
  def __init__(self):
    self.Clock = 0
    self.Index = 0
    self.R_over_Z = 0
    self.Layer = 0
    self.Energy = 0
    self.Phi = 0
    self.SortKey = 0
    self.FrameValid = True
    self.DataValid = True

  @classmethod    
  def FromInt( cls , clk , i , j ):
    lCell = cls()
    lCell.Clock = (4*j)+clk
    lCell.Index = i
    lCell.SortKey = j
    return lCell
    
  @classmethod    
  def FromString( cls , data ):
    data = data.split()
    lCell = cls()
    lCell.Clock = int( data[0] )
    lCell.Index = int( data[1] )
    lCell.R_over_Z = int( data[2] )
    lCell.Layer = int( data[3] )
    lCell.Energy = int( data[4] )
    lCell.Phi = int( data[5] )
    lCell.SortKey = int( data[6] )
    lCell.FrameValid = ( data[7] == "TRUE" )
    lCell.DataValid = ( data[8] == "TRUE" )
    return lCell
    
  def __eq__( self , other ):
    return ( self.Index == other.Index ) \
    and ( self.R_over_Z == other.R_over_Z ) \
    and ( self.Layer == other.Layer ) \
    and ( self.Energy == other.Energy ) \
    and ( self.Phi == other.Phi ) \
    and ( self.SortKey == other.SortKey ) \
    and ( self.FrameValid == other.FrameValid ) \
    and ( self.DataValid == other.DataValid ) #\
    #and ( self.Clock == other.Clock ) 
    
  def __str__(self):
    return "".join( [ "{0:11}".format(x) for x in [ self.Clock , self.Index , self.R_over_Z , self.Layer , self.Energy , self.Phi , self.SortKey , self.FrameValid , self.DataValid ] ] )
    
# ---------------------------------------------------------------------------------------

class HistogramCell:
  def __init__(self):
    self.Clock = 0
    self.Index = 0
    self.S = 0
    self.X = 0
    self.Y = 0
    self.N = 0
    self.SortKey = 0
    # self.MaximaOffset = 0
    self.FrameValid = True
    self.DataValid = True

  @classmethod    
  def FromInt( cls , clk , i , j ):
    lCell = cls()
    lCell.Clock = (4*j)+clk
    lCell.Index = i
    lCell.SortKey = j
    return lCell
    
  @classmethod    
  def FromString( cls , data ):
    data = data.split()
    lCell = cls()
    lCell.Clock = int( data[0] )
    lCell.Index = int( data[1] )
    lCell.S = int( data[2] )
    lCell.X = int( data[3] )
    lCell.Y = int( data[4] )
    lCell.N = int( data[5] )
    lCell.SortKey = int( data[6] )
    # lCell.MaximaOffset = int( data[7] )
    lCell.FrameValid = ( data[7] == "TRUE" )
    lCell.DataValid = ( data[8] == "TRUE" )
    return lCell

  def __add__( self , other ):
    self.S = int( self.S + other.S )
    self.X = int( self.X + other.X )
    self.Y = int( self.Y + other.Y )
    self.N = int( self.N + other.N )  
    return self

  def __sub__( self , other ):
    self.S -= other.S
    self.X -= other.X
    self.Y -= other.Y
    self.N -= other.N  
    return self

  def __truediv__( self , other ):
    ret = copy.deepcopy( self )
  
    ret.S = self.S // other 
    ret.X = 0 # self.X // other 
    ret.Y = 0 # self.Y // other 
    ret.N = 0 # self.N // other 
    return ret

  def __mul__( self , other ):
    self.S = int( self.S * other // 262144 )
    # self.X = int( self.X * other // 262144 )
    # self.Y = int( self.Y * other // 262144 )
    return self
    
  def __eq__( self , other ):
    return ( self.Index == other.Index ) \
    and ( self.S == other.S ) \
    and ( self.X == other.X ) \
    and ( self.Y == other.Y ) \
    and ( self.N == other.N ) \
    and ( self.SortKey == other.SortKey ) \
    and ( self.FrameValid == other.FrameValid ) \
    and ( self.DataValid == other.DataValid ) #\
    #and ( self.Clock == other.Clock ) 
    # and ( ( self.MaximaOffset == other.MaximaOffset ) or ( self.MaximaOffset < -4 ) ) \
    
  def __str__(self):
    return "".join( [ "{0:11}".format(x) for x in [ self.Clock , self.Index , self.S , self.X , self.Y , self.N , self.SortKey , self.FrameValid , self.DataValid ] ] )  #, self.MaximaOffset


    
# ---------------------------------------------------------------------------------------
def ReadHistogramCells( filename ):
  with open( filename , "r" ) as f:
    File = f.readline()  
    Headers = f.readline().split()   
    return [ HistogramCell.FromString( l ) for l in f.readlines() ]

def ReadTriggerCells( filename ):
  with open( filename , "r" ) as f:
    File = f.readline()  
    Headers = f.readline().split()   
    return [ TriggerCell.FromString( l ) for l in f.readlines() ]    
    
def Compare( Name , Data, Expected ):
  print( Name , ":", end=' ')
  retval = True
  # print()
  for d , e in zip( Data , Expected ):
    # print ( d , e , "-----------" )
    if not ( d == e ):
      retval = False
      print( "Data Mismatch" )
      print( "Data    ", d )
      print( "Expected", e )
  if retval:
    print( "Match" )
  return retval


#===============================================================
def CreateSubPlots( val ):
  # If you can make a fully-occupied array with an aspect-ratio of less than 2:1 then do that
  # Otherwise, make the squarest array possible that will hold the requested value
  div1 = int( np.sqrt( val ) )
  while( val % div1 ) : div1 -= 1
  div2 = int(val/div1)
  
  if ( div2 > (2*div1) ):
    div1 = np.floor( np.sqrt( val ) )
    div2 = np.ceil( val/div1 )
    
  return plt.subplots( int(div1) , int(div2) )
#===============================================================
  

  
fig, axs = CreateSubPlots( 20 )
ax = iter( axs.ravel() )

   
def HistogramCellPlot( Data , title , ax ):
  lPlot = [ [ 0 for i in range( 72 ) ] for j in range( 40 ) ]
  for i in Data: lPlot [ i.SortKey ][ i.Index ] = i.S
  ax.imshow( lPlot , cmap=plt.get_cmap('Blues') , vmin=0 , origin='lower', aspect="auto" )
  ax.set_title( title )  
  ax.set_xlabel( "Channel" )
  ax.set_ylabel( "SortKey" )
  
  
def ScatterPlot( ax , Data, title , range_x , range_y , x , y , z = None ):
  X = [ getattr( i , x ) for i in Data ]
  Y = [ getattr( i , y ) for i in Data ]  
  if z:
    Z = [ getattr( i , z ) for i in Data ]  
    ax.scatter( X , Y , c = Z , cmap=plt.get_cmap('Blues') , marker='.')
  else:
    ax.scatter( X , Y , marker='.')
  ax.set_title( title )  
  ax.set_xlabel( x )
  ax.set_xlim( *range_x )
  ax.set_ylabel( y )
  ax.set_ylim( *range_y )
  
# ---------------------------------------------------------------------------------------







# ---------------------------------------------------------------------------------------
LinksToTriggerCell = ReadTriggerCells( "DebugFiles/LinksToTriggerCell.txt" )
ScatterPlot( ax.__next__() , LinksToTriggerCell , "LinksToTriggerCell" , (-1024,1024) , (0,2048) , "Phi" , "R_over_Z" ) # , "Energy" )
ScatterPlot( ax.__next__() , LinksToTriggerCell , "Phi vs. Input Channel" , (0,71) , (-1024,1024) , "Index" , "Phi" )

# TriggerCellsIn = [ ( i.R_over_Z , i.Phi ) for i in LinksToTriggerCell ]
# ---------------------------------------------------------------------------------------

  
# ---------------------------------------------------------------------------------------
# TriggerCellDistribution = ReadTriggerCells( "DebugFiles/TriggerCellDistributionInt1.txt" )
# TriggerCellDistribution = ReadTriggerCells( "DebugFiles/TriggerCellDistributionInt2.txt" )
# TriggerCellDistribution = ReadTriggerCells( "DebugFiles/TriggerCellDistributionInt1.txt" )
TriggerCellDistribution = ReadTriggerCells( "DebugFiles/TriggerCellDistribution.txt" )
TriggerCellDistributionCp = copy.deepcopy( TriggerCellDistribution )
for i in TriggerCellDistributionCp: i.Phi += (29*(i.Index-36))

# TriggerCellsOut = [ ( i.R_over_Z , i.Phi ) for i in TriggerCellDistributionCp ]

ScatterPlot( ax.__next__() , TriggerCellDistributionCp , "Phi vs. Processing Channel" , (0,71) , (-1024,1024) , "Index" , "Phi" )
ScatterPlot( ax.__next__() , TriggerCellDistributionCp , "TriggerCellDistribution" , (-1024,1024) , (0,2048) , "Phi" , "R_over_Z" ) # , "Energy" )
# ---------------------------------------------------------------------------------------

# hist = [ 0 for i in range(4000) ]
# for i , j in zip( sorted( TriggerCellsIn ) , sorted( TriggerCellsOut ) ):
  # hist[ i[1] - j[1] ] += 1
  # #print ( i , j )

# for i in range( -2000 , 2000 ):
  # if hist[i] : print( i , hist[i] )
  
# Correct = []
# Incorrect = []

# for i in TriggerCellsOut:
  # if i in TriggerCellsIn: Correct.append( i )
  # else :                  Incorrect.append( i )
  
# print( sorted( Correct ) )
    

# ---------------------------------------------------------------------------------------
ExpectedTriggerCellToHistogramCell = [] 
for i in TriggerCellDistribution:
  Cell = HistogramCell()
  Cell.Clock = i.Clock + 1
  Cell.Index = i.Index
  Cell.S = i.Energy
  Cell.X = i.Phi
  Cell.Y = ( i.R_over_Z - 256 ) % 32
  Cell.N = 1
  Cell.SortKey = int(( i.R_over_Z - 256 )/32)
  ExpectedTriggerCellToHistogramCell.append( Cell )

TriggerCellToHistogramCell = ReadHistogramCells( "DebugFiles/TriggerCellToHistogramCell.txt" )  
# Not unique at this point - Use scatter instead of histo
ScatterPlot( ax.__next__() , TriggerCellToHistogramCell , "TriggerCellToHistogramCell" , (0,71) , (0,39) , "Index" , "SortKey" )# , "S" )
Compare( "TriggerCellToHistogramCell" , TriggerCellToHistogramCell , ExpectedTriggerCellToHistogramCell )
# ---------------------------------------------------------------------------------------
  
# ---------------------------------------------------------------------------------------
ExpectedHistogram = [ HistogramCell.FromInt( 110 , i , j ) for j in range( 40 ) for i in range( 72 ) ]
                        
for i in TriggerCellToHistogramCell: ExpectedHistogram[ (72*i.SortKey) + i.Index ] += i
  
Histogram = ReadHistogramCells( "DebugFiles/Histogram.txt" )[864:864+2880] 
HistogramCellPlot( Histogram , "Histogram" , ax.__next__() )
Compare( "Histogram" , Histogram , ExpectedHistogram ) 

DroppedCells = ReadHistogramCells( "DebugFiles/HistogramDroppedCells.txt" ) 
for i in DroppedCells: ExpectedHistogram[ (72*i.SortKey) + i.Index ] -= i
HistogramCellPlot( DroppedCells , "DroppedCells" , ax.__next__() )
Compare( "Histogram excl. dropped cells" , Histogram , ExpectedHistogram ) 
# ---------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------
SumWidths = [ 6 , 5 , 5 , 5 , 4 , 4 , 4 , 4 , 3 , 3 , 3 , 3 , 3 , 3 , 3 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ]
ExpectedExponentialSmearingKernel1D = copy.deepcopy( Histogram )

for i in ExpectedExponentialSmearingKernel1D:
  i.Clock += 5 
  
  row = i.SortKey
  col = i.Index
          
  index = (72*row) + col
  scale = 1   
  
  width = SumWidths[row]
  offset = 1
    
  while width > 0 :
  
    l1 = HistogramCell()
    l2 = HistogramCell()
    r1 = HistogramCell()
    r2 = HistogramCell()
  
    if width >= 2:
      if (col - offset - 1) >= 0 : l2 = Histogram[index - offset - 1] / 4
      if (col + offset + 1) <= 71 : r2 = Histogram[index + offset + 1] / 4

    if (col - offset) >= 0 :     l1 = Histogram[index - offset] / 2
    if (col + offset) <= 71 :    r1 = Histogram[index + offset] / 2      
  
    i += ( ( l2 + l1 ) / scale ) + ( ( r1 + r2 ) / scale )
    scale *= 4
    width -= 2
    offset += 2
   
ExponentialSmearingKernel1D = ReadHistogramCells( "DebugFiles/ExponentialSmearingKernel1D.txt" )[864:864+2880] 
HistogramCellPlot( ExponentialSmearingKernel1D , "ExponentialSmearingKernel1D" , ax.__next__() )
Compare( "ExponentialSmearingKernel1D" , ExponentialSmearingKernel1D , ExpectedExponentialSmearingKernel1D )       
# ---------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------
CalibrationConstants = [ 239097 , 257998 , 237359 , 219776 , 250090 , 233956 , 219776 , 207218 , 252022 , 239097 , 227434 , 216856 , 207218 , 198400 , 190302 , 255975 , 246315 , 237359 , 229030 , 221266 , 214012 , 207218 , 200842 , 194847 , 189199 , 183869 , 178832 , 174063 , 169542 , 165250 , 161169 , 262143 , 255975 , 250090 , 244470 , 239097 , 233956 , 229030 , 224308 , 219776 ]
ExpectedAreaNormalization = copy.deepcopy( ExponentialSmearingKernel1D )

for i in ExpectedAreaNormalization:
  i.Clock += 2 
  i *= CalibrationConstants[ i.SortKey ]
    
AreaNormalization = ReadHistogramCells( "DebugFiles/AreaNormalization.txt" )[864:864+2880] 
HistogramCellPlot( AreaNormalization , "AreaNormalization" , ax.__next__() )
Compare( "AreaNormalization" , AreaNormalization , ExpectedAreaNormalization )       
# ---------------------------------------------------------------------------------------
  
# ---------------------------------------------------------------------------------------
ExpectedExponentialSmearingKernel2D = copy.deepcopy( AreaNormalization )#[72:72+2736]

for i in ExpectedExponentialSmearingKernel2D:
  i.Clock += 5 
  
  row = i.SortKey
  col = i.Index
  index = (72*row) + col
          
  if (row - 1) >= 0 :  i += ( AreaNormalization[index - 72] / 2 )
  if (row + 1) <= 39 : i += ( AreaNormalization[index + 72] / 2 )
  
ExponentialSmearingKernel2D = ReadHistogramCells( "DebugFiles/ExponentialSmearingKernel2D.txt" )[864:864+2880] 
HistogramCellPlot( ExponentialSmearingKernel2D , "ExponentialSmearingKernel2D" , ax.__next__() )
Compare( "ExponentialSmearingKernel2D" , ExponentialSmearingKernel2D , ExpectedExponentialSmearingKernel2D )       
# ---------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------
ThresholdConstants = [  20 , 20 , 19 , 19 , 18 , 18 , 17 , 17 , 16 , 16 , 15 , 15 , 14 , 14 , 13 , 13 , 12 , 12 , 11 , 11 , 10 , 10 , 9 , 9 , 8 , 8 , 7 , 7 , 6 , 6 , 5 , 5 , 4 , 4 , 3 , 3 , 2 , 2 , 1 , 1 ]
ExpectedSimpleMaximaFinder = [ HistogramCell.FromInt( 127 , i , j ) for j in range( 40 ) for i in range( 72 ) ]

for i in ExponentialSmearingKernel2D:
    
  row = i.SortKey
  col = i.Index
  index = (72*row) + col
  
  if i.S > ThresholdConstants[ i.SortKey ]:
    ExpectedSimpleMaximaFinder[index] = copy.deepcopy( i )
  
SimpleMaximaFinder = ReadHistogramCells( "DebugFiles/SimpleMaximaFinder.txt" )[864:864+2880] 
HistogramCellPlot( SimpleMaximaFinder , "SimpleMaximaFinder" , ax.__next__() )
Compare( "SimpleMaximaFinder" , SimpleMaximaFinder , ExpectedSimpleMaximaFinder ) 
# ---------------------------------------------------------------------------------------



# # # ---------------------------------------------------------------------------------------
# # MaximaWidths = [ 6 , 5 , 5 , 5 , 4 , 4 , 4 , 4 , 3 , 3 , 3 , 3 , 3 , 3 , 3 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ]
# # ExpectedMaximaFinder1D = [ HistogramCell.FromInt( 127 , i , j ) for j in range( 40 ) for i in range( 72 ) ]
# # for i in ExpectedMaximaFinder1D:
    
  # # row = i.SortKey
  # # col = i.Index

  # # # for j in range( 6 , -7 , -1 ):
  # # for j in range( -7 , 6 ):
  
    # # index = col + j
    # # if index < 0 or index > 71 or j > MaximaWidths[ row ] or j < -MaximaWidths[ row ] :
      # # if( i.S == 0 ):
        # # i.MaximaOffset = -7
    # # else :
      # # lRef = ExponentialSmearingKernel2D[ (72*row) + index ]     
      # # if( lRef.S > i.S ):
        # # i.S = lRef.S
        # # i.X = lRef.X
        # # i.Y = lRef.Y
        # # i.N = lRef.N
        # # i.MaximaOffset = j
  
# # MaximaFinder1D = ReadHistogramCells( "DebugFiles/MaximaFinder1D.txt" )[864:864+2880] 
# # Compare( "MaximaFinder1D" , MaximaFinder1D , ExpectedMaximaFinder1D ) 
# # # ---------------------------------------------------------------------------------------
  
# # # ---------------------------------------------------------------------------------------
# # ExpectedMaximaFinder2D =  copy.deepcopy( MaximaFinder1D )
# # for i in ExpectedMaximaFinder2D:
    
  # # row = i.SortKey
  # # col = i.Index
  
  # # criteria0 = True
  # # criteria1 = ( i.MaximaOffset == 0 )
  # # criteria2 = True
  
  # # if row > 0:
    # # lRef = MaximaFinder1D[ (72*(row-1)) + col ]
    # # criteria0 = ( i.S > lRef.S ) or ( ( i.S == lRef.S ) and lRef.MaximaOffset >= 0 )

  # # if row < 39:
    # # lRef = MaximaFinder1D[ (72*(row+1)) + col ]
    # # criteria2 = ( i.S > lRef.S ) or ( ( i.S == lRef.S ) and lRef.MaximaOffset > 0 )
    
  # # if criteria0 and criteria1 and criteria2 :
    # # i.MaximaOffset = 1
  # # else:
    # # i.S = 0
    # # i.X = 0
    # # i.Y = 0
    # # i.N = 0 
    # # i.MaximaOffset = 0
    
# # MaximaFinder2D = ReadHistogramCells( "DebugFiles/MaximaFinder2D.txt" )[864:864+2880] 
# # Compare( "MaximaFinder2D" , MaximaFinder2D , ExpectedMaximaFinder2D )       
# # # ---------------------------------------------------------------------------------------



# ---------------------------------------------------------------------------------------
ExpectedCalculateAveragePositions = copy.deepcopy( SimpleMaximaFinder )

for i in ExpectedCalculateAveragePositions:
    
  if i.N:
    inv_N = int( round( 0x1FFFF / i.N ) )
  
    i.X = ( i.X * inv_N ) >> 17
    i.Y = ( i.Y * inv_N ) >> 17
    
CalculateAveragePositions = ReadHistogramCells( "DebugFiles/CalculateAveragePositions.txt" )[864:864+2880] 
HistogramCellPlot( CalculateAveragePositions , "CalculateAveragePositions" , ax.__next__() )
Compare( "CalculateAveragePositions" , CalculateAveragePositions , ExpectedCalculateAveragePositions ) 
# ---------------------------------------------------------------------------------------


# # ---------------------------------------------------------------------------------------
# ExpectedDomainOfInfluenceLeft =  [ HistogramCell.FromInt( 137 , i , j ) for j in range( 40 ) for i in range( 72 ) ] # copy.deepcopy( CalculateAveragePositions )
# ExpectedDomainOfInfluenceRight = [ HistogramCell.FromInt( 137 , i , j ) for j in range( 40 ) for i in range( 72 ) ] # copy.deepcopy( CalculateAveragePositions )

# for row in range( 40 ):
  # for col in range( 72 ):
  
    # index = (72*row) + col
  
    # if CalculateAveragePositions[index].X < 15:
      # ExpectedDomainOfInfluenceLeft[index].S = CalculateAveragePositions[index].S
      # ExpectedDomainOfInfluenceLeft[index].X = CalculateAveragePositions[index].X
      # ExpectedDomainOfInfluenceLeft[index].Y = CalculateAveragePositions[index].Y
      # ExpectedDomainOfInfluenceLeft[index].N = CalculateAveragePositions[index].N 
    # else:
      # ExpectedDomainOfInfluenceRight[index].S = CalculateAveragePositions[index].S
      # ExpectedDomainOfInfluenceRight[index].X = CalculateAveragePositions[index].X
      # ExpectedDomainOfInfluenceRight[index].Y = CalculateAveragePositions[index].Y
      # ExpectedDomainOfInfluenceRight[index].N = CalculateAveragePositions[index].N    
  
    # for j in range( 1 , 4 ):
      # if not ExpectedDomainOfInfluenceLeft[index].S :
        # ExpectedDomainOfInfluenceLeft[index].MaximaOffset = j
        # if (col - j) < 0 : break
        # ExpectedDomainOfInfluenceLeft[index].S = CalculateAveragePositions[index - j].S
        # ExpectedDomainOfInfluenceLeft[index].X = CalculateAveragePositions[index - j].X
        # ExpectedDomainOfInfluenceLeft[index].Y = CalculateAveragePositions[index - j].Y
        # ExpectedDomainOfInfluenceLeft[index].N = CalculateAveragePositions[index - j].N
 

    # for j in range( 1 , 4 ):
      # if not ExpectedDomainOfInfluenceRight[index].S :        
        # ExpectedDomainOfInfluenceRight[index].MaximaOffset = j
        # if (col + j) > 71 : break
        # ExpectedDomainOfInfluenceRight[index].S = CalculateAveragePositions[index + j].S
        # ExpectedDomainOfInfluenceRight[index].X = CalculateAveragePositions[index + j].X
        # ExpectedDomainOfInfluenceRight[index].Y = CalculateAveragePositions[index + j].Y
        # ExpectedDomainOfInfluenceRight[index].N = CalculateAveragePositions[index + j].N
  
# DomainOfInfluenceLeft = ReadHistogramCells( "DebugFiles/DomainOfInfluenceLeft.txt" )[864:864+2880] 
# DomainOfInfluenceRight = ReadHistogramCells( "DebugFiles/DomainOfInfluenceRight.txt" )[864:864+2880] 
# HistogramCellPlot( DomainOfInfluenceLeft , "DomainOfInfluenceLeft" , ax.__next__() )
# HistogramCellPlot( DomainOfInfluenceRight , "DomainOfInfluenceRight" , ax.__next__() )
# Compare( "DomainOfInfluenceLeft" , DomainOfInfluenceLeft , ExpectedDomainOfInfluenceLeft ) 
# Compare( "DomainOfInfluenceRight" , DomainOfInfluenceRight , ExpectedDomainOfInfluenceRight ) 
# # ---------------------------------------------------------------------------------------



mng = plt.get_current_fig_manager()
mng.window.showMaximized()
plt.show()
