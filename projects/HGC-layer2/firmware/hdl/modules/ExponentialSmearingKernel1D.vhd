-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY ExponentialSmearingKernel1D IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END ExponentialSmearingKernel1D;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF ExponentialSmearingKernel1D IS

  SIGNAL CellsIn , SumHalfLeft , SumHalfRight , LatchedLeft , LatchedRight : Vector( -5 TO 76 ) := NullVector( 82 );
  SIGNAL Output                                                            : Vector( 0 TO 71 )  := NullVector( 72 );

  TYPE tWidths IS ARRAY( 0 TO 39 ) OF INTEGER RANGE 0 TO 6;
  CONSTANT cSumWidths : tWidths := ( 6 , 5 , 5 , 5 , 4 , 4 , 4 , 4 , 3 , 3 , 3 , 3 , 3 , 3 , 3 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 );

BEGIN

-- -------------------------------------------------------------------------
  g1                              : FOR i IN 0 TO 71 GENERATE
    SIGNAL SumWidth               : INTEGER RANGE 0 TO 6 := 0;
    SIGNAL LeftInput , RightInput : tData                := cNull;
  BEGIN

    CellsIn( i ) <= CellPipeIn( PipeOffset + 0 )( i );

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

-- HERE WE ARE MAKING OPTIMAL USE OF THE FACT THAT DATA COMES OUT OF THE HISTOGRAM ONLY EVERY FOURTH CLOCK CYCLE
-- BY REUSING THE LOGIC UP TO 3 TIMES AND PERFORMING REPEATED BIT-SHIFTING ON EACH CLOCK      

        IF CellsIn( i ) .DataValid THEN
          SumWidth          <= cSumWidths( CellsIn( i ) .SortKey );
          LatchedLeft( i )  <= CellsIn( i - 1 ) / 2;
          LatchedRight( i ) <= CellsIn( i + 1 ) / 2;
          SumHalfLeft( i )  <= ( CellsIn( i - 1 ) / 2 ) + ( CellsIn( i - 2 ) / 4 );
          SumHalfRight( i ) <= ( CellsIn( i + 1 ) / 2 ) + ( CellsIn( i + 2 ) / 4 );
          LeftInput         <= cNull;
          RightInput        <= cNull;
        ELSE

-- Bitshift the pieces we are adding on each clock-cycle
-- and, while we are at it, shift the cell indices
-- which means we can do away with the source multiplexer
          SumHalfLeft( i )  <= SumHalfLeft( i - 2 ) / 4;
          SumHalfRight( i ) <= SumHalfRight( i + 2 ) / 4;
          LatchedLeft( i )  <= LatchedLeft( i - 2 ) / 4;
          LatchedRight( i ) <= LatchedRight( i + 2 ) / 4;

-- Select our input based on the remaining width of the sum
-- and update the remaining width
          IF SumWidth = 0 THEN
            SumWidth   <= 0;
            LeftInput  <= cNull;
            RightInput <= cNull;
          ELSIF SumWidth = 1 THEN
            SumWidth   <= 0;
            LeftInput  <= LatchedLeft( i );
            RightInput <= LatchedRight( i );
          ELSE
            SumWidth   <= SumWidth - 2;
            LeftInput  <= SumHalfLeft( i );
            RightInput <= SumHalfRight( i );
          END IF;
        END IF;

-- Do the actual sum
        IF CellPipeIn( PipeOffset + 1 )( i ) .DataValid THEN
          Output( i ) <= CellPipeIn( PipeOffset + 1 )( i );
        ELSE
          Output( i ) <= Output( i ) + LeftInput + RightInput; -- Sortkey comes from "left" entry of sum
        END IF;

        Output( i ) .DataValid  <= CellPipeIn( PipeOffset + 4 )( i ) .DataValid;
        Output( i ) .FrameValid <= CellPipeIn( PipeOffset + 4 )( i ) .FrameValid;

      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "ExponentialSmearingKernel1D" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
