-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY MaximaFinder1D IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END MaximaFinder1D;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF MaximaFinder1D IS

  SIGNAL CellsIn , MaximaLeft , MaximaRight , LatchedLeft , LatchedRight : Vector( -5 TO 76 ) := NullVector( 82 );
  SIGNAL Output                                                          : Vector( 0 TO 71 )  := NullVector( 72 );

  TYPE tWidths IS ARRAY( 0 TO 39 ) OF INTEGER RANGE 1 TO 6;
  CONSTANT cMaximaWidths : tWidths := ( 6 , 5 , 5 , 5 , 4 , 4 , 4 , 4 , 3 , 3 , 3 , 3 , 3 , 3 , 3 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 );

BEGIN

-- -------------------------------------------------------------------------
  g1                              : FOR i IN 0 TO 71 GENERATE
    SIGNAL MaximaWidth            : INTEGER RANGE 0 TO 6 := 0;
    SIGNAL LeftInput , RightInput : tData                := cNull;
  BEGIN

    CellsIn( i ) <= CellPipeIn( PipeOffset + 0 )( i );

    PROCESS( clk )
      VARIABLE CgtL , RgtC , RgtL : BOOLEAN := False;
    BEGIN
      IF RISING_EDGE( clk ) THEN

-- HERE WE ARE MAKING OPTIMAL USE OF THE FACT THAT DATA COMES OUT OF THE HISTOGRAM ONLY EVERY FOURTH CLOCK CYCLE
-- BY REUSING THE LOGIC UP TO 3 TIMES AND PERFORMING REPEATED BIT-SHIFTING ON EACH CLOCK      

        IF CellsIn( i ) .DataValid THEN
          MaximaWidth                     <= cMaximaWidths( CellsIn( i ) .SortKey );

          LatchedLeft( i )                <= CellsIn( i - 1 );
          LatchedLeft( i ) .MaximaOffset  <= -1;
          LatchedRight( i )               <= CellsIn( i + 1 );
          LatchedRight( i ) .MaximaOffset <= + 1;

-- In principle, we could do both comparisons once and reuse them, but that would add to the complexity and I will consider it only if we become resource limited...
          IF( CellsIn( i - 1 ) .S > CellsIn( i - 2 ) .S ) THEN
            MaximaLeft( i )               <= CellsIn( i - 1 );
            MaximaLeft( i ) .MaximaOffset <= -1;
          ELSE
            MaximaLeft( i )               <= CellsIn( i - 2 );
            MaximaLeft( i ) .MaximaOffset <= -2;
          END IF;

          IF( CellsIn( i + 1 ) .S >= CellsIn( i + 2 ) .S ) THEN
            MaximaRight( i )               <= CellsIn( i + 1 );
            MaximaRight( i ) .MaximaOffset <= + 1;
          ELSE
            MaximaRight( i )               <= CellsIn( i + 2 );
            MaximaRight( i ) .MaximaOffset <= + 2;
          END IF;

          LeftInput  <= cNull;
          RightInput <= cNull;
        ELSE

-- Shift the cells so we can do away with a layer of multiplexing
-- and update the value of the offset
          LatchedLeft( i ) <= LatchedLeft( i - 2 );
          MaximaLeft( i )  <= MaximaLeft( i - 2 );

          IF LatchedLeft( i - 2 ) .MaximaOffset > -5 THEN
            LatchedLeft( i ) .MaximaOffset <= LatchedLeft( i - 2 ) .MaximaOffset - 2;
            MaximaLeft( i ) .MaximaOffset  <= MaximaLeft( i - 2 ) .MaximaOffset - 2;
          ELSE
            LatchedLeft( i ) .MaximaOffset <= -7;
            MaximaLeft( i ) .MaximaOffset  <= -7;
          END IF;

          LatchedRight( i ) <= LatchedRight( i + 2 );
          MaximaRight( i )  <= MaximaRight( i + 2 );

          IF LatchedRight( i + 2 ) .MaximaOffset < 5 THEN
            LatchedRight( i ) .MaximaOffset <= LatchedRight( i + 2 ) .MaximaOffset + 2;
            MaximaRight( i ) .MaximaOffset  <= MaximaRight( i + 2 ) .MaximaOffset + 2;
          ELSE
            LatchedRight( i ) .MaximaOffset <= 7;
            MaximaRight( i ) .MaximaOffset  <= 7;
          END IF;

-- Select our input based on the remaining width
          IF MaximaWidth = 0 THEN
            MaximaWidth <= 0;
            LeftInput   <= cNull;
            RightInput  <= cNull;
          ELSIF MaximaWidth = 1 THEN
            MaximaWidth <= 0;
            LeftInput   <= LatchedLeft( i );
            RightInput  <= LatchedRight( i );
          ELSE
            MaximaWidth <= MaximaWidth - 2;
            LeftInput   <= MaximaLeft( i );
            RightInput  <= MaximaRight( i );
          END IF;
        END IF;


        IF CellPipeIn( PipeOffset + 1 )( i ) .DataValid THEN
          Output( i )               <= CellPipeIn( PipeOffset + 1 )( i );
          Output( i ) .MaximaOffset <= 0;
        ELSE
          CgtL := ( Output( i ) .S > LeftInput.S );
          RgtC := ( Output( i ) .S < RightInput.S );
          RgtL := ( LeftInput.S < RightInput.S );

          IF CgtL THEN
            IF RgtC THEN -- Current value is greater than left BUT less than or equal to right => Take the right value
              Output( i ) <= RightInput;
            ELSE -- Current value is greater than left AND greater than or equal to right => Keep our current value
              Output( i ) <= Output( i );
            END IF;
          ELSE
            IF RgtL THEN -- Current value is less than or equal to left but left is less than or equal to right => Keep right
              Output( i ) <= RightInput;
            ELSE -- Current value is less than or equal to left and left is greater than or equal to right => Keep left
              Output( i ) <= LeftInput;
            END IF;
          END IF;

        END IF;

        Output( i ) .SortKey    <= CellPipeIn( PipeOffset + 4 )( i ) .SortKey;
        Output( i ) .DataValid  <= CellPipeIn( PipeOffset + 4 )( i ) .DataValid;
        Output( i ) .FrameValid <= CellPipeIn( PipeOffset + 4 )( i ) .FrameValid;

      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "MaximaFinder1D" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
