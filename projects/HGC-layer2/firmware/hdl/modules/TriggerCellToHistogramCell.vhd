-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC

-- .include components/PkgTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in TriggerCell

-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- .include components/PkgLocalTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in LocalTriggerCell
-- .include ReuseableElements/DataPipe.vhd in LocalTriggerCell
-- .include ReuseableElements/Debugger.vhd in LocalTriggerCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY TriggerCell;
USE TriggerCell.DataType;
USE TriggerCell.ArrayTypes;

LIBRARY HistogramCell;
USE HistogramCell.DataType;
USE HistogramCell.ArrayTypes;

LIBRARY LocalTriggerCell;
USE LocalTriggerCell.DataType;
USE LocalTriggerCell.ArrayTypes;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY TriggerCellToHistogramCell IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk                     : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn              : IN TriggerCell.ArrayTypes.VectorPipe;
    HistogramCellPipeOut    : OUT HistogramCell.ArrayTypes.VectorPipe;
    LocalTriggerCellPipeOut : OUT LocalTriggerCell.ArrayTypes.VectorPipe
  );
END TriggerCellToHistogramCell;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF TriggerCellToHistogramCell IS
  SIGNAL HistogramCellOutput    : HistogramCell.ArrayTypes.Vector( 0 TO 71 )    := HistogramCell.ArrayTypes.NullVector( 72 );
  SIGNAL LocalTriggerCellOutput : LocalTriggerCell.ArrayTypes.Vector( 0 TO 71 ) := LocalTriggerCell.ArrayTypes.NullVector( 72 );
BEGIN

-- -------------------------------------------------------------------------
  g1             : FOR i IN 0 TO 71 GENERATE
    SIGNAL lCell : TriggerCell.DataType.tData := TriggerCell.DataType.cNull;
  BEGIN

    lCell <= CellPipeIn( PipeOffset )( i );

    PROCESS( clk )
      VARIABLE SortKey , LocalRoverZ : INTEGER := 0;
    BEGIN
      IF RISING_EDGE( clk ) THEN

        LocalRoverZ := ( ( TO_INTEGER( lCell.R_over_Z ) - 256 ) MOD 32 );
        SortKey     := ( ( TO_INTEGER( lCell.R_over_Z ) - 256 ) / 32 ) MOD 64;

        HistogramCellOutput( i ) .S             <= TO_UNSIGNED( TO_INTEGER( lCell.Energy ) , 18 );
        HistogramCellOutput( i ) .X             <= TO_UNSIGNED( TO_INTEGER( lCell.Phi ) , 18 );
        HistogramCellOutput( i ) .Y             <= TO_UNSIGNED( LocalRoverZ , 18 );
        HistogramCellOutput( i ) .N             <= 1;
        HistogramCellOutput( i ) .SortKey       <= SortKey;
        HistogramCellOutput( i ) .DataValid     <= lCell.DataValid;
        HistogramCellOutput( i ) .FrameValid    <= lCell.FrameValid;

        LocalTriggerCellOutput( i ) .R_over_Z   <= TO_UNSIGNED( LocalRoverZ , 11 ); -- Only use 5 bits
        LocalTriggerCellOutput( i ) .Layer      <= lCell.Layer;
        LocalTriggerCellOutput( i ) .Energy     <= lCell.Energy;
        LocalTriggerCellOutput( i ) .Phi        <= TO_UNSIGNED( TO_INTEGER( lCell.Phi ) , 5 );
        LocalTriggerCellOutput( i ) .SortKey    <= SortKey;
        LocalTriggerCellOutput( i ) .DataValid  <= lCell.DataValid;
        LocalTriggerCellOutput( i ) .FrameValid <= lCell.FrameValid;

      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  HistogramCellOutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , HistogramCellOutput , HistogramCellPipeOut );

  LocalTriggerCellOutputPipeInstance : ENTITY LocalTriggerCell.DataPipe
  PORT MAP( clk , LocalTriggerCellOutput , LocalTriggerCellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  HistogramCellDebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "TriggerCellToHistogramCell" )
  PORT MAP( clk , HistogramCellOutput );

  LocalTriggerCellDebugInstance : ENTITY LocalTriggerCell.Debug
  GENERIC MAP( "LocalTriggerCells" )
  PORT MAP( clk , LocalTriggerCellOutput );
-- -------------------------------------------------------------------------

END rtl;
