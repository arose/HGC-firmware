-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY MaximaInterleaver IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END MaximaInterleaver;
-- -------------------------------------------------------------------------

-- Within a row, maxima cannot be closer than every other cell (with a window-size of 3)
-- and they are only arriving every fourth clock-cycle, so...
-- interleave 8 neighbouring, potential maxima on a single output

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF MaximaInterleaver IS
  SIGNAL Latch  : Vector( 0 TO 73 ) := NullVector( 74 );
  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );
BEGIN

-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 71 GENERATE
  BEGIN
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        IF CellPipeIn( PipeOffset )( i ) .DataValid THEN
          IF CellPipeIn( PipeOffset )( i ) .MaximaOffset = 1 THEN
            Latch( i )               <= CellPipeIn( PipeOffset )( i );
            Latch( i ) .MaximaOffset <= i MOD 8;
          ELSE
            Latch( i ) <= cNull;
          END IF;
        ELSE
          Latch( i ) <= Latch( i + 2 );
        END IF;
      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
  g2 : FOR i IN 0 TO 8 GENERATE
  BEGIN
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        IF Latch( ( 8 * i ) + 0 ) .DataValid THEN
          Output( i ) <= Latch( ( 8 * i ) + 0 );
        ELSE
          Output( i ) <= Latch( ( 8 * i ) + 1 ); -- If it is valid, great, otherwise it is NULL, great
        END IF;

        Output( i ) .FrameValid <= CellPipeIn( PipeOffset + 1 )( ( 8 * i ) + 0 ) .FrameValid;
      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "MaximaInterleaver" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
