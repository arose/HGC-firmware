-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY DomainOfInfluence IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk              : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn       : IN VectorPipe;
    CellPipeLeftOut  : OUT VectorPipe;
    CellPipeRightOut : OUT VectorPipe
  );
END DomainOfInfluence;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF DomainOfInfluence IS
  SIGNAL OutputLeft1 , OutputRight1             : Vector( 0 TO 71 )  := NullVector( 72 );
-- SIGNAL OutputLeft2 , OutputRight2 : Vector( 0 TO 71 ) := NullVector( 72 );
  SIGNAL CellsIn , LatchedLeft1 , LatchedRight1 : Vector( -1 TO 72 ) := NullVector( 74 );
-- SIGNAL LatchedLeft2 , LatchedRight2 : Vector( -1 TO 72 ) := NullVector( 74 );
BEGIN

-- HERE WE ARE MAKING OPTIMAL USE OF THE FACT THAT DATA COMES OUT OF THE HISTOGRAM ONLY EVERY FOURTH CLOCK CYCLE
-- BY REUSING THE LOGIC UP TO 3 TIMES AND PERFORMING REPEATED BIT-SHIFTING ON EACH CLOCK    
-- This allows a maximum domain of influence of +/- 3 cells in phi  
-- -------------------------------------------------------------------------

  g1 : FOR i IN 0 TO 71 GENERATE
  BEGIN
    CellsIn( i ) <= CellPipeIn( PipeOffset + 0 )( i );

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        IF CellsIn( i ) .DataValid THEN

          IF CellsIn( i ) .X < 15 THEN
            OutputLeft1( i )               <= CellsIn( i );
            OutputLeft1( i ) .MaximaOffset <= 0;

            OutputRight1( i )              <= cNull;
            OutputRight1( i ) .SortKey     <= CellsIn( i ) .SortKey;
          ELSE
            OutputLeft1( i )                <= cNull;
            OutputLeft1( i ) .SortKey       <= CellsIn( i ) .SortKey;

            OutputRight1( i )               <= CellsIn( i );
            OutputRight1( i ) .MaximaOffset <= 0;
          END IF;

          LatchedLeft1( i )                <= CellsIn( i - 1 );
          LatchedLeft1( i ) .MaximaOffset  <= 1;

          LatchedRight1( i )               <= CellsIn( i + 1 );
          LatchedRight1( i ) .MaximaOffset <= 1;

        ELSE

          IF( OutputLeft1( i ) .S = 18x"00000" ) THEN
            OutputLeft1( i )          <= LatchedLeft1( i );
            OutputLeft1( i ) .SortKey <= OutputLeft1( i ) .SortKey;
          END IF;

          IF( OutputRight1( i ) .S = 18x"00000" ) THEN
            OutputRight1( i )          <= LatchedRight1( i );
            OutputRight1( i ) .SortKey <= OutputRight1( i ) .SortKey;
          END IF;

          LatchedLeft1( i )                <= LatchedLeft1( i - 1 );
          LatchedLeft1( i ) .MaximaOffset  <= ( LatchedLeft1( i - 1 ) .MaximaOffset + 1 ) MOD 4;

          LatchedRight1( i )               <= LatchedRight1( i + 1 );
          LatchedRight1( i ) .MaximaOffset <= ( LatchedRight1( i + 1 ) .MaximaOffset + 1 ) MOD 4;

        END IF;

        OutputLeft1( i ) .DataValid   <= CellPipeIn( PipeOffset + 3 )( i ) .DataValid;
        OutputLeft1( i ) .FrameValid  <= CellPipeIn( PipeOffset + 3 )( i ) .FrameValid;
        OutputRight1( i ) .DataValid  <= CellPipeIn( PipeOffset + 3 )( i ) .DataValid;
        OutputRight1( i ) .FrameValid <= CellPipeIn( PipeOffset + 3 )( i ) .FrameValid;

      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- The logic can be copied a second time to extend the domain of influence to +/- 6 cells in phi
-- -- -------------------------------------------------------------------------
-- g2 : FOR i IN 0 TO 71 GENERATE
-- BEGIN    
-- PROCESS( clk )
-- BEGIN
-- IF RISING_EDGE( clk ) THEN
-- IF CellPipeIn( PipeOffset + 4 )( i ) .DataValid THEN
-- OutputLeft2( i )   <= OutputLeft1( i );
-- OutputRight2( i )  <= OutputRight1( i );
-- LatchedLeft2( i )  <= LatchedLeft1( i - 1 );
-- LatchedRight2( i ) <= LatchedRight1( i + 1 );         
-- ELSE
-- If ( OutputLeft2( i ).S = (OTHERS=>'0') ) THEN
-- OutputLeft2( i ) <= LatchedLeft2( i );
-- end if;

-- If ( OutputRight2( i ).S = (OTHERS=>'0') ) THEN
-- OutputRight2( i ) <= LatchedRight2( i );
-- end if;

-- LatchedLeft2( i )  <= LatchedLeft2( i - 1 );
-- LatchedRight2( i ) <= LatchedRight2( i + 1 );  

-- OutputLeft2( i ) .DataValid  <= CellPipeIn( PipeOffset + 8 )( i ) .DataValid;  
-- OutputLeft2( i ) .FrameValid <= CellPipeIn( PipeOffset + 8 )( i ) .FrameValid;          
-- OutputRight2( i ) .DataValid  <= CellPipeIn( PipeOffset + 8 )( i ) .DataValid;  
-- OutputRight2( i ) .FrameValid <= CellPipeIn( PipeOffset + 8 )( i ) .FrameValid;            
-- END IF;
-- END IF;
-- END PROCESS;
-- END GENERATE;
-- -- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstanceL : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , OutputLeft1 , CellPipeLeftOut );

  OutputPipeInstanceR : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , OutputRight1 , CellPipeRightOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstanceL : ENTITY HistogramCell.Debug
  GENERIC MAP( "DomainOfInfluenceLeft" )
  PORT MAP( clk , OutputLeft1 );

  DebugInstanceR : ENTITY HistogramCell.Debug
  GENERIC MAP( "DomainOfInfluenceRight" )
  PORT MAP( clk , OutputRight1 );
-- -------------------------------------------------------------------------

END rtl;
