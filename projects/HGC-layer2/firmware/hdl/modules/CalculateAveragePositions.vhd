-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY CalculateAveragePositions IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END CalculateAveragePositions;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF CalculateAveragePositions IS


  TYPE tDivisionLUT IS ARRAY( 0 TO 511 ) OF UNSIGNED( 17 DOWNTO 0 );

  FUNCTION PrepareDivisionLUTs RETURN tDivisionLUT IS
    VARIABLE lRet : tDivisionLUT := ( OTHERS => ( OTHERS => '1' ) );
  BEGIN
    FOR i IN 1 TO 511 LOOP
      lRet( i ) := TO_UNSIGNED( INTEGER( ROUND( REAL( ( 2.0 ** 17.0 ) -1.0 ) / REAL( i ) ) ) , 18 ); -- Effectively a fraction of 2^17, but in 17-bits not 18
    END LOOP;
    RETURN lRet;
  END PrepareDivisionLUTs;

  CONSTANT DivisionLUT               : tDivisionLUT := PrepareDivisionLUTs;
  ATTRIBUTE ram_style                : STRING;
  ATTRIBUTE ram_style OF DivisionLUT : CONSTANT IS "block";

  SIGNAL Latch                       : Vector( 0 TO 73 ) := NullVector( 74 );
  SIGNAL Output                      : Vector( 0 TO 71 ) := NullVector( 72 );
BEGIN

-- HERE WE ARE MAKING OPTIMAL USE OF THE FACT THAT DATA COMES OUT OF THE HISTOGRAM ONLY EVERY FOURTH CLOCK CYCLE
-- BY REUSING THE LOGIC UP TO 3 TIMES AND PERFORMING REPEATED BIT-SHIFTING ON EACH CLOCK      
-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 71 GENERATE
  BEGIN
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        IF CellPipeIn( PipeOffset )( i ) .DataValid OR CellPipeIn( PipeOffset + 4 )( i ) .DataValid THEN
          Latch( i ) <= CellPipeIn( PipeOffset )( i );
        ELSE
          Latch( i ) <= Latch( i + 1 );
        END IF;
      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
  g2                      : FOR i IN 0 TO 17 GENERATE
    SIGNAL Reg            : tData                   := cNull;
    SIGNAL InvDenominator : UNSIGNED( 17 DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN

    PROCESS( clk )
      VARIABLE TempX , TempY : UNSIGNED( 35 DOWNTO 0 ) := ( OTHERS => '0' );
    BEGIN
      IF RISING_EDGE( clk ) THEN

        Reg                     <= Latch( 4 * i );
        InvDenominator          <= DivisionLUT( Latch( 4 * i ) .N );

-- Every fourth entry is "recalculated"       
        Output( ( 4 * i ) + 3 ) <= Reg;

        TempX := Reg.X * InvDenominator;
        Output( ( 4 * i ) + 3 ) .X <= TempX( 34 DOWNTO 17 ); -- Use the built-in 17-bit shifter in the DSP48e

        TempY := Reg.Y * InvDenominator;
        Output( ( 4 * i ) + 3 ) .Y          <= TempY( 34 DOWNTO 17 ); -- Use the built-in 17-bit shifter in the DSP48e

        Output( ( 4 * i ) + 3 ) .SortKey    <= CellPipeIn( PipeOffset + 5 )( ( 4 * i ) + 3 ) .Sortkey;
        Output( ( 4 * i ) + 3 ) .DataValid  <= CellPipeIn( PipeOffset + 5 )( ( 4 * i ) + 3 ) .DataValid;
        Output( ( 4 * i ) + 3 ) .FrameValid <= CellPipeIn( PipeOffset + 5 )( ( 4 * i ) + 3 ) .FrameValid;

      END IF;
    END PROCESS;

-- The rest are shifted downwards               
-- And the valid-flags just clocked direct from the input        
    g2a : FOR j IN 0 TO 2 GENERATE
      PROCESS( clk )
      BEGIN
        IF RISING_EDGE( clk ) THEN
          Output( ( 4 * i ) + j )             <= Output( ( 4 * i ) + j + 1 );
          Output( ( 4 * i ) + j ) .SortKey    <= CellPipeIn( PipeOffset + 5 )( ( 4 * i ) + j ) .SortKey;
          Output( ( 4 * i ) + j ) .DataValid  <= CellPipeIn( PipeOffset + 5 )( ( 4 * i ) + j ) .DataValid;
          Output( ( 4 * i ) + j ) .FrameValid <= CellPipeIn( PipeOffset + 5 )( ( 4 * i ) + j ) .FrameValid;
        END IF;
      END PROCESS;
    END GENERATE;

  END GENERATE;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "CalculateAveragePositions" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
