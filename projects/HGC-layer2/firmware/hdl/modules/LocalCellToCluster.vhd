-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC

-- .include components/PkgLocalTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in LocalTriggerCell
-- .include ReuseableElements/DataPipe.vhd in LocalTriggerCell
-- .include ReuseableElements/DataRam.vhd in LocalTriggerCell
-- .include ReuseableElements/Debugger.vhd in LocalTriggerCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY LocalTriggerCell;
USE LocalTriggerCell.DataType;
USE LocalTriggerCell.ArrayTypes;

LIBRARY Cluster;
USE Cluster.DataType;
USE Cluster.ArrayTypes;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY LocalCellToCluster IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk              : IN STD_LOGIC := '0'; -- The algorithm clock
    LocalCellPipeIn  : IN LocalTriggerCell.ArrayTypes.VectorPipe;
    ClusterPipeOut   : OUT Cluster.ArrayTypes.VectorPipe
    );
END LocalCellToCluster;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF LocalCellToCluster IS

  TYPE tArrayOfIntegers   IS ARRAY( NATURAL RANGE <> ) OF NATURAL RANGE 0 to 2047;
  
  -- Layer depth in mm (offset from the front face of the endcap)
  CONSTANT cDepths : tArrayOfIntegers( 0 TO 63 ) := ( 0 , -- No zero layer?
                                                      12 , 24 , 37 , 49 , 61 , 73 , 85 , 98 , 110 , 122 , 134 , 146 , 159 , 171 ,  -- CE-E
                                                      183 , 195 , 207 , 220 , 232 , 244 , 256 , 268 , 281 , 293 , 305 , 317 , 329 , 342 ,  -- CE-E
                                                      382 , 431 , 480 , 529 , 578 , 627 , 676 , 725 ,  -- CE-FH
                                                      776 , 826 , 877 , 928 , 1012 , 1096 , 1180 , 1264 , 1348 , 1432 , 1516 , 1599 , 1683 , 1767 , 1851 , 1935 ,  -- CE-BH
                                                      0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ); -- Padding

  CONSTANT cRadius : tArrayOfIntegers( 0 TO 63 ) := ( OTHERS => 50 );
                                                      
  Signal Depth , Radius : tArrayOfIntegers( 0 to 71 ) := ( OTHERS => 0 );  
  Signal Clusters1 , ClustersOut : Cluster.ArrayTypes.Vector( 0 to 71 ) := Cluster.ArrayTypes.NullVector( 72 );
BEGIN

-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 71 GENERATE
    SIGNAL lCell : LocalTriggerCell.DataType.tData := LocalTriggerCell.DataType.cNull;
  BEGIN

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
      
        -- -----------------------------------
        -- Clock 0
        Radius( i ) <= cRadius( TO_INTEGER( LocalCellPipeIn( PipeOffset + 0 )( i ).Layer ) ); 
        -- -----------------------------------

        -- -----------------------------------
        -- Clock 1
        Depth( i ) <= cDepths( TO_INTEGER( LocalCellPipeIn( PipeOffset + 1 )( i ).Layer ) );   
        
        if ( LocalCellPipeIn( PipeOffset + 1 )( i ).DeltaR2 < Radius( i ) ) THEN
          lCell <= LocalCellPipeIn( PipeOffset + 1 )( i );
        else
          lCell <= LocalTriggerCell.DataType.cNull;
        end if;
        -- -----------------------------------       
        
        -- -----------------------------------
        -- Clock 2

        -- Energy = 8 bit
        -- R_over_Z = 11 bit
        -- Layer = 6 bit --> Z = 11 bit
        -- Phi = 5 bit
        
        -- 8 bit
        Clusters1( i ).E    <= TO_UNSIGNED( TO_INTEGER( lCell.Energy ) , 18 );
        -- 8 bit * 11 bit / 8 bit = 11 bit
        Clusters1( i ).ERZ  <= TO_UNSIGNED( ( TO_INTEGER( lCell.Energy ) * TO_INTEGER( lCell.R_over_Z ) ) / 256 , 21 );   
        -- 8 bit * 11 bit / 8 bit = 11 bit
        Clusters1( i ).EZ   <= TO_UNSIGNED( ( TO_INTEGER( lCell.Energy ) * Depth(i) ) / 256 , 21 );   
        -- 8 bit * 5 bit / 2 bit = 11 bit
        Clusters1( i ).EPhi <= TO_UNSIGNED( ( TO_INTEGER( lCell.Energy ) * TO_INTEGER( lCell.Phi ) ) / 4 , 21 );  
        -- -----------------------------------
        
        -- -----------------------------------
        -- Clock 3       
        ClustersOut( i ) <= Clusters1( i );

        -- 11 bit * 11 bit / 11 bit = 11 bit
        ClustersOut( i ).ERZ2  <= TO_UNSIGNED( ( TO_INTEGER( Clusters1( i ).ERZ ) * TO_INTEGER( Clusters1( i ).ERZ ) ) / 2048 , 21 );   
        -- 11 bit * 11 bit / 11 bit = 11 bit
        ClustersOut( i ).EZ2   <= TO_UNSIGNED( ( TO_INTEGER( Clusters1( i ).EZ ) * TO_INTEGER( Clusters1( i ).EZ ) ) / 2048 , 21 );   
        -- 11 bit * 11 bit / 11 bit = 11 bit
        ClustersOut( i ).EPhi2 <= TO_UNSIGNED( ( TO_INTEGER( Clusters1( i ).EPhi ) * TO_INTEGER( Clusters1( i ).EPhi ) ) / 2048  , 21 );  
        
        -- Copy SortKey and valid flags direct from the input
        ClustersOut( i ).SortKey    <= LocalCellPipeIn( PipeOffset + 3 )( i ).SortKey;      
        ClustersOut( i ).DataValid  <= LocalCellPipeIn( PipeOffset + 3 )( i ).DataValid;        
        ClustersOut( i ).FrameValid <= LocalCellPipeIn( PipeOffset + 3 )( i ).FrameValid;       
        -- -----------------------------------        
             
      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
ClusterOutputPipeInstance : ENTITY Cluster.DataPipe
PORT MAP( clk , ClustersOut , ClusterPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
DebugInstance : ENTITY Cluster.Debug
GENERIC MAP( "LocalCellToCluster" )
PORT MAP( clk , ClustersOut );
-- -------------------------------------------------------------------------

END rtl;
