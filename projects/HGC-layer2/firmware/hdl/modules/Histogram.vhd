-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataRam.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY Histogram IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END Histogram;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF Histogram IS
  SIGNAL Output , Dropped : Vector( 0 TO 71 ) := NullVector( 72 );
  CONSTANT OffSet         : INTEGER           := 84;
BEGIN



-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 71 GENERATE

    TYPE tAddressPipe IS ARRAY( 0 TO 3 ) OF INTEGER RANGE 0 TO 511;

    TYPE tPhaseArray  IS ARRAY( 0 TO 3 ) OF UNSIGNED( 1 DOWNTO 0 );
    SIGNAL phases                                          : tPhaseArray := ( "00" , "01" , "10" , "11" );
    SIGNAL read_phase , read_phase_del , write_phase       : UNSIGNED( 1 DOWNTO 0 ); -- Aliases for specific phases
    SIGNAL AddressPipe0 , AddressPipe1                     : tAddressPipe := ( OTHERS => 0 );

    SIGNAL RamOutput0 , RamOutput1 , RamInput0 , RamInput1 : tData        := cNull;
    SIGNAL Updated                                         : tData        := cNull;
    SIGNAL WriteEnable , WriteEnable0 , WriteEnable1       : BOOLEAN      := false;

    SIGNAL ReadOut                                         : tData        := cNull;
    SIGNAL ReadOutIndex                                    : tAddressPipe := ( OTHERS => ( 216-OffSet ) MOD 216 );
  BEGIN


    DataRamInstance0 : ENTITY HistogramCell.DataRam
    PORT MAP(
      clk         => clk ,
      DataIn      => RamInput0 ,
      WriteAddr   => AddressPipe0( 3 ) ,
      WriteEnable => WriteEnable0 ,
      ReadAddr    => AddressPipe0( 0 ) ,
      DataOut     => RamOutput0
    );

    DataRamInstance1 : ENTITY HistogramCell.DataRam
    PORT MAP(
      clk         => clk ,
      DataIn      => RamInput1 ,
      WriteAddr   => AddressPipe1( 3 ) ,
      WriteEnable => WriteEnable1 ,
      ReadAddr    => AddressPipe1( 0 ) ,
      DataOut     => RamOutput1
    );

    write_phase    <= phases( 1 );
    read_phase     <= phases( 0 );
    read_phase_del <= phases( 2 );

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        phases( 0 TO 3 )       <= phases( 1 TO 3 ) & phases( 0 );

        AddressPipe0( 1 TO 3 ) <= AddressPipe0( 0 TO 2 );
        AddressPipe1( 1 TO 3 ) <= AddressPipe1( 0 TO 2 );

        ReadOutIndex( 1 TO 3 ) <= ReadOutIndex( 0 TO 2 );

        Dropped( i )           <= cNull;
        WriteEnable            <= False;

-- Ram access logic
        IF( read_phase( 1 ) = '0' ) THEN
          AddressPipe0( 0 ) <= TO_INTEGER( read_phase( 0 ) & "00" & TO_UNSIGNED( CellPipeIn( PipeOffset + 0 )( i ) .SortKey , 6 ) );
          AddressPipe1( 0 ) <= TO_INTEGER( ( NOT read_phase( 0 ) ) & "00" & TO_UNSIGNED( ReadOutIndex( 0 ) / 4 , 6 ) );
        ELSE
          AddressPipe0( 0 ) <= TO_INTEGER( ( NOT read_phase( 0 ) ) & "00" & TO_UNSIGNED( ReadOutIndex( 0 ) / 4 , 6 ) );
          AddressPipe1( 0 ) <= TO_INTEGER( read_phase( 0 ) & "00" & TO_UNSIGNED( CellPipeIn( PipeOffset + 0 )( i ) .SortKey , 6 ) );
        END IF;

        IF( read_phase_del( 1 ) = '0' ) THEN
          Updated <= RamOutput0 + CellPipeIn( PipeOffset + 2 )( i );
          Readout <= RamOutput1;
        ELSE
          Updated <= RamOutput1 + CellPipeIn( PipeOffset + 2 )( i );
          Readout <= RamOutput0;
        END IF;

        IF( CellPipeIn( PipeOffset + 2 )( i ) .DataValid ) THEN
          IF CellPipeIn( PipeOffset + 2 )( i ) .SortKey < ( ReadOutIndex( 2 ) / 4 ) THEN
            WriteEnable <= True;
          ELSE
            Dropped( i ) <= CellPipeIn( PipeOffset + 2 )( i );
          END IF;
        END IF;

        IF( write_phase( 1 ) = '0' ) THEN
          RamInput0    <= Updated;
          WriteEnable0 <= WriteEnable;

          RamInput1    <= cNull;
          WriteEnable1 <= true;
        ELSE
          RamInput0    <= cNull;
          WriteEnable0 <= true;

          RamInput1    <= Updated;
          WriteEnable1 <= WriteEnable;
        END IF;

-- Readout logic
        IF( CellPipeIn( PipeOffset + 0 )( i ) .FrameValid AND NOT CellPipeIn( PipeOffset + 1 )( i ) .FrameValid ) THEN
          ReadOutIndex( 0 ) <= ( 216-OffSet ) MOD 216;
        ELSE
          ReadOutIndex( 0 ) <= ( ReadOutIndex( 0 ) + 1 ) MOD 216;
        END IF;

        IF( ReadOutIndex( 3 ) MOD 4 = 0 ) THEN
          Output( i )              <= Readout;
          Output( i ) . SortKey    <= ReadOutIndex( 3 ) / 4;
          Output( i ) . FrameValid <= ( ReadOutIndex( 3 ) < 160 );
        ELSE
          Output( i ) <= Output( i ) + Readout;
        END IF;

        IF( ReadOutIndex( 3 ) MOD 4 = 3 ) AND Output( i ) . FrameValid THEN
          Output( i ) .DataValid <= True;
        ELSE
          Output( i ) .DataValid <= False;
        END IF;

      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "Histogram" )
  PORT MAP( clk , Output );

  Debug2Instance : ENTITY HistogramCell.Debug
  GENERIC MAP( "HistogramDroppedCells" )
  PORT MAP( clk , Dropped );
-- -------------------------------------------------------------------------

END rtl;
