-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC

-- .include components/PkgCluster.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in Cluster
-- .include ReuseableElements/DataPipe.vhd in Cluster
-- .include ReuseableElements/DataRam.vhd in Cluster
-- .include ReuseableElements/Debugger.vhd in Cluster


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY Cluster;
USE Cluster.DataType.ALL;
USE Cluster.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY ClusterSum IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk            : IN STD_LOGIC := '0'; -- The algorithm clock
    ClusterPipeIn  : IN VectorPipe;
    ClusterPipeOut : OUT VectorPipe
    );
END ClusterSum;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF ClusterSum IS

  Signal ClustersOut :                  Cluster.ArrayTypes.Vector( 0 to 71 ) := Cluster.ArrayTypes.NullVector( 72 );
      
BEGIN

-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 71 GENERATE

    Signal ClusterRamIn0 , ClusterRamIn1 , ClusterRamOut0 , ClusterRamOut1        : Cluster.DataType.tData := Cluster.DataType.cNull;
    Signal ClusterToBeUpdated , UpdatedCluster , ClusterReadOut : Cluster.DataType.tData := Cluster.DataType.cNull;
  
    SIGNAL Page , PageClk : INTEGER RANGE 0 TO 1 := 0;

    SIGNAL UpdateAddr , ReadoutAddr : INTEGER RANGE 0 TO 511 := 0;
    SIGNAL ReadAddr0 , ReadAddr1 : INTEGER RANGE 0 TO 511 := 0;
    
  BEGIN

    
    -- ------
    ClusterRamInstance0 : ENTITY Cluster.DataRam
    PORT MAP(
      clk         => clk ,
      DataIn      => ClusterRamIn0 ,
      WriteAddr   => ClusterRamIn0 .SortKey ,
      WriteEnable => ClusterRamIn0 .DataValid ,
      ReadAddr    => ReadAddr0 ,
      DataOut     => ClusterRamOut0
    );

    ClusterRamInstance1 : ENTITY Cluster.DataRam
    PORT MAP(
      clk         => clk ,
      DataIn      => ClusterRamIn1 ,
      WriteAddr   => ClusterRamIn1 .SortKey ,
      WriteEnable => ClusterRamIn1 .DataValid ,
      ReadAddr    => ReadAddr1 ,
      DataOut     => ClusterRamOut1
    );
    -- ------
    
    -- ------
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        
        if ( not ClusterPipeIn( PipeOffset + 2 )( i ).FrameValid ) and ( ClusterPipeIn( PipeOffset + 3 )( i ).FrameValid ) then
          Page <= 1 - Page;  
        end if;
        
        
        if not ClusterPipeIn( PipeOffset + 0 )( i ).FrameValid then  
          ReadAddr0 <= 0;
          ReadAddr1 <= 0;
          
          ClusterRamIn0 <= cNull;
          ClusterRamIn1 <= cNull;
       
        else
          if Page = 0 then
            -- Reading and updating RAM 0
            -- Reading-out and cleaning RAM 1
          
            -- RAM 0
            ReadAddr0 <= ClusterPipeIn( PipeOffset + 0 )( i ).SortKey;
            
            -- RAM 1
            ReadAddr1 <= ReadAddr1 + 1;
            ClusterRamIn1 <= cNull;
            ClusterRamIn1.DataValid <= TRUE;
            ClusterRamIn1.SortKey <= ClusterRamIn1.SortKey + 1;         
            
          else
            -- Reading and updating RAM 1
            -- Reading-out and cleaning RAM 0

            -- RAM 1
            ReadAddr1 <= ClusterPipeIn( PipeOffset + 0 )( i ).SortKey;

            -- RAM 0
            ReadAddr0 <= ReadAddr0 + 1;
            ClusterRamIn0 <= cNull;
            ClusterRamIn0.DataValid <= TRUE;
            ClusterRamIn0.SortKey <= ClusterRamIn0.SortKey + 1;    
            
          end if; 
        end if;

        
        if ClusterPipeIn( PipeOffset + 1 )( i ).FrameValid then  

          if Page = 0 then
            -- Reading and updating RAM 0
            -- Reading-out and cleaning RAM 1

            -- RAM 0
            ClusterToBeUpdated <= ClusterRamOut0;
            
            -- RAM 1
            ClusterReadOut <= ClusterRamOut1;
                    
          else
            -- Reading and updating RAM 1
            -- Reading-out and cleaning RAM 0

            -- RAM 1
            ClusterToBeUpdated <= ClusterRamOut1;

            -- RAM 0
            ClusterReadOut <= ClusterRamOut0;
 
          end if; 
        end if;

        
        
      END IF;
    END PROCESS;
    -- ------

  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
ClusterOutputPipeInstance : ENTITY Cluster.DataPipe
PORT MAP( clk , ClustersOut , ClusterPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
DebugInstance : ENTITY Cluster.Debug
GENERIC MAP( "ClusterSum" )
PORT MAP( clk , ClustersOut );
-- -------------------------------------------------------------------------

END rtl;
