-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC

-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- .include components/PkgDomainOfInfluence.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in DomainOfInfluence
-- .include ReuseableElements/DataPipe.vhd in DomainOfInfluence
-- .include ReuseableElements/Debugger.vhd in DomainOfInfluence

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType;
USE HistogramCell.ArrayTypes;

LIBRARY DomainOfInfluence;
USE DomainOfInfluence.DataType;
USE DomainOfInfluence.ArrayTypes;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY DomainOfInfluence2D IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk        : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn : IN HistogramCell.ArrayTypes.VectorPipe;
    DoiPipeOut : OUT DomainOfInfluence.ArrayTypes.VectorPipe
  );
END DomainOfInfluence2D;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF DomainOfInfluence2D IS

  TYPE ValidFlags IS ARRAY( -1 TO 72 ) OF BOOLEAN;
  SIGNAL Propagate                    : ValidFlags                                       := ( OTHERS => false );

  SIGNAL CellsIn                      : HistogramCell.ArrayTypes.Vector( -1 TO 72 )      := HistogramCell.ArrayTypes.NullVector( 74 );
  SIGNAL DoiIn , LatchedLeft1 , LatchedRight1 : DomainOfInfluence.DataType.tDoiArray( -1 TO 72 ) := ( OTHERS => DomainOfInfluence.DataType.cNullDoi );

  TYPE tDoiArrayPipe IS ARRAY( 0 TO 19 ) OF DomainOfInfluence.DataType.tDoiArray( 0 TO 71 );
  SIGNAL CompareHpipe , CompareLpipe , MappedCompares : tDoiArrayPipe := ( OTHERS => ( OTHERS => DomainOfInfluence.DataType.cNullDoi ) );


  TYPE tIntArray IS ARRAY( 0 TO 9 ) OF INTEGER;
  CONSTANT dY    : tIntArray                                      := ( 0 *32, 0 *32,  -1 *32,  -1 *32,  +1 *32,  +1 *32,  -2 *32,  -2 *32,  +2 *32,  +2*32 );

  SIGNAL Output1 : DomainOfInfluence.ArrayTypes.Vector( 0 TO 71 ) := DomainOfInfluence.ArrayTypes.NullVector( 72 );


-- SIGNAL LatchedLeftPipe1 , LatchedRightPipe1 : VectorPipe( 0 to 19 )( -1 TO 72 ) := NullVectorPipe( 20 , 74 );
-- SIGNAL LatchedLeft2 , LatchedRight2 : Vector( -1 TO 72 ) := NullVector( 74 );
BEGIN

-- HERE WE ARE MAKING OPTIMAL USE OF THE FACT THAT DATA COMES OUT OF THE HISTOGRAM ONLY EVERY FOURTH CLOCK CYCLE
-- BY REUSING THE LOGIC UP TO 3 TIMES AND PERFORMING REPEATED BIT-SHIFTING ON EACH CLOCK    
-- This allows a maximum domain of influence of +/- 3 cells in phi  
-- -------------------------------------------------------------------------




  g1 : FOR i IN 0 TO 71 GENERATE
  BEGIN

    CellsIn( i )             <= CellPipeIn( PipeOffset + 0 )( i );

    MappedCompares( 0 )( i ) <= CompareHpipe( 8 )( i );
    MappedCompares( 1 )( i ) <= CompareLpipe( 8 )( i );
    MappedCompares( 2 )( i ) <= CompareHpipe( 12 )( i );
    MappedCompares( 3 )( i ) <= CompareLpipe( 12 )( i );
    MappedCompares( 4 )( i ) <= CompareHpipe( 4 )( i );
    MappedCompares( 5 )( i ) <= CompareLpipe( 4 )( i );
    MappedCompares( 6 )( i ) <= CompareHpipe( 16 )( i );
    MappedCompares( 7 )( i ) <= CompareLpipe( 16 )( i );
    MappedCompares( 8 )( i ) <= CompareHpipe( 0 )( i );
    MappedCompares( 9 )( i ) <= CompareLpipe( 0 )( i );


    PROCESS( clk )
      VARIABLE Index : INTEGER RANGE 0 TO 5 := 0;
    BEGIN

      IF RISING_EDGE( clk ) THEN

        -- DoiIn( i ).Calibration <= 9x"100"; -- Calibration is some funtion of Cell.S and Cell.Sortkey
        DoiIn( i ).X           <= TO_SIGNED( TO_INTEGER( CellsIn( i ).X ) , 9 );
        DoiIn( i ).Y           <= TO_SIGNED( TO_INTEGER( CellsIn( i ).Y ) , 8 );
        DoiIn( i ).Index       <= CellsIn( i ).Index;       
        DoiIn( i ).DataValid   <= ( CellsIn( i ).S /= 18x"00000" );              
      
-- -------------------------------
        IF CellPipeIn( PipeOffset + 1 )( i ) .DataValid OR CellPipeIn( PipeOffset + 1 )( i ) .DataValid THEN

          Propagate( i )         <= NOT DoiIn( i ).DataValid; -- If there is data in the current cell, left and right must not propagate further

          LatchedLeft1( i )      <= DoiIn( i - 1 );
          LatchedLeft1( i ).X    <= DoiIn( i - 1 ).X - 29;
          
          LatchedRight1( i )     <= DoiIn( i + 1 );
          LatchedRight1( i ).X   <= DoiIn( i + 1 ).X + 29;
          
          CompareHpipe( 0 )( i ) <= DoiIn( i );
          CompareLpipe( 0 )( i ) <= DomainOfInfluence.DataType.cNullDoi;

        ELSE

          IF Propagate( i - 1 ) THEN
            LatchedLeft1( i ) <= LatchedLeft1( i - 1 );
            IF( LatchedLeft1( i - 1 ) .DataValid ) THEN -- AND( LatchedLeft1( i - 1 ) .dX /= -7 ) THEN
              LatchedLeft1( i ) .X <= LatchedLeft1( i - 1 ) .X - 29;
            END IF;
          ELSE
            LatchedLeft1( i ) <= DomainOfInfluence.DataType.cNullDoi;
          END IF;

          IF Propagate( i + 1 ) THEN
            LatchedRight1( i ) <= LatchedRight1( i + 1 );
            IF( LatchedRight1( i + 1 ) .DataValid ) THEN -- AND( LatchedRight1( i + 1 ) .dX /= 7 ) THEN
              LatchedRight1( i ) .X <= LatchedRight1( i + 1 ) .X + 29;
            END IF;
          ELSE
            LatchedRight1( i ) <= DomainOfInfluence.DataType.cNullDoi;
          END IF;

          IF LatchedLeft1( i ) .Calibration > LatchedRight1( i ) .Calibration THEN -- Prioritize the seed with the larger calibration factor, since it will dominate
            CompareHpipe( 0 )( i ) <= LatchedLeft1( i );
            CompareLpipe( 0 )( i ) <= LatchedRight1( i );
          ELSE
            CompareHpipe( 0 )( i ) <= LatchedRight1( i );
            CompareLpipe( 0 )( i ) <= LatchedLeft1( i );
          END IF;

        END IF;
-- -------------------------------

-- -------------------------------
        IF CellPipeIn( PipeOffset + 10 )( i ) .DataValid THEN
          Output1( i ) <= DomainOfInfluence.DataType.cNull;
          Index := 0;
        END IF;

        FOR j IN 0 TO 9 LOOP
          IF Index /= 3 THEN
            IF MappedCompares( j )( i ) .DataValid THEN
              Output1( i ) .Data( Index )    <= MappedCompares( j )( i );
              Output1( i ) .Data( Index ) .Y <= MappedCompares( j )( i ).Y + dY( j );
              Index := Index + 1;
            END IF;
          END IF;
        END LOOP;

        Output1( i ) .SortKey    <= CellPipeIn( PipeOffset + 13 )( i ) .SortKey;
        Output1( i ) .DataValid  <= CellPipeIn( PipeOffset + 13 )( i ) .DataValid;
        Output1( i ) .FrameValid <= CellPipeIn( PipeOffset + 13 )( i ) .FrameValid;
-- -------------------------------

      END IF;
    END PROCESS;
  END GENERATE;


  g2a   : FOR i IN 0 TO 71 GENERATE
    g2b : FOR j IN 1 TO 19 GENERATE
      PROCESS( clk )
      BEGIN
        IF RISING_EDGE( clk ) THEN
          CompareHpipe( j )( i ) <= CompareHpipe( j-1 )( i );
          CompareLpipe( j )( i ) <= CompareLpipe( j-1 )( i );
        END IF;
      END PROCESS;
    END GENERATE;
  END GENERATE;
-- -- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline 
  OutputPipeInstance : ENTITY DomainOfInfluence.DataPipe
  PORT MAP( clk , Output1 , DoiPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstanceL : ENTITY DomainOfInfluence.Debug
  GENERIC MAP( "DomainOfInfluence" )
  PORT MAP( clk , Output1 );
-- -------------------------------------------------------------------------

END rtl;
