-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC

-- .include components/PkgLocalTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in LocalTriggerCell
-- .include ReuseableElements/DataPipe.vhd in LocalTriggerCell
-- .include ReuseableElements/DataRam.vhd in LocalTriggerCell
-- .include ReuseableElements/Debugger.vhd in LocalTriggerCell

-- .include components/PkgDomainOfInfluence.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in DomainOfInfluence
-- .include ReuseableElements/DataPipe.vhd in DomainOfInfluence
-- .include ReuseableElements/DataRam.vhd in DomainOfInfluence
-- .include ReuseableElements/Debugger.vhd in DomainOfInfluence


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY LocalTriggerCell;
USE LocalTriggerCell.DataType;
USE LocalTriggerCell.ArrayTypes;

LIBRARY DomainOfInfluence;
USE DomainOfInfluence.DataType;
USE DomainOfInfluence.ArrayTypes;

LIBRARY TriggerCell;
USE TriggerCell.DataType;
USE TriggerCell.ArrayTypes;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY Clusterizer IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk              : IN STD_LOGIC := '0'; -- The algorithm clock
    LocalCellPipeIn  : IN LocalTriggerCell.ArrayTypes.VectorPipe;
    DoiPipeIn        : IN DomainOfInfluence.ArrayTypes.VectorPipe;
    LocalCellPipeOut : OUT LocalTriggerCell.ArrayTypes.VectorPipe
  );
END Clusterizer;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF Clusterizer IS

  SIGNAL DoiRamOut              : DomainOfInfluence.ArrayTypes.Vector( 0 TO 71 )               := DomainOfInfluence.ArrayTypes.NullVector( 72 );
  SIGNAL DoiRamPipe             : DomainOfInfluence.ArrayTypes.VectorPipe( 0 TO 6 )( 0 TO 71 ) := DomainOfInfluence.ArrayTypes.NullVectorPipe( 7 , 72 );


  SIGNAL TcRamOut0 , TcRamOut1 , TcRamOut2 : LocalTriggerCell.ArrayTypes.Vector( 0 TO 71 )                := LocalTriggerCell.ArrayTypes.NullVector( 72 );
  SIGNAL TcRamPipe              : LocalTriggerCell.ArrayTypes.VectorPipe( 0 TO 9 )( 0 TO 71 )  := LocalTriggerCell.ArrayTypes.NullVectorPipe( 10 , 72 );

  SIGNAL LocalTriggerCellOutput : LocalTriggerCell.ArrayTypes.Vector( 0 TO 71 )                := LocalTriggerCell.ArrayTypes.NullVector( 72 );

BEGIN

-- -------------------------------------------------------------------------
  g1                   : FOR i IN 0 TO 71 GENERATE
    SIGNAL TcWriteAddr : INTEGER RANGE 0 TO 1023 := 0;

    TYPE tAddrPipe IS ARRAY( 0 TO 1 ) OF INTEGER RANGE 0 TO 1023;
    SIGNAL TcReadAddr              : tAddrPipe                        := ( 0 , 1 );

    SIGNAL TcIn                    : LocalTriggerCell.DataType.tData  := LocalTriggerCell.DataType.cNull;
    SIGNAL DoiIn                   : DomainOfInfluence.DataType.tData := DomainOfInfluence.DataType.cNull;

    SIGNAL CommonR                 : INTEGER                          := 0;

    SIGNAL DoiCounter , TimeOffset : INTEGER                          := 0;

    CONSTANT TimeOffsetThreshold   : INTEGER                          := 128; -- SHOULD BE PROGRAMMABLE!    

    TYPE tLUT IS ARRAY( 0 TO 245 ) OF INTEGER RANGE 0 TO 255;
    CONSTANT cCosLUT : tLUT := ( 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 2 , 2 , 2 , 2 , 2 , 2 , 3 , 3 , 3 , 3 , 4 , 4 , 4 , 4 , 5 , 5 , 5 , 6 , 6 , 6 , 7 , 7 , 7 , 8 , 8 , 8 , 9 , 9 , 9 , 10 , 10 , 11 , 11 , 12 , 12 , 12 , 13 , 13 , 14 , 14 , 15 , 15 , 16 , 16 , 17 , 18 , 18 , 19 , 19 , 20 , 20 , 21 ,
                                 22 , 22 , 23 , 23 , 24 , 25 , 25 , 26 , 27 , 27 , 28 , 29 , 29 , 30 , 31 , 32 , 32 , 33 , 34 , 35 , 35 , 36 , 37 , 38 , 39 , 39 , 40 , 41 , 42 , 43 , 44 , 45 , 45 , 46 , 47 , 48 , 49 , 50 , 51 , 52 , 53 , 54 , 55 , 56 , 57 , 58 , 59 , 60 , 61 , 62 , 63 , 64 , 65 , 66 , 67 , 68 , 69 , 70 , 71 , 72 , 73 ,
                                 75 , 76 , 77 , 78 , 79 , 80 , 81 , 83 , 84 , 85 , 86 , 87 , 89 , 90 , 91 , 92 , 94 , 95 , 96 , 97 , 99 , 100 , 101 , 103 , 104 , 105 , 107 , 108 , 109 , 111 , 112 , 114 , 115 , 116 , 118 , 119 , 121 , 122 , 123 , 125 , 126 , 128 , 129 , 131 , 132 , 134 , 135 , 137 , 138 , 140 , 141 , 143 , 145 , 146 , 148 ,
                                 149 , 151 , 153 , 154 , 156 , 157 , 159 , 161 , 162 , 164 , 166 , 167 , 169 , 171 , 172 , 174 , 176 , 178 , 179 , 181 , 183 , 185 , 186 , 188 , 190 , 192 , 194 , 195 , 197 , 199 , 201 , 203 , 205 , 206 , 208 , 210 , 212 , 214 , 216 , 218 , 220 , 222 , 224 , 226 , 228 , 229 , 231 , 233 , 235 , 237 , 239 ,
                                 241 , 243 , 246 , 248 , 250 , 252 , 254 , 255 );

    TYPE tPoints IS ARRAY( NATURAL RANGE <> ) OF INTEGER;
    SIGNAL Phi , R                                                                                      : tPoints( 0 TO 2 )                := ( OTHERS => 0 );

    SIGNAL dPhi , dR , dR2 , dR2clk , R2 , R2clk , CosLutOut , R2Cos , Delta2 , CalibDelta2 : tPoints( 0 TO 2 )                := ( OTHERS => 0 );
    -- SIGNAL DeltaComp : tPoints( 0 TO 4 ) := ( OTHERS => 0 );
    -- SIGNAL DoiComp   : DomainOfInfluence.DataType.tData := DomainOfInfluence.DataType.cNull;

  BEGIN

    DoiIn <= DoiPipeIn( 0 )( i );
    TcIn  <= LocalCellPipeIn( 0 )( i );


    LocalTriggerCellRamInstance : ENTITY LocalTriggerCell.DataRam
    GENERIC MAP(
      Count => 1024
    )
    PORT MAP(
      clk         => clk ,
      DataIn      => TcIn ,
      WriteAddr   => TcWriteAddr ,
      WriteEnable => TcIn.DataValid ,
      ReadAddr    => TcReadAddr( 0 ) ,
      DataOut     => TcRamOut0( i )
    );

    DoiRamInstance : ENTITY DomainOfInfluence.DataRam
    PORT MAP(
      clk         => clk ,
      DataIn      => DoiIn ,
      WriteAddr   => DoiCounter ,
      WriteEnable => DoiIn.DataValid ,
      ReadAddr    => TcRamOut0( i ) .SortKey ,
      DataOut     => DoiRamOut( i )
    );


-- Process keeping track of inputs
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        IF NOT DoiIn.FrameValid THEN
          DoiCounter <= 0;
        ELSIF DoiIn.DataValid THEN
          DoiCounter <= ( DoiCounter + 1 ) MOD 64;
        END IF;

        IF TcIn.DataValid THEN
          TcWriteAddr <= ( TcWriteAddr + 1 ) MOD 1024;
        END IF;

        IF LocalCellPipeIn( 0 )( i ) .FrameValid AND NOT LocalCellPipeIn( 1 )( i ) .FrameValid THEN
          TimeOffset <= 0;
        ELSE
          TimeOffset <= ( TimeOffset + 1 ) MOD 216;
        END IF;

      END IF;
    END PROCESS;


-- Process keeping track of Ram Outputs
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        
        TcReadAddr( 1 )            <= TcReadAddr( 0 );

        -- CommonR aligned with TcRamOut1
        CommonR                    <= ( 32 * TcRamOut0( i ) .SortKey ) + 256;                    
        TcRamOut1( i )             <= TcRamOut0( i );     

        IF( TcReadAddr( 1 ) < TcWriteAddr ) AND( TcRamOut0( i ) .SortKey < DoiCounter ) AND( TimeOffset > TimeOffsetThreshold ) THEN -- NEED TO HANDLE ADDRESS RAP-AROUND
          TcReadAddr( 0 )           <= ( TcReadAddr( 1 ) + 2 ) MOD 1024;
          TcRamOut1( i ) .DataValid <= TRUE;
        ELSE
          TcReadAddr( 0 )           <= ( TcReadAddr( 1 ) );
          TcRamOut1( i ) .DataValid <= FALSE;
        END IF;

        TcRamOut1( i ) .FrameValid <= DoiPipeIn( 4 )( i ) .FrameValid;        
        
        -- What was previously clocked as Phi( 4 ) and R( 4 ) is now clocked as TcRamOut2
        -- if TcRamOut1( i ) .DataValid then
          TcRamOut2( i )             <= TcRamOut1( i );
          TcRamOut2( i ).R_over_Z    <= TO_UNSIGNED( CommonR , 11 ) + TcRamOut1( i ).R_over_Z;
        -- else
          -- TcRamOut2( i )             <= LocalTriggerCell.DataType.cNull;
        -- end if;          
        
      END IF;
    END PROCESS;
   
    
-- Calculate the geometric terms
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        -- Phi( 4 ) <= TO_INTEGER( TcRamPipe( 0 )( i ) .Phi ); -- Dropping the "29 x (j-36)" term, since it is lost in the subsequent subtraction
        -- R( 4 )   <= CommonR + TO_INTEGER( TcRamPipe( 0 )( i ) .R_over_Z );

-- ---------------------------------------------------------------------------        
        FOR j IN 0 TO 2 LOOP
-- Step 0
          Phi( j )  <= TO_INTEGER( DoiRamPipe( 0 )( i ) .Data( j ) .X ); -- Dropping the "29 x (j-36)" term, since it is lost in the subsequent subtraction
          R( j )    <= CommonR + TO_INTEGER( DoiRamPipe( 0 )( i ) .Data( j ) .Y ); -- Should be ~trivial to implement in fabric

-- Step 1
          dPhi( j ) <= ABS( Phi( j ) - TO_INTEGER( TcRamOut2( i ).Phi ) );
          dR( j )   <= R( j ) - TO_INTEGER( TcRamOut2( i ).R_over_Z );
          R2( j )   <= R( j ) * TO_INTEGER( TcRamOut2( i ).R_over_Z );

-- Step 2
          IF dPhi( j ) > 245 THEN
            CosLutOut( j ) <= 255;
          ELSE
            CosLutOut( j ) <= cCosLUT( dPhi( j ) );
          END IF;

          dR2( j )         <= dR( j ) * dR( j );
          R2clk( j )       <= R2( j );

-- Step 3
          R2Cos( j )       <= ( CosLutOut( j ) * R2clk( j ) ) / ( 2 ** 12 ); -- LUT is scaled by 2^12        
          dR2clk( j )      <= dR2( j );

-- Step 4
          Delta2( j )      <= dR2clk( j ) + R2Cos( j );

-- Step 5
          CalibDelta2( j ) <= ( Delta2( j ) * TO_INTEGER( DoiRamPipe( 5 )( i ) .Data( j ) .Calibration ) ) / ( 2 ** 8 );
        END LOOP;
-- ---------------------------------------------------------------------------        

-- Step 6
        LocalTriggerCellOutput( i )           <= TcRamPipe( 5 )( i ); -- TcRamPipe now clocks one cycle after DoiRamPipe
        
        IF ( CalibDelta2( 0 ) <= CalibDelta2( 1 ) ) AND ( CalibDelta2( 0 ) <= CalibDelta2( 2 ) ) THEN
          LocalTriggerCellOutput( i ) .SortKey  <= DoiRamPipe( 6 )( i ) .Data( 0 ) .Index;
          LocalTriggerCellOutput( i ) .DeltaR2  <= CalibDelta2( 0 );  
        ELSIF ( CalibDelta2( 1 ) <= CalibDelta2( 2 ) ) THEN
          LocalTriggerCellOutput( i ) .SortKey  <= DoiRamPipe( 6 )( i ) .Data( 1 ) .Index;
          LocalTriggerCellOutput( i ) .DeltaR2  <= CalibDelta2( 1 );       
        ELSE
          LocalTriggerCellOutput( i ) .SortKey  <= DoiRamPipe( 6 )( i ) .Data( 2 ) .Index;
          LocalTriggerCellOutput( i ) .DeltaR2  <= CalibDelta2( 2 );  
        END IF;

      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the ram output in a pipeline
  DoiRamPipeInstance : ENTITY DomainOfInfluence.DataPipe
  PORT MAP( clk , DoiRamOut , DoiRamPipe );

  TriggerCellRamPipeInstance : ENTITY LocalTriggerCell.DataPipe
  PORT MAP( clk , TcRamOut2 , TcRamPipe );
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
-- Store the result in a pipeline
LocalTriggerCellOutputPipeInstance : ENTITY LocalTriggerCell.DataPipe
PORT MAP( clk , LocalTriggerCellOutput , LocalCellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
DebugInstance : ENTITY LocalTriggerCell.Debug
GENERIC MAP( "ClusteredLocalTriggerCells" )
PORT MAP( clk , LocalTriggerCellOutput );
-- -------------------------------------------------------------------------

END rtl;
