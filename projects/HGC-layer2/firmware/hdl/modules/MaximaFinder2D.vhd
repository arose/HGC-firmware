-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY MaximaFinder2D IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END MaximaFinder2D;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF MaximaFinder2D IS
  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );
BEGIN

-- -------------------------------------------------------------------------
  g1                                            : FOR i IN 0 TO 71 GENERATE
    SIGNAL ValidCell0 , ValidCell1 , ValidCell2 : tData   := cNull;
    SIGNAL Criteria0 , Criteria1 , Criteria2    : BOOLEAN := FALSE;
  BEGIN

    ValidCell0 <= CellPipeIn( PipeOffset )( i );

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        IF ValidCell0.DataValid OR CellPipeIn( PipeOffset + 4 )( i ) .DataValid OR CellPipeIn( PipeOffset + 8 )( i ) .DataValid THEN
          ValidCell1 <= ValidCell0;
          ValidCell2 <= ValidCell1;
        END IF;

-- OPTIMISATION NOTE - COULD ACTUALLY JUST HAVE A -1, 0 , 1 FLAG FOR MaximaOffset, RATHER THAN A GENUINE OFFSET
        Criteria0   <= ( ValidCell1.S > ValidCell0.S ) OR( ( ValidCell1.S = ValidCell0.S ) AND( ValidCell0.MaximaOffset > 0 ) );
        Criteria1   <= ( ValidCell1.MaximaOffset = 0 );
        Criteria2   <= ( ValidCell1.S > ValidCell2.S ) OR( ( ValidCell1.S = ValidCell2.S ) AND( ValidCell2.MaximaOffset >= 0 ) );

-- The extra clock-cycle to register the criteria pushes the "central" row into ValidCell2
        Output( i ) <= ValidCell2;
        IF Criteria0 AND Criteria1 AND Criteria2 THEN
          Output( i )               <= ValidCell2;
          Output( i ) .MaximaOffset <= 1;
        ELSE
          Output( i )               <= cNull;
          Output( i ) .MaximaOffset <= 0;
        END IF;

        Output( i ) .SortKey    <= CellPipeIn( PipeOffset + 5 )( i ) .SortKey;
        Output( i ) .DataValid  <= CellPipeIn( PipeOffset + 5 )( i ) .DataValid;
        Output( i ) .FrameValid <= CellPipeIn( PipeOffset + 5 )( i ) .FrameValid;

      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "MaximaFinder2D" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
