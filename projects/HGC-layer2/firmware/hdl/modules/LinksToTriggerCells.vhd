-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in TriggerCell
-- .include ReuseableElements/DataPipe.vhd in TriggerCell
-- .include ReuseableElements/Debugger.vhd in TriggerCell
-- .include ReuseableElements/PkgDebug.vhd
-- .include ReuseableElements/PkgUtilities.vhd
-- .include TopLevelInterfaces/mp7_data_types.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY Interfaces;
USE Interfaces.mp7_data_types.ALL;

LIBRARY TriggerCell;
USE TriggerCell.DataType.ALL;
USE TriggerCell.ArrayTypes.ALL;

LIBRARY Utilities;
USE Utilities.debugging.ALL;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY LinksToTriggerCells IS
  PORT(
    clk         : IN STD_LOGIC            := '0'; -- The algorithm clock
    linksIn     : IN ldata( 71 DOWNTO 0 ) := ( OTHERS => LWORD_NULL );
    CellPipeOut : OUT VectorPipe
  );
END LinksToTriggerCells;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF LinksToTriggerCells IS
  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );

  TYPE tArrayOfIntegers   IS ARRAY( 0 TO 7 ) OF INTEGER;
  TYPE tLinkToDetLayerMap IS ARRAY( 0 TO 8 ) OF tArrayOfIntegers;

-- Assume candidates are zero-indexed on the link  
   CONSTANT LayerLut : tLinkToDetLayerMap := ( ( 1 , 19 , 33 , 42 , -1 , -1 , -1 , -1 ) ,
                                               (3 , 21 , 34 , 43 , -1 , -1 , -1 , -1 ) ,
                                               (5 , 23 , 35 , -1 , -1 , -1 , -1 , -1 ) ,
                                               (7 , 25 , 36 , -1 , -1 , -1 , -1 , -1 ) ,
                                               (9 , 27 , 37 , -1 , -1 , -1 , -1 , -1 ) ,
                                               (11 , 29 , 38 , 44 , 45 , -1 , -1 , -1 ) ,
                                               (13 , 30 , 39 , 46 , 47 , -1 , -1 , -1 ) ,
                                               (15 , 31 , 40 , 48 , 49 , -1 , -1 , -1 ) ,
                                               (17 , 32 , 50 , 51 , 52 , -1 , -1 , -1 ) );

-- Assume candidates are one-indexed on the link
--  CONSTANT LayerLut : tLinkToDetLayerMap := ( (-1,1,19,33,42,-1,-1,-1) , (-1,3,21,34,43,-1,-1,-1) , (-1,5,23,35,-1,-1,-1,-1) , (-1,7,25,36,-1,-1,-1,-1) , (-1,9,27,37,-1,-1,-1,-1) , (-1,11,29,38,44,45,-1,-1) , (-1,13,30,39,46,47,-1,-1) , (-1,15,31,40,48,49,-1,-1) , (-1,17,32,50,51,52,-1,-1) );

-- Phi is a 10bit unsigned per 60-degree sector. For unduplicated sectors this wastes a bit on the fibre, but ho-hum.
-- CONSTANT PhiLut : tArrayOfIntegers := ( -1536 , -1536 , -512 , -512 , -512 , -512 , 512 , 512 );
  CONSTANT PhiLut : tArrayOfIntegers := ( -1024 , -1024 , -512 , -512 , 0 , 0 , 512 , 512 ); -- WE ARE IGNORING THE SIGN BIT


BEGIN

-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 71 GENERATE
  BEGIN

    PROCESS( clk )
      VARIABLE LocalLayer , LutOutput , LocalPhi : INTEGER;
    BEGIN
      IF RISING_EDGE( clk ) THEN

        Output( i ) .R_over_Z <= UNSIGNED( linksIn( i ) .data( 10 DOWNTO 0 ) );

        if( linksIn( i ) .data( 20 ) = '1' ) then
          REPORT "IGNORING THE 10TH PHI BIT BECAUSE WE HAVE NO OVERLAPS" SEVERITY WARNING;
        end if;
        
        LocalPhi := TO_INTEGER( "0" & UNSIGNED( linksIn( i ) .data( 19 DOWNTO 11 ) ) );

        Output( i ) .Phi <= TO_SIGNED( LocalPhi + PhiLut( i MOD 8 ) , 13 );

        LocalLayer := TO_INTEGER( UNSIGNED( linksIn( i ) .data( 23 DOWNTO 21 ) ) );
        LutOutput  := LayerLut( i / 8 )( LocalLayer );
        ASSERT( LutOutput /= -1 ) REPORT( "INVALID CELL LAYER: Link = " & INTEGER'IMAGE( i ) & ", Local Layer = " & INTEGER'IMAGE( LocalLayer ) & ", Clock = " & INTEGER'IMAGE( SimulationClockCounter ) ) SEVERITY FAILURE;
        Output( i ) .Layer      <= TO_UNSIGNED( LutOutput , 6 );

        Output( i ) .Energy     <= UNSIGNED( linksIn( i ) .data( 31 DOWNTO 24 ) );
        Output( i ) .DataValid  <= ( linksIn( i ) .data( 31 DOWNTO 24 ) /= x"00" );
        Output( i ) .FrameValid <= to_boolean( linksIn( i ) .valid );

      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline  
  OutputPipeInstance : ENTITY TriggerCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY TriggerCell.Debug
  GENERIC MAP( "LinksToTriggerCell" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
