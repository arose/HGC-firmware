-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY SimpleMaximaFinder IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END SimpleMaximaFinder;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF SimpleMaximaFinder IS

  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );

  TYPE tThresholds IS ARRAY( 0 TO 39 ) OF INTEGER RANGE 0 TO 31;
  CONSTANT cThresholds : tThresholds := ( 20 , 20 , 19 , 19 , 18 , 18 , 17 , 17 , 16 , 16 , 15 , 15 , 14 , 14 , 13 , 13 , 12 , 12 , 11 , 11 , 10 , 10 , 9 , 9 , 8 , 8 , 7 , 7 , 6 , 6 , 5 , 5 , 4 , 4 , 3 , 3 , 2 , 2 , 1 , 1 );

BEGIN

-- -------------------------------------------------------------------------
  g1                 : FOR i IN 0 TO 71 GENERATE
    SIGNAL Threshold : UNSIGNED( 17 DOWNTO 0 ) := ( OTHERS => '0' );
    SIGNAL CellIn    : tData                   := cNull;
  BEGIN


    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        Threshold <= TO_UNSIGNED( cThresholds( CellPipeIn( PipeOffset + 0 )( i ) .SortKey ) , 18 );
        CellIn    <= CellPipeIn( PipeOffset + 0 )( i );

        IF CellIn.S > Threshold THEN
          Output( i ) <= CellIn;
        ELSE
          Output( i ) <= cNull;
        END IF;

        Output( i ) .SortKey    <= CellIn.SortKey;
        Output( i ) .DataValid  <= CellIn.DataValid;
        Output( i ) .FrameValid <= CellIn.FrameValid;

      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "SimpleMaximaFinder" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
