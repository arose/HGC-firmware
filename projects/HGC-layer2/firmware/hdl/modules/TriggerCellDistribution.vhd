-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgTriggerCell.vhd
-- .include ReuseableElements/DistributionServer.vhd in TriggerCell
-- .include ReuseableElements/PkgArrayTypes.vhd in TriggerCell
-- .include ReuseableElements/DataPipe.vhd in TriggerCell
-- .include ReuseableElements/Debugger.vhd in TriggerCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY TriggerCell;
USE TriggerCell.DataType.ALL;
USE TriggerCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY TriggerCellDistribution IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END TriggerCellDistribution;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF TriggerCellDistribution IS
  SIGNAL Intermediate1 , Intermediate2 , Intermediate3 , Output : Vector( 0 TO 71 ) := NullVector( 72 );

  TYPE tTriggerCellGroups IS ARRAY( 0 TO 3 ) OF Vector( 0 TO 17 );
  SIGNAL lInputGroups1 , lOutputGroups1 : tTriggerCellGroups := ( OTHERS => NullVector( 18 ) );
  SIGNAL lInputGroups2 , lOutputGroups2 : tTriggerCellGroups := ( OTHERS => NullVector( 18 ) );

  TYPE tParameterArray IS ARRAY( 0 TO 3 ) OF INTEGER;
  CONSTANT cOutputSizes : tParameterArray := ( 6 , 6 , 6 , 6 );

BEGIN

-- -------------------------------------------------------------------------
-- Map ( 9 depths x 8 fibres ) to 4 x ( 2 fibres x 9 depths )
  g1    : FOR i IN 0 TO 8 GENERATE
    g1a : FOR j IN 0 TO 7 GENERATE
      PROCESS( clk )
        VARIABLE lCell : tData := cNull;
      BEGIN
        IF RISING_EDGE( clk ) THEN
          lCell         := CellPipeIn( PipeOffset )( ( 8 * i ) + j );

-- 10bits/60 degree sector = range 0 to 1023
-- Each group of links spans range 0 to 511
-- Integer divide by 86 maps into 1 of 6 coarse phi sectors
          lCell.SortKey := ( TO_INTEGER( lCell.Phi ) MOD 512 ) / 86;
          lCell.Phi     := TO_SIGNED( ( ( TO_INTEGER( lCell.Phi ) MOD 512 ) MOD 86 ) , 13 );
-- Need handling of overlap regions

          lInputGroups1( j / 2 )( ( 2 * i ) + ( j MOD 2 ) ) <= lCell;
        END IF;
      END PROCESS;
    END GENERATE;
  END GENERATE;

-- First distribution layer
  g2                                        : FOR i IN 0 TO 3 GENERATE
    g2a                                     : FOR j IN 1 TO 3 GENERATE
-- Distribute 4 x ( 2 fibres x 9 depths ) to 4 x ( 6 or 8 phi-sectors x 3 depths )
      TriggerCellDistributionServerInstance : ENTITY TriggerCell.DistributionServer
      PORT MAP(
        clk     => clk ,
        DataIn  => lInputGroups1( i )( 6 * ( j-1 ) TO( 6 * j ) -1 ) ,
        DataOut => lOutputGroups1( i )( cOutputSizes( i ) * ( j-1 ) TO( cOutputSizes( i ) * j ) -1 )
      );
    END GENERATE;
  END GENERATE;

-- Map 4 x ( 6 or 8 phi-sectors x 3 depths ) to 4 x ( 3 depths x 6 or 8 phi-sectors )
  g3      : FOR i IN 0 TO 3 GENERATE
    g3a   : FOR j IN 0 TO cOutputSizes( i ) -1 GENERATE
      g3b : FOR k IN 0 TO 2 GENERATE
        PROCESS( clk )
          VARIABLE lCell : tData := cNull;
        BEGIN
          IF RISING_EDGE( clk ) THEN
            lCell       := lOutputGroups1( i )( ( cOutputSizes( i ) * k ) + j );

-- Integer divide range 0 to 170 by 57 maps into 3 fine regions           
-- lCell.SortKey := ( ( TO_INTEGER( lCell.Phi ) MOD 512 ) MOD 86 ) / 29;

          lCell.SortKey := TO_INTEGER( lCell.Phi ) / 29;
          lCell.Phi     := TO_SIGNED( ( TO_INTEGER( lCell.Phi ) MOD 29 ) , 13 );

-- Need handling of overlap regions          

            lInputGroups2( i )( ( 3 * j ) + k ) <= lCell;
          END IF;
        END PROCESS;
      END GENERATE;
    END GENERATE;
  END GENERATE;

-- Second distribution layer
  g4                                        : FOR i IN 0 TO 3 GENERATE
    g4a                                     : FOR j IN 1 TO cOutputSizes( i ) GENERATE
-- Distribute 4 x ( 3 depths x 6 or 8 phi-sectors ) to 4 x ( 1 depths x 18 or 24 phi-sectors )
      TriggerCellDistributionServerInstance : ENTITY TriggerCell.DistributionServer
      PORT MAP(
        clk     => clk ,
        DataIn  => lInputGroups2( i )( 3 * ( j-1 ) TO( 3 * j ) -1 ) ,
        DataOut => lOutputGroups2( i )( 3 * ( j-1 ) TO( 3 * j ) -1 )
      );
    END GENERATE;
  END GENERATE;

-- Map 4 x ( 1 depths x 18 or 24 phi-sectors ) to flat ( 72 phi-sectors )
  Output        <= lOutputGroups2( 0 )( 0 TO 17 ) & lOutputGroups2( 1 )( 0 TO 17 ) & lOutputGroups2( 2 )( 0 TO 17 ) & lOutputGroups2( 3 )( 0 TO 17 );
-- -------------------------------------------------------------------------


  Intermediate1 <= lInputGroups1( 0 )( 0 TO 17 ) & lInputGroups1( 1 )( 0 TO 17 ) & lInputGroups1( 2 )( 0 TO 17 ) & lInputGroups1( 3 )( 0 TO 17 );
  Intermediate2 <= lOutputGroups1( 0 )( 0 TO 17 ) & lOutputGroups1( 1 )( 0 TO 17 ) & lOutputGroups1( 2 )( 0 TO 17 ) & lOutputGroups1( 3 )( 0 TO 17 );
  Intermediate3 <= lOutputGroups2( 0 )( 0 TO 17 ) & lOutputGroups2( 1 )( 0 TO 17 ) & lOutputGroups2( 2 )( 0 TO 17 ) & lOutputGroups2( 3 )( 0 TO 17 );

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY TriggerCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance1 : ENTITY TriggerCell.Debug
  GENERIC MAP( "TriggerCellDistributionInt1" )
  PORT MAP( clk , Intermediate1 );

  DebugInstance2 : ENTITY TriggerCell.Debug
  GENERIC MAP( "TriggerCellDistributionInt2" )
  PORT MAP( clk , Intermediate2 );

  DebugInstance3 : ENTITY TriggerCell.Debug
  GENERIC MAP( "TriggerCellDistributionInt3" )
  PORT MAP( clk , Intermediate3 );

  DebugInstance : ENTITY TriggerCell.Debug
  GENERIC MAP( "TriggerCellDistribution" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------


END rtl;
