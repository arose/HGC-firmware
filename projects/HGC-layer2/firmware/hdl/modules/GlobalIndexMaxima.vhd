-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY GlobalIndexMaxima IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END GlobalIndexMaxima;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF GlobalIndexMaxima IS

  TYPE tIndices IS ARRAY( natural range <> ) OF NATURAL;
  SIGNAL Indices  :tIndices( 0 TO 71 )  := ( OTHERS => 0 );
  
  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );

BEGIN

-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 3 GENERATE
  BEGIN
  
    PROCESS( clk )
      VARIABLE Counter : INTEGER RANGE 0 TO 511 := 1;
      VARIABLE LocalIndices : tIndices( 0 TO 17 )  := ( OTHERS => 0 );
    BEGIN
      IF RISING_EDGE( clk ) THEN
      
        Counter := 0;
        FOR j IN 0 TO 17 LOOP
          if not CellPipeIn( PipeOffset + 0 )( (18*i)+j ).FrameValid then
            LocalIndices( j ) := 0;          
            Counter := 1;
          elsif CellPipeIn( PipeOffset + 0 )( (18*i)+j ).S > 0 then
            LocalIndices( j ) := Counter;
            Counter := ( Counter + 1 ) MOD 512;
          else
            LocalIndices( j ) := 0;          
          end if; 
        END LOOP;

        Indices( (18*i) TO (18*i)+17 ) <= LocalIndices;
        
      END IF;
    END PROCESS;
  END GENERATE;
    
-- Step 2
  g2 : FOR i IN 0 TO 71 GENERATE
  BEGIN
  
    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        if Indices( i ) > 100 then
          Output( i ) <= cNull;
        else
          Output( i ) <= CellPipeIn( PipeOffset + 1 )( i );
          Output( i ).Index <= Indices( i ) + ( 100 * (i/18) );            
        end if;          

        Output( i ) .SortKey    <= CellPipeIn( PipeOffset + 1 )( i ).SortKey;
        Output( i ) .DataValid  <= CellPipeIn( PipeOffset + 1 )( i ).DataValid;
        Output( i ) .FrameValid <= CellPipeIn( PipeOffset + 1 )( i ).FrameValid;
        
      END IF;
    END PROCESS;
  END GENERATE;          

-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "GlobalIndexMaxima" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
