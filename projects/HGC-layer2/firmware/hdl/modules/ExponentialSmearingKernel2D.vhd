-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY ExponentialSmearingKernel2D IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END ExponentialSmearingKernel2D;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF ExponentialSmearingKernel2D IS
  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );
BEGIN

-- -------------------------------------------------------------------------
  g1                                            : FOR i IN 0 TO 71 GENERATE
    SIGNAL ValidCell0 , ValidCell1 , ValidCell2 : tData := cNull;
  BEGIN

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        IF CellPipeIn( PipeOffset )( i ) .DataValid THEN
          ValidCell0 <= CellPipeIn( PipeOffset )( i );
        ELSE
          ValidCell0 <= cNull;
        END IF;


        IF ValidCell0.DataValid OR CellPipeIn( PipeOffset + 5 )( i ) .DataValid OR CellPipeIn( PipeOffset + 9 )( i ) .DataValid THEN
          ValidCell1 <= ValidCell0;
          ValidCell2 <= ValidCell1;
        END IF;

        Output( i )             <= ( ValidCell1 ) + ( ValidCell0 / 2 ) + ( ValidCell2 / 2 ); -- Sortkey comes from "left" entry of sum
        Output( i ) .DataValid  <= CellPipeIn( PipeOffset + 5 )( i ) .DataValid;
        Output( i ) .FrameValid <= CellPipeIn( PipeOffset + 5 )( i ) .FrameValid;

      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "ExponentialSmearingKernel2D" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
