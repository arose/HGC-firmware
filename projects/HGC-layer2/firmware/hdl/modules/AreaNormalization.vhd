-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell
-- .include ReuseableElements/DataPipe.vhd in HistogramCell
-- .include ReuseableElements/Debugger.vhd in HistogramCell

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType.ALL;
USE HistogramCell.ArrayTypes.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY AreaNormalization IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk         : IN STD_LOGIC := '0'; -- The algorithm clock
    CellPipeIn  : IN VectorPipe;
    CellPipeOut : OUT VectorPipe
  );
END AreaNormalization;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF AreaNormalization IS
  SIGNAL Output : Vector( 0 TO 71 ) := NullVector( 72 );

  TYPE tCalibrationConstants IS ARRAY( 0 TO 39 ) OF INTEGER RANGE 0 TO( 2 ** 18 ) -1;
  CONSTANT cCalibrationConstants : tCalibrationConstants := ( 239097 , 257998 , 237359 , 219776 , 250090 , 233956 , 219776 , 207218 , 252022 , 239097 , 227434 , 216856 , 207218 , 198400 , 190302 , 255975 , 246315 , 237359 , 229030 , 221266 , 214012 , 207218 , 200842 , 194847 , 189199 , 183869 , 178832 , 174063 , 169542 , 165250 , 161169 , 262143 , 255975 , 250090 , 244470 , 239097 , 233956 , 229030 , 224308 , 219776 );

BEGIN

-- -------------------------------------------------------------------------
  g1                           : FOR i IN 0 TO 71 GENERATE
    SIGNAL CalibrationConstant : INTEGER RANGE 0 TO( 2 ** 18 ) -1 := 0;
  BEGIN

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN

        CalibrationConstant <= cCalibrationConstants( CellPipeIn( PipeOffset + 0 )( i ) .SortKey );

        Output( i )         <= CellPipeIn( PipeOffset + 1 )( i ) * CalibrationConstant;

      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY HistogramCell.DataPipe
  PORT MAP( clk , Output , CellPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  DebugInstance : ENTITY HistogramCell.Debug
  GENERIC MAP( "AreaNormalization" )
  PORT MAP( clk , Output );
-- -------------------------------------------------------------------------

END rtl;
