-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC

-- .include components/PkgTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in TriggerCell

-- .include components/PkgHistogramCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in HistogramCell

-- .include components/PkgLocalTriggerCell.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in LocalTriggerCell

-- .include components/PkgDomainOfInfluence.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in DomainOfInfluence

-- .include TopLevelInterfaces/mp7_data_types.vhd

-- .include modules/LinksToTriggerCells.vhd
-- .include modules/TriggerCellDistribution.vhd
-- .include modules/TriggerCellToHistogramCell.vhd
-- .include modules/Histogram.vhd
-- .include modules/ExponentialSmearingKernel1D.vhd
-- .include modules/AreaNormalization.vhd
-- .include modules/ExponentialSmearingKernel2D.vhd
-- -- .include modules/MaximaFinder1D.vhd
-- -- .include modules/MaximaFinder2D.vhd
-- -- .include modules/MaximaInterleaver.vhd
-- .include modules/SimpleMaximaFinder.vhd
-- .include modules/CalculateAveragePositions.vhd
-- .include modules/DomainOfInfluence2D.vhd
-- .include modules/Clusterizer.vhd


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY Interfaces;
USE Interfaces.mp7_data_types.ALL;

LIBRARY TriggerCell;
USE TriggerCell.DataType;
USE TriggerCell.ArrayTypes;

LIBRARY LocalTriggerCell;
USE LocalTriggerCell.DataType;
USE LocalTriggerCell.ArrayTypes;

LIBRARY HistogramCell;
USE HistogramCell.DataType;
USE HistogramCell.ArrayTypes;

LIBRARY DomainOfInfluence;
USE DomainOfInfluence.DataType;
USE DomainOfInfluence.ArrayTypes;

LIBRARY Cluster;
USE Cluster.DataType;
USE Cluster.ArrayTypes;


LIBRARY HGC;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY HgcProcessorTop IS
  PORT(
    clk             : IN STD_LOGIC; -- The algorithm clock
    LinksIn         : IN ldata( 71 DOWNTO 0 )                           := ( OTHERS => LWORD_NULL );
    LinksOut        : OUT ldata( 71 DOWNTO 0 )                          := ( OTHERS => LWORD_NULL );
-- Prevent all the logic being synthesized away when running standalone
    DebuggingOutput : OUT LocalTriggerCell.ArrayTypes.Vector( 0 TO 71 ) := LocalTriggerCell.ArrayTypes.NullVector( 72 )
  );
END HgcProcessorTop;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF HgcProcessorTop IS

  SUBTYPE tTriggerCellPipe IS TriggerCell.ArrayTypes.VectorPipe( 0 TO 16 )( 0 TO 71 );
  CONSTANT NullTriggerCellPipe : tTriggerCellPipe := TriggerCell.ArrayTypes.NullVectorPipe( 17 , 72 );

  SUBTYPE tLocalTriggerCellPipe IS LocalTriggerCell.ArrayTypes.VectorPipe( 0 TO 16 )( 0 TO 71 );
  CONSTANT NullLocalTriggerCellPipe : tLocalTriggerCellPipe := LocalTriggerCell.ArrayTypes.NullVectorPipe( 17 , 72 );

  SUBTYPE tHistogramCellPipe IS HistogramCell.ArrayTypes.VectorPipe( 0 TO 16 )( 0 TO 71 );
  CONSTANT NullHistogramCellPipe : tHistogramCellPipe := HistogramCell.ArrayTypes.NullVectorPipe( 17 , 72 );

  SUBTYPE tDomainOfInfluencePipe IS DomainOfInfluence.ArrayTypes.VectorPipe( 0 TO 16 )( 0 TO 71 );
  CONSTANT NullDomainOfInfluencePipe : tDomainOfInfluencePipe := DomainOfInfluence.ArrayTypes.NullVectorPipe( 17 , 72 );

  SUBTYPE tClusterPipe IS Cluster.ArrayTypes.VectorPipe( 0 TO 16 )( 0 TO 71 );
  CONSTANT NullClusterPipe : tClusterPipe := Cluster.ArrayTypes.NullVectorPipe( 17 , 72 );

  
  SIGNAL InputPipe                   : tTriggerCellPipe       := NullTriggerCellPipe;
  SIGNAL DistributedPipe             : tTriggerCellPipe       := NullTriggerCellPipe;
  SIGNAL WeightedPositionPipe        : tHistogramCellPipe     := NullHistogramCellPipe;
  SIGNAL LocalCellPipe               : tLocalTriggerCellPipe  := NullLocalTriggerCellPipe;
  SIGNAL HistogramCellPipe           : tHistogramCellPipe     := NullHistogramCellPipe;
  SIGNAL SmearedCell1dPipe           : tHistogramCellPipe     := NullHistogramCellPipe;
  SIGNAL NormalizedCell1dPipe        : tHistogramCellPipe     := NullHistogramCellPipe;
  SIGNAL SmearedCell2dPipe           : tHistogramCellPipe     := NullHistogramCellPipe;
-- SIGNAL Maxima1dPipe            : tHistogramCellPipe := NullHistogramCellPipe;
-- SIGNAL Maxima2dPipe            : tHistogramCellPipe := NullHistogramCellPipe;
-- SIGNAL MaximaInterleavedPipe   : tHistogramCellPipe := NullHistogramCellPipe;
-- SIGNAL tTriggerCellPipe : tHistogramCellPipe := NullHistogramCellPipe;

  SIGNAL SimpleMaximaPipe            : tHistogramCellPipe     := NullHistogramCellPipe;
  SIGNAL MaximaCentrePipe            : tHistogramCellPipe     := NullHistogramCellPipe;
  SIGNAL IndexedMaximaPipe           : tHistogramCellPipe     := NullHistogramCellPipe;
  
  SIGNAL DoiPipe                     : tDomainOfInfluencePipe := NullDomainOfInfluencePipe;
  SIGNAL ClusteredLocalCellPipe      : tLocalTriggerCellPipe  := NullLocalTriggerCellPipe;

  SIGNAL ClusterPipe                 : tClusterPipe           := NullClusterPipe;
  SIGNAL ClusterSumPipe              : tClusterPipe           := NullClusterPipe;
  
  
  
BEGIN

-- -------------------------------------------------------------------------
  LinksToTriggerCellsInstance : ENTITY HGC.LinksToTriggerCells
  PORT MAP(
    clk         => clk ,
    linksIn     => LinksIn ,
    CellPipeOut => InputPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  TriggerCellDistributionInstance : ENTITY HGC.TriggerCellDistribution
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => InputPipe ,
    CellPipeOut => DistributedPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  TriggerCellToHistogramCellInstance : ENTITY HGC.TriggerCellToHistogramCell
  PORT MAP(
    clk                     => clk ,
    CellPipeIn              => DistributedPipe ,
    HistogramCellPipeOut    => WeightedPositionPipe ,
    LocalTriggerCellPipeOut => LocalCellPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  HistogramInstance : ENTITY HGC.Histogram
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => WeightedPositionPipe ,
    CellPipeOut => HistogramCellPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  ExponentialSmearingKernel1DInstance : ENTITY HGC.ExponentialSmearingKernel1D
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => HistogramCellPipe ,
    CellPipeOut => SmearedCell1dPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  AreaNormalizationInstance : ENTITY HGC.AreaNormalization
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => SmearedCell1dPipe ,
    CellPipeOut => NormalizedCell1dPipe
  );
-- -------------------------------------------------------------------------  
-- -------------------------------------------------------------------------
  ExponentialSmearingKernel2DInstance : ENTITY HGC.ExponentialSmearingKernel2D
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => NormalizedCell1dPipe ,
    CellPipeOut => SmearedCell2dPipe
  );
-- -------------------------------------------------------------------------  
-- -- -------------------------------------------------------------------------
-- MaximaFinder1DInstance : ENTITY HGC.MaximaFinder1D
-- PORT MAP(
-- clk         => clk ,
-- CellPipeIn  => SmearedCell2dPipe ,
-- CellPipeOut => Maxima1dPipe
-- );
-- -- -------------------------------------------------------------------------
-- -- -------------------------------------------------------------------------
-- MaximaFinder2DInstance : ENTITY HGC.MaximaFinder2D
-- PORT MAP(
-- clk         => clk ,
-- CellPipeIn  => Maxima1dPipe ,
-- CellPipeOut => Maxima2dPipe
-- );
-- -- -------------------------------------------------------------------------
-- -- -------------------------------------------------------------------------
-- MaximaInterleaverInstance : ENTITY HGC.MaximaInterleaver
-- PORT MAP(
-- clk         => clk ,
-- CellPipeIn  => Maxima2dPipe ,
-- CellPipeOut => MaximaInterleavedPipe
-- );
-- -- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  SimpleMaximaFinderInstance : ENTITY HGC.SimpleMaximaFinder
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => SmearedCell2dPipe ,
    CellPipeOut => SimpleMaximaPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  CalculateAveragePositionsInstance : ENTITY HGC.CalculateAveragePositions
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => SimpleMaximaPipe ,
    CellPipeOut => MaximaCentrePipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  GlobalIndexMaximaInstance : ENTITY HGC.GlobalIndexMaxima
  PORT MAP(
    clk         => clk ,
    CellPipeIn  => MaximaCentrePipe ,
    CellPipeOut => IndexedMaximaPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  DomainOfInfluence2DInstance : ENTITY HGC.DomainOfInfluence2D
  PORT MAP(
    clk        => clk ,
    CellPipeIn => IndexedMaximaPipe ,
    DoiPipeOut => DoiPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  ClusterizerInstance : ENTITY HGC.Clusterizer
  PORT MAP(
    clk              => clk ,
    LocalCellPipeIn  => LocalCellPipe ,
    DoiPipeIn        => DoiPipe ,
    LocalCellPipeOut => ClusteredLocalCellPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  LocalCellToClusterInstance : ENTITY HGC.LocalCellToCluster
  PORT MAP(
    clk              => clk ,
    LocalCellPipeIn  => ClusteredLocalCellPipe , 
    ClusterPipeOut   => ClusterPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  ClusterSumInstance : ENTITY HGC.ClusterSum
  PORT MAP(
    clk            => clk ,
    ClusterPipeIn  => ClusterPipe , 
    ClusterPipeOut => ClusterSumPipe
  );
-- -------------------------------------------------------------------------


-- MAKE SURE THIS IS THE LAST ENTRY IN THE CHAIN FOR STANDALONE SYNTHESIS
  DebuggingOutput <= ClusteredLocalCellPipe( 0 );

END rtl;
