-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HGC
-- .include TopLevelInterfaces/mp7_data_types.vhd
-- .include ReuseableElements/PkgDebug.vhd
-- .include ReuseableElements/PkgUtilities.vhd
-- .include components/PkgConstants.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY HGC;
USE HGC.constants.ALL;

LIBRARY Interfaces;
USE Interfaces.mp7_data_types.ALL;

LIBRARY Utilities;
USE Utilities.debugging.ALL;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY DummyData IS
PORT( clk        : IN STD_LOGIC;
        LinkData : OUT ldata( 71 DOWNTO 0 ) := ( OTHERS => LWORD_NULL )
     );
END ENTITY DummyData;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF DummyData IS
BEGIN
  references               : PROCESS( clk )
    VARIABLE SEED1 , SEED2 : POSITIVE := 123108263;
    VARIABLE RandBit       : STD_LOGIC;
    VARIABLE RandR         : UNSIGNED( 10 DOWNTO 0 );
    VARIABLE RandPhi       : SIGNED( 9 DOWNTO 0 );
    VARIABLE E_real        : REAL;
  BEGIN

    IF( RISING_EDGE( clk ) ) THEN

-- FOR i IN 0 TO 71 LOOP
-- LinkData( i ) <= LWORD_NULL;

-- -- Frame Valid
-- IF SimulationClockCounter < ( cTimeMultiplexingPeriod * cFramesPerBx ) -6 THEN
-- LinkData( i ) .valid <= '1';

-- -- IF SimulationClockCounter > 5 AND SimulationClockCounter < 216 THEN
-- -- IF (SimulationClockCounter > 0) and (SimulationClockCounter < 5) THEN
-- -- IF( SimulationClockCounter = 5 ) THEN

-- -- if i mod 8 = 0 then
-- IF i = 5 THEN

-- -- Phi            
-- SET_RANDOM_VAR( SEED1 , SEED2 , RandPhi );

-- CASE i MOD 8 IS
-- WHEN 0 | 1  => RandPhi( 9 ) := '1'; --RandBit; -- CURRENTLY NOTHING IN THE OVERLAP!
-- WHEN 2 | 3  => RandPhi( 9 ) := '0';
-- WHEN 4 | 5  => RandPhi( 9 ) := '1';
-- WHEN 6 | 7  => RandPhi( 9 ) := '0'; --RandBit; -- CURRENTLY NOTHING IN THE OVERLAP! 
-- WHEN OTHERS => NULL; -- Can't believe I'm having to specify this...
-- END CASE;

-- LinkData( i ) .data( 20 DOWNTO 11 ) <= STD_LOGIC_VECTOR( RandPhi );
-- --LinkData( i ) .data( 15 DOWNTO 11 ) <= STD_LOGIC_VECTOR( RandPhi( 4 downto 0 ) );


-- -- R_over_Z
-- -- SET_RANDOM_VAR( SEED1 , SEED2 , RandR );
-- -- RandR := UNSIGNED( LinkData( i ) .data( 10 DOWNTO 0 ) ) + RandR( 4 DOWNTO 0 );
-- if LinkData( i ) .data( 10 DOWNTO 0 ) /= "00000000000" then
-- RandR := UNSIGNED( LinkData( i ) .data( 10 DOWNTO 0 ) ) + TO_UNSIGNED( 7 , 11 );
-- else
-- RandR := TO_UNSIGNED( 256 , 11 );            
-- end if;
-- LinkData( i ) .data( 10 DOWNTO 0 ) <= STD_LOGIC_VECTOR( RandR );

-- -- Energy

-- if ( RandR < 1500 ) then
-- -- E_Real := ( ( ( real( to_integer( RandR ) ) - 300.0 ) / 32.0 ) ** 2 );
-- -- E_Real := E_Real + ( ( ( real( to_integer( RandPhi ) ) - 128.0 ) / 32.0 ) ** 2 );
-- -- E_Real := 255.0 * exp( - E_Real );
-- E_real := 64.0;
-- LinkData( i ) .data( 31 DOWNTO 24 ) <= STD_LOGIC_VECTOR( TO_UNSIGNED( INTEGER( E_Real ) , 8 ) );
-- end if;


-- END IF;
-- -- END IF;
-- END IF;
-- END LOOP;


      FOR i IN 0 TO 71 LOOP
        LinkData( i ) <= LWORD_NULL;

-- Frame Valid
        IF SimulationClockCounter < ( cTimeMultiplexingPeriod * cFramesPerBx ) -6 THEN
          LinkData( i ) .valid <= '1';

          IF ( i = 55 ) AND ( SimulationClockCounter = 30 OR SimulationClockCounter = 31 ) THEN
            LinkData( i ) .data( 20 DOWNTO 11 ) <= STD_LOGIC_VECTOR( 10x"0AB" + TO_UNSIGNED( 90 * (SimulationClockCounter-30) , 10 ) ); -- Phi
            LinkData( i ) .data( 10 DOWNTO 0 )  <= 11x"2CD"; -- R
            LinkData( i ) .data( 31 DOWNTO 24 ) <= 8x"1F"; -- Energy

          END IF;
-- END IF;
        END IF;
      END LOOP;


      SimulationClockCounter <= SimulationClockCounter + 1;

    END IF;
  END PROCESS;
END ARCHITECTURE rtl;
