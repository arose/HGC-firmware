-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library LocalTriggerCell
-- .include ReuseableElements/PkgUtilities.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

LIBRARY Utilities;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE DataType IS

-- -------------------------------------------------------------------------       
  TYPE tData IS RECORD
    R_over_Z   : UNSIGNED( 10 DOWNTO 0 );
    Layer      : UNSIGNED( 5 DOWNTO 0 );
    Energy     : UNSIGNED( 7 DOWNTO 0 );
    Phi        : UNSIGNED( 4 DOWNTO 0 );
-- -------------------------------------------------------------------------       
-- Utility field, used for the key in the distribution server and the row index in the histogram
    SortKey    : INTEGER RANGE 0 TO 511;
    DeltaR2    : NATURAL;
-- -------------------------------------------------------------------------       
    DataValid  : BOOLEAN;
    FrameValid : BOOLEAN;
  END RECORD;

  CONSTANT cNull : tData := (
    (OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , -- R-over-Z , Layer , Energy , Phi
    0 , 0 ,  -- SortKey , DeltaR2
    FALSE , FALSE ); -- DataValid , FrameValid

  FUNCTION ToStdLogicVector( aData     : tData ) RETURN STD_LOGIC_VECTOR;
  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData;

  FUNCTION WriteHeader RETURN STRING;
  FUNCTION WriteData( aData : tData ) RETURN STRING;

  ATTRIBUTE SIZE            : NATURAL;
  ATTRIBUTE SIZE OF tData   : TYPE IS 36; -- Width into RAM
-- -------------------------------------------------------------------------       

END DataType;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
PACKAGE BODY DataType IS

  FUNCTION ToStdLogicVector( aData : tData ) RETURN STD_LOGIC_VECTOR IS
    VARIABLE lRet                  : STD_LOGIC_VECTOR( ( tData'SIZE-1 ) DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN
    lRet( 4 DOWNTO 0 )   := STD_LOGIC_VECTOR( aData.R_over_Z( 4 DOWNTO 0 ) );
    lRet( 9 DOWNTO 5 )   := STD_LOGIC_VECTOR( aData.Phi );
    lRet( 15 DOWNTO 10 ) := STD_LOGIC_VECTOR( aData.Layer );
    lRet( 23 DOWNTO 16 ) := STD_LOGIC_VECTOR( aData.Energy );
    lRet( 32 DOWNTO 24 ) := STD_LOGIC_VECTOR( TO_UNSIGNED( aData.SortKey , 9 ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData IS
    VARIABLE lRet                      : tData := cNull;
  BEGIN
    lRet.R_over_Z( 4 DOWNTO 0 ) := UNSIGNED( aStdLogicVector( 4 DOWNTO 0 ) );
    lRet.Phi      := UNSIGNED( aStdLogicVector( 9 DOWNTO 5 ) );
    lRet.Layer    := UNSIGNED( aStdLogicVector( 15 DOWNTO 10 ) );
    lRet.Energy   := UNSIGNED( aStdLogicVector( 23 DOWNTO 16 ) );
    lRet.SortKey  := TO_INTEGER( UNSIGNED( aStdLogicVector( 32 DOWNTO 24 ) ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION WriteHeader RETURN STRING IS
    VARIABLE aLine : LINE;
  BEGIN
    WRITE( aLine , STRING' ( "R_over_Z" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Layer" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Energy" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Phi" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "SortKey" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "FrameValid" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "DataValid" ) , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteHeader;

  FUNCTION WriteData( aData : tData ) RETURN STRING IS
    VARIABLE aLine          : LINE;
  BEGIN
    WRITE( aLine , TO_INTEGER( aData.R_over_Z ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Layer ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Energy ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Phi ) , RIGHT , 15 );
    WRITE( aLine , aData.SortKey , RIGHT , 15 );
    WRITE( aLine , aData.FrameValid , RIGHT , 15 );
    WRITE( aLine , aData.DataValid , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteData;

END DataType;
