-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library TriggerCell
-- .include ReuseableElements/PkgUtilities.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

LIBRARY Utilities;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE DataType IS

-- -------------------------------------------------------------------------       
  TYPE tData IS RECORD
    R_over_Z   : UNSIGNED( 10 DOWNTO 0 );
    Layer      : UNSIGNED( 5 DOWNTO 0 );
    Energy     : UNSIGNED( 7 DOWNTO 0 );
    Phi        : SIGNED( 12 DOWNTO 0 );
-- -------------------------------------------------------------------------       
-- Utility field, used for the key in the distribution server and the row index in the histogram
    SortKey    : INTEGER RANGE 0 TO 63;
-- -------------------------------------------------------------------------       
    DataValid  : BOOLEAN;
    FrameValid : BOOLEAN;
  END RECORD;

  CONSTANT cNull : tData := (
    (OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , -- R-over-Z , Layer , Energy , Phi
    0 , -- SortKey
    FALSE , FALSE ); -- DataValid , FrameValid

  FUNCTION ToStdLogicVector( aData     : tData ) RETURN STD_LOGIC_VECTOR;
  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData;

  FUNCTION WriteHeader RETURN STRING;
  FUNCTION WriteData( aData : tData ) RETURN STRING;

  ATTRIBUTE SIZE            : NATURAL;
  ATTRIBUTE SIZE OF tData   : TYPE IS 54; -- 36 + 18 if that means block-ram can be optimized 
-- -------------------------------------------------------------------------       

END DataType;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
PACKAGE BODY DataType IS

  FUNCTION ToStdLogicVector( aData : tData ) RETURN STD_LOGIC_VECTOR IS
    VARIABLE lRet                  : STD_LOGIC_VECTOR( ( tData'SIZE - 1 ) DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN
    lRet( 10 DOWNTO 0 )  := STD_LOGIC_VECTOR( aData.R_over_Z );
    lRet( 23 DOWNTO 11 ) := STD_LOGIC_VECTOR( aData.Phi );
    lRet( 29 DOWNTO 24 ) := STD_LOGIC_VECTOR( aData.Layer );
    lRet( 37 DOWNTO 30 ) := STD_LOGIC_VECTOR( aData.Energy );
    lRet( 43 DOWNTO 38 ) := STD_LOGIC_VECTOR( TO_UNSIGNED( aData.SortKey , 6 ) );
-- lRet( 70 )           := to_std_logic( aData.DataValid );
-- lRet( 71 )           := to_std_logic( aData.FrameValid );
    RETURN lRet;
  END FUNCTION;

  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData IS
    VARIABLE lRet                      : tData := cNull;
  BEGIN
    lRet.R_over_Z := UNSIGNED( aStdLogicVector( 10 DOWNTO 0 ) );
    lRet.Phi      := SIGNED( aStdLogicVector( 23 DOWNTO 11 ) );
    lRet.Layer    := UNSIGNED( aStdLogicVector( 29 DOWNTO 24 ) );
    lRet.Energy   := UNSIGNED( aStdLogicVector( 37 DOWNTO 30 ) );
    lRet.SortKey  := TO_INTEGER( UNSIGNED( aStdLogicVector( 43 DOWNTO 38 ) ) );
-- lRet.DataValid  := to_boolean( aStdLogicVector( 70 ) );
-- lRet.FrameValid := to_boolean( aStdLogicVector( 71 ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION WriteHeader RETURN STRING IS
    VARIABLE aLine : LINE;
  BEGIN
    WRITE( aLine , STRING' ( "R_over_Z" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Layer" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Energy" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Phi" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "SortKey" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "FrameValid" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "DataValid" ) , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteHeader;

  FUNCTION WriteData( aData : tData ) RETURN STRING IS
    VARIABLE aLine          : LINE;
  BEGIN
    WRITE( aLine , TO_INTEGER( aData.R_over_Z ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Layer ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Energy ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Phi ) , RIGHT , 15 );
    WRITE( aLine , aData.SortKey , RIGHT , 15 );
    WRITE( aLine , aData.FrameValid , RIGHT , 15 );
    WRITE( aLine , aData.DataValid , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteData;

END DataType;
