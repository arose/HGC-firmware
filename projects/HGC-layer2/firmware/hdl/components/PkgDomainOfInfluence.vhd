-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library DomainOfInfluence
-- .include ReuseableElements/PkgUtilities.vhd

-- .include components/PkgHistogramCell.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

LIBRARY Utilities;
USE Utilities.Utilities.ALL;

LIBRARY HistogramCell;
USE HistogramCell.DataType;

-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE DataType IS

-- -------------------------------------------------------------------------       
  TYPE tDoi IS RECORD
    Calibration : UNSIGNED( 8 DOWNTO 0 );
    X           : SIGNED( 8 DOWNTO 0 );
    Y           : SIGNED( 7 DOWNTO 0 );
    Index       : INTEGER RANGE 0 to 511;
    DataValid   : BOOLEAN;
  END RECORD;

  CONSTANT cNullDoi : tDoi := ( 9x"100" , ( OTHERS => '0' ) , ( OTHERS => '0' ) , 0 , FALSE );

  TYPE tDoiArray IS ARRAY( INTEGER RANGE <> ) OF tDoi;

  TYPE tData     IS RECORD
    Data       : tDoiArray( 0 TO 2 );
    SortKey    : INTEGER RANGE 0 TO 127;
    DataValid  : BOOLEAN;
    FrameValid : BOOLEAN;
  END RECORD;


  CONSTANT cNull : tData := ( ( OTHERS => cNullDoi ) ,
    0 , FALSE , FALSE ); -- SortKey , DataValid , FrameValid 

  FUNCTION ToStdLogicVector( aData     : tData ) RETURN STD_LOGIC_VECTOR;
  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData;

  FUNCTION WriteHeader RETURN STRING;
  FUNCTION WriteData( aData : tData ) RETURN STRING;

  ATTRIBUTE SIZE            : NATURAL;
  ATTRIBUTE SIZE OF tData   : TYPE IS 108; -- 72 + 36 if that means block-ram can be optimized 
-- -------------------------------------------------------------------------       

END DataType;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
PACKAGE BODY DataType IS


  FUNCTION ToStdLogicVector( aData : tData ) RETURN STD_LOGIC_VECTOR IS
    VARIABLE x                     : INTEGER                          := 0;
    VARIABLE lRet                  : STD_LOGIC_VECTOR( 107 DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN

    FOR i IN 0 TO 2 LOOP
      x                            := 36 * i;
      lRet( x + 0 )                := TO_STD_LOGIC( aData.Data( i ) .DataValid );
      lRet( x + 9 DOWNTO x + 1 )   := STD_LOGIC_VECTOR( aData.Data( i ) .Calibration );
      lRet( x + 18 DOWNTO x + 10 ) := STD_LOGIC_VECTOR( aData.Data( i ) .X );
      lRet( x + 26 DOWNTO x + 19 ) := STD_LOGIC_VECTOR( aData.Data( i ) .Y );
      lRet( x + 35 DOWNTO x + 27 ) := STD_LOGIC_VECTOR( TO_UNSIGNED( aData.Data( i ) .Index , 9 ) );
    END LOOP;

    RETURN lRet;
  END FUNCTION;

  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData IS
    VARIABLE x                         : INTEGER := 0;
    VARIABLE lRet                      : tData   := cNull;
  BEGIN
    FOR i IN 0 TO 2 LOOP
      x                           := 36 * i;
      lRet.Data( i ) .DataValid   := TO_BOOLEAN( aStdLogicVector( x + 0 ) ); 
      lRet.Data( i ) .Calibration := UNSIGNED( aStdLogicVector( x + 9 DOWNTO x + 1 ) );
      lRet.Data( i ) .X           := SIGNED( aStdLogicVector( x + 18 DOWNTO x + 10 ) );
      lRet.Data( i ) .Y           := SIGNED( aStdLogicVector( x + 26 DOWNTO x + 19 ) );
      lRet.Data( i ) .Index       := TO_INTEGER( UNSIGNED( aStdLogicVector( x + 35 DOWNTO x + 27 ) ) );
    END LOOP;
    RETURN lRet;
  END FUNCTION;

  FUNCTION WriteHeader RETURN STRING IS
    VARIABLE aLine : LINE;
  BEGIN

    FOR i IN 0 TO 2 LOOP
      WRITE( aLine , STRING' ( "Calibration" ) , RIGHT , 15 );
      WRITE( aLine , STRING' ( "X" ) , RIGHT , 6 );
      WRITE( aLine , STRING' ( "Y" ) , RIGHT , 6 );
      WRITE( aLine , STRING' ( "Index" ) , RIGHT , 6 );
    END LOOP;

    WRITE( aLine , STRING' ( "SortKey" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "FrameValid" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "DataValid" ) , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteHeader;

  FUNCTION WriteData( aData : tData ) RETURN STRING IS
    VARIABLE aLine          : LINE;
  BEGIN
    FOR i IN 0 TO 2 LOOP
      WRITE( aLine , TO_INTEGER( aData.Data( i ) .Calibration ) , RIGHT , 15 );
      WRITE( aLine , TO_INTEGER( aData.Data( i ) .X ) , RIGHT , 6 );
      WRITE( aLine , TO_INTEGER( aData.Data( i ) .Y ) , RIGHT , 6 );
      WRITE( aLine , aData.Data( i ) .Index , RIGHT , 6 );
    END LOOP;

    WRITE( aLine , aData.SortKey , RIGHT , 15 );
    WRITE( aLine , aData.FrameValid , RIGHT , 15 );
    WRITE( aLine , aData.DataValid , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteData;

END DataType;
