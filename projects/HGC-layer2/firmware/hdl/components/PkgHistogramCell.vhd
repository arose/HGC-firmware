-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library HistogramCell
-- .include ReuseableElements/PkgUtilities.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

LIBRARY Utilities;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE DataType IS

-- -------------------------------------------------------------------------       
  TYPE tData IS RECORD
    S          : UNSIGNED( 17 DOWNTO 0 );
    X          : UNSIGNED( 17 DOWNTO 0 );
    Y          : UNSIGNED( 17 DOWNTO 0 );
    N          : INTEGER RANGE 0 TO 1023;
-- Utility field, used for the key in the distribution server and the row index in the histogram
    SortKey    : INTEGER RANGE 0 TO 63;
    Index      : INTEGER RANGE 0 TO 511;
-- -------------------------------------------------------------------------       
    DataValid  : BOOLEAN;
    FrameValid : BOOLEAN;
  END RECORD;

  CONSTANT cNull : tData := ( ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , 0 , -- S , X , Y , N
     0 , 0 , -- SortKey , Index
    FALSE , FALSE ); -- DataValid , FrameValid 

  FUNCTION "+" ( Left , Right          : tData ) RETURN tData;
  FUNCTION "/" ( Numerator             : tData ; Denomentator : NATURAL ) RETURN tData;
  FUNCTION "*" ( Numerator           : tData ; Factor : INTEGER ) RETURN tData;

  FUNCTION ToStdLogicVector( aData     : tData ) RETURN STD_LOGIC_VECTOR;
  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData;

  FUNCTION WriteHeader RETURN STRING;
  FUNCTION WriteData( aData : tData ) RETURN STRING;
-- -------------------------------------------------------------------------       

  ATTRIBUTE SIZE            : NATURAL;
  ATTRIBUTE SIZE OF tData   : TYPE IS 72; -- Width into RAM

END DataType;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
PACKAGE BODY DataType IS

  FUNCTION "+" ( Left , Right : tData ) RETURN tData IS
    VARIABLE lRet             : tData := cNull;
  BEGIN
    lRet.S := Left.S + Right.S;
    lRet.X := Left.X + Right.X;
    lRet.Y := Left.Y + Right.Y;
    IF Left.N + Right.N > 1023 THEN
      lRet.N := 1023;
      REPORT "HISTOGRAM N-COUNT SATURATED" SEVERITY WARNING;
    ELSE
      lRet.N := Left.N + Right.N;
    END IF;
    lRet.SortKey    := Left.SortKey;
-- lRet.MaximaOffset := Left.MaximaOffset;
    lRet.FrameValid := Left.FrameValid;
    lRet.DataValid  := Left.DataValid;

    RETURN lRet;
  END FUNCTION "+";

  FUNCTION "/" ( Numerator : tData ; Denomentator : NATURAL ) RETURN tData IS
    VARIABLE lRet          : tData := cNull;
  BEGIN
    lRet.S := TO_UNSIGNED( TO_INTEGER( Numerator.S ) / Denomentator , 18 );
    lRet.X := ( OTHERS => '0' );
    lRet.Y := ( OTHERS => '0' );
    lRet.N := 0;

-- lRet.X            := TO_UNSIGNED( TO_INTEGER( Numerator.X ) / Denomentator , 18 );
-- lRet.Y            := TO_UNSIGNED( TO_INTEGER( Numerator.Y ) / Denomentator , 18 );
-- lRet.N            := Numerator.N / Denomentator;

-- lRet.SortKey      := Numerator.SortKey;
-- lRet.MaximaOffset := Numerator.MaximaOffset;
-- lRet.FrameValid   := Numerator.FrameValid;
-- lRet.DataValid    := Numerator.DataValid;

    RETURN lRet;
  END FUNCTION "/";

  FUNCTION "*" ( Numerator : tData ; Factor : INTEGER ) RETURN tData IS
    VARIABLE S , X , Y       : UNSIGNED( 35 DOWNTO 0 ) := ( OTHERS => '0' );
    VARIABLE lRet            : tData                   := cNull;
  BEGIN
    lRet   := Numerator;
    S      := Numerator.S * TO_UNSIGNED( Factor , 18 );
    lRet.S := S( 35 DOWNTO 18 );

-- S                 := Numerator.S * TO_UNSIGNED( Factor , 18 );
-- X                 := Numerator.X * TO_UNSIGNED( Factor , 18 );
-- Y                 := Numerator.Y * TO_UNSIGNED( Factor , 18 );
-- lRet.S            := S( 35 DOWNTO 18 );
-- lRet.X            := X( 35 DOWNTO 18 );
-- lRet.Y            := Y( 35 DOWNTO 18 );
-- lRet.N            := Numerator.N;

-- lRet.SortKey      := Numerator.SortKey;
-- lRet.MaximaOffset := Numerator.MaximaOffset;
-- lRet.FrameValid   := Numerator.FrameValid;
-- lRet.DataValid    := Numerator.DataValid;

    RETURN lRet;
  END FUNCTION "*";

  FUNCTION ToStdLogicVector( aData : tData ) RETURN STD_LOGIC_VECTOR IS
    VARIABLE lRet                  : STD_LOGIC_VECTOR( ( tData'SIZE-1 ) DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN
    lRet( 17 DOWNTO 0 )  := STD_LOGIC_VECTOR( aData.S );
    lRet( 35 DOWNTO 18 ) := STD_LOGIC_VECTOR( aData.X );
    lRet( 53 DOWNTO 36 ) := STD_LOGIC_VECTOR( aData.Y );
    lRet( 63 DOWNTO 54 ) := STD_LOGIC_VECTOR( TO_UNSIGNED( aData.N , 10 ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData IS
    VARIABLE lRet                      : tData := cNull;
  BEGIN
    lRet.S := UNSIGNED( aStdLogicVector( 17 DOWNTO 0 ) );
    lRet.X := UNSIGNED( aStdLogicVector( 35 DOWNTO 18 ) );
    lRet.Y := UNSIGNED( aStdLogicVector( 53 DOWNTO 36 ) );
    lRet.N := TO_INTEGER( UNSIGNED( aStdLogicVector( 63 DOWNTO 54 ) ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION WriteHeader RETURN STRING IS
    VARIABLE aLine : LINE;
  BEGIN
    WRITE( aLine , STRING' ( "S" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "X" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Y" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "N" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "SortKey" ) , RIGHT , 15 );
-- WRITE( aLine , STRING' ( "MaximaOffset" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "FrameValid" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "DataValid" ) , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteHeader;

  FUNCTION WriteData( aData : tData ) RETURN STRING IS
    VARIABLE aLine          : LINE;
  BEGIN
    WRITE( aLine , TO_INTEGER( aData.S ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.X ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Y ) , RIGHT , 15 );
    WRITE( aLine , aData.N , RIGHT , 15 );
    WRITE( aLine , aData.SortKey , RIGHT , 15 );
-- WRITE( aLine , aData.MaximaOffset , RIGHT , 15 );
    WRITE( aLine , aData.FrameValid , RIGHT , 15 );
    WRITE( aLine , aData.DataValid , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteData;

END DataType;
