-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library Cluster
-- .include ReuseableElements/PkgUtilities.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

LIBRARY Utilities;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE DataType IS

-- -------------------------------------------------------------------------       
  TYPE tData IS RECORD
    E     : UNSIGNED( 17 DOWNTO 0 );
    ERZ   : UNSIGNED( 20 DOWNTO 0 );
    ERZ2  : UNSIGNED( 20 DOWNTO 0 );
    EZ    : UNSIGNED( 20 DOWNTO 0 );
    EZ2   : UNSIGNED( 20 DOWNTO 0 );
    EPhi  : UNSIGNED( 20 DOWNTO 0 );
    EPhi2 : UNSIGNED( 20 DOWNTO 0 );
    SortKey : INTEGER RANGE 0 TO 511;
    DataValid  : BOOLEAN;
    FrameValid : BOOLEAN;
  END RECORD;

  CONSTANT cNull : tData := (
    (OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) , ( OTHERS => '0' ) ,
    0 , FALSE , FALSE ); -- DataValid , FrameValid

  FUNCTION ToStdLogicVector( aData     : tData ) RETURN STD_LOGIC_VECTOR;
  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData;

  FUNCTION WriteHeader RETURN STRING;
  FUNCTION WriteData( aData : tData ) RETURN STRING;

  ATTRIBUTE SIZE : NATURAL;
  ATTRIBUTE SIZE OF tData : TYPE IS 144;  
-- -------------------------------------------------------------------------       

END DataType;
-- -------------------------------------------------------------------------



-- -------------------------------------------------------------------------
PACKAGE BODY DataType IS

  FUNCTION ToStdLogicVector( aData : tData ) RETURN STD_LOGIC_VECTOR IS
    VARIABLE lRet                  : STD_LOGIC_VECTOR( 143 DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN
    lRet( 17 DOWNTO 0 )    := STD_LOGIC_VECTOR( aData.E );
    lRet( 38 DOWNTO 18 )   := STD_LOGIC_VECTOR( aData.ERZ );
    lRet( 59 DOWNTO 39 )   := STD_LOGIC_VECTOR( aData.ERZ2 );
    lRet( 80 DOWNTO 60 )   := STD_LOGIC_VECTOR( aData.EZ );
    lRet( 101 DOWNTO 81 )   := STD_LOGIC_VECTOR( aData.EZ2 );
    lRet( 122 DOWNTO 102 )  := STD_LOGIC_VECTOR( aData.EPhi );
    lRet( 143 DOWNTO 123 ) := STD_LOGIC_VECTOR( aData.EPhi2 );
    RETURN lRet;
  END FUNCTION;

  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData IS
    VARIABLE lRet                      : tData := cNull;
  BEGIN
    lRet.E     := UNSIGNED( aStdLogicVector( 17 DOWNTO 0 ) );
    lRet.ERZ   := UNSIGNED( aStdLogicVector( 38 DOWNTO 18 ) );
    lRet.ERZ2  := UNSIGNED( aStdLogicVector( 59 DOWNTO 39 ) );
    lRet.EZ    := UNSIGNED( aStdLogicVector( 80 DOWNTO 60 ) );
    lRet.EZ2   := UNSIGNED( aStdLogicVector( 101 DOWNTO 81 ) );
    lRet.EPhi  := UNSIGNED( aStdLogicVector( 122 DOWNTO 102 ) );
    lRet.EPhi2 := UNSIGNED( aStdLogicVector( 143 DOWNTO 123 ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION WriteHeader RETURN STRING IS
    VARIABLE aLine : LINE;
  BEGIN
    WRITE( aLine , STRING' ( "E" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "ERZ" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "ERZ2" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "EZ" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "EZ2" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "EPhi" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "EPhi2" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "FrameValid" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "DataValid" ) , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteHeader;

  FUNCTION WriteData( aData : tData ) RETURN STRING IS
    VARIABLE aLine          : LINE;
  BEGIN
    WRITE( aLine , TO_INTEGER( aData.E ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.ERZ ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.ERZ2 ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.EZ ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.EZ2 ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.EPhi ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.EPhi2 ) , RIGHT , 15 );
    WRITE( aLine , aData.FrameValid , RIGHT , 15 );
    WRITE( aLine , aData.DataValid , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteData;

END DataType;
