# EMP framework installation instructions

## Setup a python 2 environment

```
conda create -y -n py2 python=2.7
conda activate py2
```

## Setup ipbb

Repository: https://github.com/ipbus/ipbb

### Clone ipbb

Navigate to root directory of project (where emp-fwk is to be setup)

```
git clone https://github.com/ipbus/ipbb.git
```

### checkout relevant branch of ipbb

Once inside the ipbb directory, checkout latest tag:

```
cd ipbb
git checkout tags/v0.4.3 -b v0.4.3
cd ..
```

### Install ipbb

Installation from source will not work if the system-level python is not python 2. To go around this with conda, one should install ipbb from pip

```
pip install https://github.com/ipbus/ipbb/archive/<tag or
branch>.tar.gz # assumes that py2 env is active
```

For latest version at time of writing, execute:

```
pip install https://github.com/ipbus/ipbb/archive/v0.4.3.tar.gz
```

### Create a project with ipbb

```
ipbb init emp-root
cd emp-root/src
```

## Setup EMP framework

From `src` directory of project root, clone latest version of EMP

```
git clone https://gitlab.cern.ch/p2-xware/firmware/emp-fwk.git
```

Checkout tag v0.2.4

```
cd emp-fwk
git checkout tags/v0.2.4 -b v0.2.4
cd ..
```

# Setup firmware code

## Clone HGC-firmware

From EMP `src` directory, clone current repository
```
git clone https://gitlab.cern.ch/arose/HGC-firmware.git

```

## Change references to board and paths in firmware project

```
# navigate to firmware project directory
cd HGC-firmware/projects/VertexFinder/project/
```

Changes to apply:
- edit `project_config.yml` and change **ipbb** to absolute path to where it is installed
- edit `project_config.yml` to change **p2-xware** to absolute path to where emp-fwk is installed
- edit `project_config_serenity.yml` and change **ipbb** to absolute path to where it is installed
- edit `project_config_serenity.yml` to change **p2-xware** to absolute path to where emp-fwk is installed
- comment out PRJDIR from `add_files.tcl`
- modify `add_files.tcl` to change path under SRCDIR

## Add ku115 dependency to vertexing project

Edit `projects/VertexFinder/firmware/cfg/top_serenity.dep`, replacing line about ku115 with

``` 
include -c emp-fwk:boards/serenity/dc_ku115 dc_ku115_am1.dep
```

### Setup firmware dependencies


#### MP7 Firmware

From EMP `src` directory, clone mp7 firwmare

```
git clone https://gitlab.cern.ch/cms-cactus/firmware/mp7.git
```

### IPBUS Firmware

From EMP `src` directory, clone ipbus firwmare

```
git clone https://github.com/ipbus/ipbus-firmware.git
```

## Create vivado project

Navigate to firmware project location `HGC-firmware/projects/VertexFinder/project/`, source Vivado and initiate the project creation:

```
cd HGC-firmware/projects/VertexFinder/project/
source /DataStore/Xilinx/Vivado/2018.3/settings64.sh # this will depend on location of Vivado installation
python create_project.py -c project_config_serenity.yml
```

## Misc
### Validating project dependencies with ipbb

If ipbb complains that it cannot find certain project dependencies, one can check which packages are missing by navigating to the project location in emp-root and asking ipbb to report on dependencies:

```
cd emp-root/proj/VertexFinderSerenity72Links
ipbb dep report
```