if { ! [batch_mode] } {
  delete wave *
  config wave -signalnamewidth 1
  noview structure
  noview signals
  noview sim
  noview "Memory List"

  force sim:/debugging/TimeStamp [clock format [clock second]]
  
  add wave -dec sim:/debugging/SimulationClockCounter
  add wave -divider

}