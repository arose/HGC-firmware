# About
This directory contains scripts and utilities for testing the vertexing firmware.

# How To
To use the batch mode Modelsim testing, first of all clone the vertexing software from https://github.com/ImperialCollegeLondon/CMS\_P2\_VXF
Add the `analysis` directory to your PYTHONPATH or link the files to the current directory.

Generate a pattern file from a Python session like:
```python
import util
import Formats
test_events = util.events[...] # Slice a subset or use all 9000 events
Formats.writepfile('my_pattern_file.mp7', test_events)
```

Modify `sim_config.yml` to point to this file, and modify the number of events if needed (the simulation needs to know this to run for the correct amount of time).

Simulate the vertexing firmware with this file using Modelsim with `python run_test.py`.
The output of any enable Debug instances will be collected in a txt files.
The path is determined in `PkgDebug.vhd` in ReusableElements 

# Coming soon
Tools to analyse the output.
Import the debug file in Python with `pandas` like:
```python
import pandas
filename = '...'
debug_output = pandas.read_csv(filename, header=1, delim_whitespace=True)
```
