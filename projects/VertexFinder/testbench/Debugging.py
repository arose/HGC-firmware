import copy

# class Cell:
  # def __init__(self):
    # self.Clock = 0
    # self.Channel = 0
    # self.R_over_Z = 0
    # self.Layer = 0
    # self.Energy = 0
    # self.Phi = 0
    # self.Accumulator_S = 0
    # self.Accumulator_X = 0
    # self.Accumulator_Y = 0
    # self.Accumulator_N = 0
    # self.SortKey = 0
    # self.MaximaOffset = -7
    # self.FrameValid = True
    # self.DataValid = True

  # @classmethod    
  # def FromInt( cls , clk , i , j ):
    # lCell = cls()
    # lCell.Clock = (4*j)+clk
    # lCell.Channel = i
    # lCell.SortKey = j
    # return lCell
    
  # @classmethod    
  # def FromString( cls , data ):
    # data = data.split()
    # lCell = cls()
    # lCell.Clock = int( data[0] )
    # lCell.Channel = int( data[1] )
    # lCell.R_over_Z = int( data[2] )
    # lCell.Layer = int( data[3] )
    # lCell.Energy = int( data[4] )
    # lCell.Phi = int( data[5] )
    # lCell.Accumulator_S = int( data[6] )
    # lCell.Accumulator_X = int( data[7] )
    # lCell.Accumulator_Y = int( data[8] )
    # lCell.Accumulator_N = int( data[9] )
    # lCell.SortKey = int( data[10] )
    # lCell.MaximaOffset = int( data[11] )
    # lCell.FrameValid = ( data[12] == "TRUE" )
    # lCell.DataValid = ( data[13] == "TRUE" )
    # return lCell

  # def __iadd__( self , other ):
    # self.Accumulator_S += other.Accumulator_S
    # self.Accumulator_X += other.Accumulator_X
    # self.Accumulator_Y += other.Accumulator_Y
    # self.Accumulator_N += other.Accumulator_N  
    # return self

  # def __isub__( self , other ):
    # self.Accumulator_S -= other.Accumulator_S
    # self.Accumulator_X -= other.Accumulator_X
    # self.Accumulator_Y -= other.Accumulator_Y
    # self.Accumulator_N -= other.Accumulator_N  
    # return self

  # def __truediv__( self , other ):
    # ret = copy.deepcopy( self )
    # ret.Accumulator_S = int( self.Accumulator_S / other )
    # ret.Accumulator_X = int( self.Accumulator_X / other )
    # ret.Accumulator_Y = int( self.Accumulator_Y / other )
    # return ret

  # def __imul__( self , other ):
    # self.Accumulator_S = int( self.Accumulator_S * other / 262144 )
    # self.Accumulator_X = int( self.Accumulator_X * other / 262144 )
    # self.Accumulator_Y = int( self.Accumulator_Y * other / 262144 )
    # return self
    
  # def __eq__( self , other ):
    # return ( self.Channel == other.Channel ) \
    # and ( self.R_over_Z == other.R_over_Z ) \
    # and ( self.Layer == other.Layer ) \
    # and ( self.Energy == other.Energy ) \
    # and ( self.Phi == other.Phi ) \
    # and ( self.Accumulator_S == other.Accumulator_S ) \
    # and ( self.Accumulator_X == other.Accumulator_X ) \
    # and ( self.Accumulator_Y == other.Accumulator_Y ) \
    # and ( self.Accumulator_N == other.Accumulator_N ) \
    # and ( self.SortKey == other.SortKey ) \
    # and ( ( self.MaximaOffset == other.MaximaOffset ) or ( self.MaximaOffset < -4 ) ) \
    # and ( self.FrameValid == other.FrameValid ) \
    # and ( self.DataValid == other.DataValid ) #\
    # #and ( self.Clock == other.Clock ) 
    
  # def __str__(self):
    # return "".join( [ "{0:11}".format(x) for x in [ self.Clock , self.Channel , self.R_over_Z , self.Layer , self.Energy , self.Phi , self.Accumulator_S , self.Accumulator_X , self.Accumulator_Y , self.Accumulator_N , self.SortKey , self.MaximaOffset , self.FrameValid , self.DataValid ] ] )
    
# # ---------------------------------------------------------------------------------------
# def ReadCells( filename ):
  # with open( filename , "r" ) as f:
    # File = f.readline()  
    # Headers = f.readline().split()   
    # return [ Cell.FromString( l ) for l in f.readlines() ]

# def Compare( Name , Data, Expected ):
  # print( Name , ":", end=' ')
  # retval = True
  # for d , e in zip( Data , Expected ):
    # if ( d != e ):
      # retval = False
      # print( "Data Mismatch" )
      # print( "Data    ", d )
      # print( "Expected", e )
  # if retval:
    # print( "Match" )
  # return retval
# # ---------------------------------------------------------------------------------------


# # ---------------------------------------------------------------------------------------
# TriggerCellDistribution = ReadCells( "DebugFiles/TriggerCellDistribution.txt" )
# # ---------------------------------------------------------------------------------------

# # ---------------------------------------------------------------------------------------
# ExpectedPrecalculateWeightedPositions = copy.deepcopy( TriggerCellDistribution )
# for i in ExpectedPrecalculateWeightedPositions:
  # i.Clock += 1
  # i.Accumulator_S = i.Energy
  # i.Accumulator_X = i.Energy * i.Phi
  # i.Accumulator_Y = i.Energy * i.R_over_Z
  # i.Accumulator_N = 1
  # i.SortKey = int(( i.R_over_Z - 256 )/32)

# PrecalculateWeightedPositions = ReadCells( "DebugFiles/PrecalculateWeightedPositions.txt" )  
# Compare( "PrecalculateWeightedPositions" , PrecalculateWeightedPositions , ExpectedPrecalculateWeightedPositions )
# # ---------------------------------------------------------------------------------------
  
# # ---------------------------------------------------------------------------------------
# ExpectedHistogram = [ Cell.FromInt( 112 , i , j ) for j in range( 40 ) for i in range( 72 ) ]
                        
# for i in PrecalculateWeightedPositions: ExpectedHistogram[ (72*i.SortKey) + i.Channel ] += i
  
# Histogram = ReadCells( "DebugFiles/Histogram.txt" )[864:864+2880] 
# Compare( "Histogram" , Histogram , ExpectedHistogram ) 

# DroppedCells = ReadCells( "DebugFiles/HistogramDroppedCells.txt" ) 
# for i in DroppedCells: ExpectedHistogram[ (72*i.SortKey) + i.Channel ] -= i
# Compare( "Histogram excl. dropped cells" , Histogram , ExpectedHistogram ) 
# # ---------------------------------------------------------------------------------------

# # ---------------------------------------------------------------------------------------
# SumWidths = [ 6 , 5 , 5 , 5 , 4 , 4 , 4 , 4 , 3 , 3 , 3 , 3 , 3 , 3 , 3 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ]
# ExpectedExponentialSmearingKernel1D = copy.deepcopy( Histogram )

# for i in ExpectedExponentialSmearingKernel1D:
  # i.Clock += 4 
  
  # row = i.SortKey
  # col = i.Channel
          
  # index = (72*row) + col
  # scale = 2   
  
  # for k in range( 1 , SumWidths[row]+1 ):  
    # if (col - k) >= 0 :  i += ( Histogram[index - k] / scale )
    # if (col + k) <= 71 : i += ( Histogram[index + k] / scale )
    # scale *= 2
    
# ExponentialSmearingKernel1D = ReadCells( "DebugFiles/ExponentialSmearingKernel1D.txt" )[864:864+2880] 
# Compare( "ExponentialSmearingKernel1D" , ExponentialSmearingKernel1D , ExpectedExponentialSmearingKernel1D )       
# # ---------------------------------------------------------------------------------------

# # ---------------------------------------------------------------------------------------
# CalibrationConstants = [ 239097 , 257998 , 237359 , 219776 , 250090 , 233956 , 219776 , 207218 , 252022 , 239097 , 227434 , 216856 , 207218 , 198400 , 190302 , 255975 , 246315 , 237359 , 229030 , 221266 , 214012 , 207218 , 200842 , 194847 , 189199 , 183869 , 178832 , 174063 , 169542 , 165250 , 161169 , 262143 , 255975 , 250090 , 244470 , 239097 , 233956 , 229030 , 224308 , 219776 ]
# ExpectedAreaNormalization = copy.deepcopy( ExponentialSmearingKernel1D )

# for i in ExpectedAreaNormalization:
  # i.Clock += 2 
  # i *= CalibrationConstants[ i.SortKey ]
    
# AreaNormalization = ReadCells( "DebugFiles/AreaNormalization.txt" )[864:864+2880] 
# Compare( "AreaNormalization" , AreaNormalization , ExpectedAreaNormalization )       
# # ---------------------------------------------------------------------------------------
  
# # ---------------------------------------------------------------------------------------
# ExpectedExponentialSmearingKernel2D = copy.deepcopy( AreaNormalization )#[72:72+2736]

# for i in ExpectedExponentialSmearingKernel2D:
  # i.Clock += 5 
  
  # row = i.SortKey
  # col = i.Channel
  # index = (72*row) + col
          
  # if (row - 1) >= 0 :  i += ( AreaNormalization[index - 72] / 2 )
  # if (row + 1) <= 38 : i += ( AreaNormalization[index + 72] / 2 )
  
  # if i.Accumulator_N > 63 : i.Accumulator_N = 63
  
# ExponentialSmearingKernel2D = ReadCells( "DebugFiles/ExponentialSmearingKernel2D.txt" )[864:864+2880] 
# Compare( "ExponentialSmearingKernel2D" , ExponentialSmearingKernel2D , ExpectedExponentialSmearingKernel2D )       
# # ---------------------------------------------------------------------------------------

# # ---------------------------------------------------------------------------------------
# MaximaWidths = [ 6 , 5 , 5 , 5 , 4 , 4 , 4 , 4 , 3 , 3 , 3 , 3 , 3 , 3 , 3 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 2 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ]
# ExpectedMaximaFinder1D = [ Cell.FromInt( 127 , i , j ) for j in range( 40 ) for i in range( 72 ) ]
# for i in ExpectedMaximaFinder1D:
    
  # row = i.SortKey
  # col = i.Channel

  # # for j in range( 6 , -7 , -1 ):
  # for j in range( -7 , 6 ):
  
    # index = col + j
    # if index < 0 or index > 71 or j > MaximaWidths[ row ] or j < -MaximaWidths[ row ] :
      # if( i.Accumulator_S == 0 ):
        # i.MaximaOffset = -7
    # else :
      # lRef = ExponentialSmearingKernel2D[ (72*row) + index ]     
      # if( lRef.Accumulator_S > i.Accumulator_S ):
        # i.Accumulator_S = lRef.Accumulator_S
        # i.Accumulator_X = lRef.Accumulator_X
        # i.Accumulator_Y = lRef.Accumulator_Y
        # i.Accumulator_N = lRef.Accumulator_N
        # i.MaximaOffset = j
  
# MaximaFinder1D = ReadCells( "DebugFiles/MaximaFinder1D.txt" )[864:864+2880] 
# Compare( "MaximaFinder1D" , MaximaFinder1D , ExpectedMaximaFinder1D ) 
# # ---------------------------------------------------------------------------------------
  
# # ---------------------------------------------------------------------------------------
# ExpectedMaximaFinder2D =  copy.deepcopy( MaximaFinder1D )
# for i in ExpectedMaximaFinder2D:
    
  # row = i.SortKey
  # col = i.Channel
  
  # criteria0 = True
  # criteria1 = ( i.MaximaOffset == 0 )
  # criteria2 = True
  
  # if row > 0:
    # lRef = MaximaFinder1D[ (72*(row-1)) + col ]
    # criteria0 = ( i.Accumulator_S > lRef.Accumulator_S ) or ( ( i.Accumulator_S == lRef.Accumulator_S ) and lRef.MaximaOffset >= 0 )

  # if row < 39:
    # lRef = MaximaFinder1D[ (72*(row+1)) + col ]
    # criteria2 = ( i.Accumulator_S > lRef.Accumulator_S ) or ( ( i.Accumulator_S == lRef.Accumulator_S ) and lRef.MaximaOffset > 0 )
    
  # if criteria0 and criteria1 and criteria2 :
    # i.MaximaOffset = 1
  # else:
    # i.Accumulator_S = 0
    # i.Accumulator_X = 0
    # i.Accumulator_Y = 0
    # i.Accumulator_N = 0 
    # i.MaximaOffset = 0
    
# MaximaFinder2D = ReadCells( "DebugFiles/MaximaFinder2D.txt" )[864:864+2880] 
# Compare( "MaximaFinder2D" , MaximaFinder2D , ExpectedMaximaFinder2D )       
# # ---------------------------------------------------------------------------------------
 
 