import Formats
import random

#constants
nlinks = 72
nusedlinks = 27
empty_data = '0v00000000'
empty_frame = [empty_data] * nlinks
valid_frame_no_trk = Formats.Track({'z0':0, 'weight':0, 'framevalid':1, 'datavalid':0}).toVHex()
valid_frame = [valid_frame_no_trk] * nlinks

with open('pfile_one_trk.dat', 'w') as pfile:
  pfile.write(Formats.header(nlinks))
  # Write some empty, invalid frames to begin
  iFrame = 0
  for frame in Formats.empty_frames(16, iFrame, nlinks):
    pfile.write(frame)
    iFrame += 1
  # Now set the framevalid high (but still no tracks)
  for i in range(4):
    frame = valid_frame
    pfile.write(Formats.frame(frame, iFrame, nlinks))
    iFrame += 1
  # Now a frame with a single track on
  frame = valid_frame
  ilink = random.randint(0, 26)
  trk = Formats.random_trk()
  frame[ilink] = trk.toVHex()
  print trk.data(), ilink
  pfile.write(Formats.frame(frame, iFrame, nlinks))
  # Reset the frame
  frame[ilink] = valid_frame_no_trk
  iFrame += 1  
  # Now a few more framevalid high, no track, frames
  for i in range(4):
    frame = valid_frame
    pfile.write(Formats.frame(frame, iFrame, nlinks))
    iFrame += 1
  # Finally some more invalid frames
  for frame in Formats.empty_frames(16, iFrame, nlinks):
    pfile.write(frame)
    iFrame += 1

