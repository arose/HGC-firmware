import bitstring as bs
import random
import pandas
import numpy as np

# The z0 bins - number of bins and range in cm
bin_config = {'nbins':256, 'min':-15., 'max':15.}
# The pt^2 bins - number of bins and range in pt^2
# Since the tracking is for pt > 2 GeV, the smallest
# pt^2 is 4 GeV^2
#weight_config = {'nbins':2**16, 'min':0., 'max':2**18}
# The pt bins - number of bins and range in pt
weight_config = {'nbins':2**16, 'min':0., 'max':2**10}

def astype(bitarray, t):
  assert isinstance(bitarray, bs.BitArray), "bitarray must be a bitstring.BitArray"
  return getattr(bitarray, t)

class Track:
  # names of data fields, in order (lsb first)
  fields = ['z0', 'weight', 'datavalid'] # The data valid is outside of the 32b word
  # number of bits and representation for different fields
  lengths = [8, 16, 1]
  types = ['uint:8', 'uint:16', 'uint:1']

  def __init__(self, data=None, hexstring=None):
    if not data is None and not hexstring is None: 
      raise("Cannot initialise with field data and hexstring simultaneously")
    if not data is None:
      self._data = pandas.DataFrame(data, pandas.Index([0]))
    if not hexstring is None:
      hexdata = bs.BitArray(hexstring)
      d = {}
      for i in range(len(self.fields)):
        l = self.lengths[i]
        ll = sum(self.lengths[:i]) # field offset into 32b hex
        t = self.types[i].split(':')[0]
        d[self.fields[i]] = astype(hexdata[32-ll-l:32-ll], t)
      self._data = pandas.DataFrame(d, pandas.Index([0]))

  def pack(self):
    fmt_str = ''
    # pad to 32 bits
    if sum(self.lengths) < 32:
      fmt_str += 'uint:' + str(32 - sum(self.lengths)) + ', '
    for fmt in self.types[::-1]:
      fmt_str += fmt + ', '  
    data = np.array(self._data[self.fields].iloc[0]).tolist()[::-1] # reverse the fields
    # the pad field
    if sum(self.lengths) < 32:
      data = [0] + data
    return bs.pack(fmt_str, *data)

  def toVHex(self):
    return str(np.array(self._data['framevalid'])[0]) + 'v' + self.pack().hex

  def data(self):
    return self._data

def random_trk():
  ''' Make a track with random variables '''
  z0 = random.randint(0, 255)
  weight = random.randint(0, 2047)
  return Track({'z0':z0, 'weight':weight, 'datavalid':1, 'framevalid':1})

def header(nlinks):
  txt = 'Board VX\n'
  txt += ' Quad/Chan :'
  for i in range(nlinks):
    quadstr = '\tq{:02d}c{}'.format(i/4, i%4) 
    txt += quadstr
  txt += '\nLink :'
  for i in range(nlinks):
    txt += '\t{:02d}'.format(i)
  txt += '\n'
  return txt

def frame(vhexdata, iframe, nlinks):
  assert(len(vhexdata) == nlinks), "Data length doesn't match expected number of links"
  txt = 'Frame {:04d} :'.format(iframe)
  for d in vhexdata:
    txt += ' ' + d
  txt += '\n'
  return txt

def empty_frames(n, istart, nlinks):
  ''' Make n empty frames for nlinks with starting frame number istart '''
  empty_data = '0v00000000'
  empty_frame = [empty_data] * nlinks
  iframe = istart
  frames = []
  for i in range(n):
    frames.append(frame(empty_frame, iframe, nlinks))
    iframe += 1
  return frames

def z0cmToHWU(z):
  '''Convert a z coordinate in cm to Hardware Units'''
  # Floor / ceiling the value
  zmin = bin_config['min']
  zmax = bin_config['max']
  x = zmax if z > zmax else z
  x = zmin if z < zmin else x
  # Get the bin index
  x = round((x - zmin) / ((zmax - zmin) / (bin_config['nbins'] - 1)))
  return x

def pt2ToHWU(pt2):
  '''Convert a pt^2 in GeV^2 to Hardware Units'''
  # Floor / ceiling the value
  pt2min = weight_config['min']
  pt2max = weight_config['max']
  x = pt2max if pt2 > pt2max else pt2
  x = -pt2min if pt2 < -pt2min else x
  # Get the bin index
  x = round((x - pt2min) / ((pt2max - pt2min) / (weight_config['nbins'] - 1)))
  return x

def weightToHWU(weight):
  '''Convert a pt^2 in GeV^2 to Hardware Units'''
  # Floor / ceiling the value
  wmin = weight_config['min']
  wmax = weight_config['max']
  x = wmax if weight > wmax else weight
  x = -wmin if weight < -wmin else x
  # Get the bin index
  x = round((x - wmin) / ((wmax - wmin) / (weight_config['nbins'] - 1)))
  return x

def eventDataFrameToPatternFile(event, nlinks=27, nframes=108, doheader=True, startframe=0, weight='pt2'):
  '''Write a pattern file for an event dataframe.
  Tracks are assigned to links randomly
  '''
  # Pick a random link for each track
  import random
  random.seed(42)
  event['link'] = [random.randint(0, nlinks-1) for i in range(len(event))]
  # Push the tracks for each link into a list
  links = [] 
  for i in range(nlinks):
    trks = event[event['link'] == i]
    trks = [Track({'z0':z0cmToHWU(t['z0']), 'weight':pt2ToHWU(t[weight]), 'datavalid':1, 'framevalid':1}).toVHex() for i, t in trks.iterrows()]
    ntrks = len(trks)
    # Pad up to the frame length
    for j in range(nframes - ntrks):
      trks.append(Track({'z0':0, 'weight':0, 'datavalid':0, 'framevalid':1}).toVHex())
    links.append(trks)
  # Put empty frames on the remaining links
  empty_data = '1v00000000'
  empty_link = [empty_data] * nframes
  for i in range(72 - nlinks):
    links.append(empty_link)
    
  links = np.array(links)
  frames = links.transpose()
  frames = [frame(f, i+startframe+8, 72) for i, f in enumerate(frames)] # +8 because there will be 8 frames of header

  ret = []
  if doheader:
    ret = [header(72)]
  
  return ret + empty_frames(8, startframe, 72) + frames# + empty_frames(16, 8 + nframes, 72)

def writepfile(filename, events, weight='pt2'):
  doheader = True
  startframe = 0
  with open(filename, 'w') as pfile:
    for i, event in enumerate(events):
      if i > 0:
        doheader = False
        startframe += 108 + 8 # The data and inter-event gap
      for frame in eventDataFrameToPatternFile(event, doheader=doheader, startframe=startframe, weight=weight):
        pfile.write(frame)
    pfile.close()

def fwFastHisto(event, ret_all=False, weight='pt2'):
  ''' Return the index and weight of the maximum bin '''
  bins = range(bin_config['nbins'])
  z0 = [z0cmToHWU(z) for z in event['z0']]
  weight = [weightToHWU(p) for p in event[weight]]
  h, b = np.histogram(z0, bins=bins, weights=weight)
  if ret_all:
    return b, h
  return b[h.argmax()], h.max()


def writeSWReference(filename, events, weight='pt2'):
  ''' Write the Z0 and Weight for each event,
  as given by the fwFastHisto function, to a csv file '''
  z0, weight = zip(*[fwFastHisto(ev, weight=weight) for ev in events])
  ref = pandas.DataFrame({'Z0' : z0, 'Weight' : weight})
  ref.to_csv(filename)
