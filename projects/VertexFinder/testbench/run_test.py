from optparse import OptionParser
import yaml
from subprocess import call
import pandas
import sys
import os

def test(yamlConfig):
  print "Writing tcl file top_simulate.do"

  if not os.path.exists("modelsim_lib"):
    os.mkdir("modelsim_lib")

  if not os.path.exists("DebuggingOutput"):
    os.mkdir("DebuggingOutput")

  with open('top_simulate.do', 'w') as dofile:
    dofile.write("do {top_compile.do}\n")
    dofile.write('vsim -G/top/g1/MP7CaptureFileReaderInstance/FileName="{}" -GDebugInstance/FilePath="{}" -voptargs="+acc" -L Utilities -L Vertex -L Track -L VertexFinder -L Interfaces -L secureip -lib xil_defaultlib VertexFinder.top \n'.format(os.path.expanduser(yamlConfig['PatternFile']), './DebuggingOutput/'))
    dofile.write('set NumericStdNoWarnings 1\n')
    dofile.write('set StdArithNoWarnings 1\n')

    runtime = 1.2 * (yamlConfig['nEvents']+1) * yamlConfig['EventLength'] * yamlConfig['clkPeriod']
    dofile.write('run {} ns\n'.format(runtime)) 
    dofile.write('quit -f\n')
    dofile.close()

  # Run the simulation
  print "Running Modelsim"
  call(["vsim", "-batch", "-do", "top_simulate.do"])

def analyse(yamlConfig):
  # Compare the output
  print "Analysing output"
  fw = pandas.read_csv('DebuggingOutput/MaximaFinder.txt', header=1, delim_whitespace=True)
  sw = pandas.read_csv(yamlConfig['ReferenceFile'])
  # Implementations match if all events have same Z0 and Weight
  match = sum((sw['Z0'] == fw['Z0']) & (sw['Weight'] == fw['Weight'])) == len(sw)
  if match:
    print "Firmware and Software agree"
  else:
    print "Firmware and Software disagree"
  return match

 ## Config module
def parse_config(config_file) :

    print "Loading configuration from " + str(config_file)
    config = open(config_file, 'r')
    return yaml.load(config)

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option('-c','--config'   ,action='store',type='string', dest='config', default='sim_config.yml', help='configuration file')
  (options, args) = parser.parse_args()

  yamlConfig = parse_config(options.config)

  test(yamlConfig)
  success = int(not analyse(yamlConfig))
  sys.exit(success)

 
