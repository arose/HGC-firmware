# Set some paths
set SRCDIR "/home/sioni/p2fwk-work/src/HGC-firmware/projects"
set PRJDIR "/home/sioni/p2fwk-work/proj/VertexFinder/top"
# Reuseable elements
set RE "Common/firmware/hdl/ReuseableElements"
set VX "VertexFinder/firmware/hdl"

# Open the project
open_project top.xpr

# Add the vertex finder modules
add_files $SRCDIR/$VX/modules/LinksToTracks.vhd
add_files $SRCDIR/$VX/modules/TracksToVertices.vhd
add_files $SRCDIR/$VX/modules/VertexDistribution.vhd
add_files $SRCDIR/$VX/modules/Fanout.vhd
add_files $SRCDIR/$VX/modules/Histogram.vhd
add_files $SRCDIR/$VX/modules/MaximaFinder.vhd
add_files $SRCDIR/$VX/modules/VertexToLink.vhd

add_files $SRCDIR/$VX/components/PkgConstants.vhd
add_files $SRCDIR/$VX/components/PkgTrack.vhd
add_files $SRCDIR/$VX/components/PkgVertex.vhd

add_files $SRCDIR/$VX/top/payload.vhd
add_files $SRCDIR/$VX/top/VertexFinderProcessor.vhd
add_files $SRCDIR/$VX/top/ku115dc_decl_full.vhd

# Import the reusable elements into the project
# Do this once for each package that the ReuseableElements should
# be imported into, into a different directory each time
file mkdir RE_Vertex
add_files -force -copy_to RE_Vertex $SRCDIR/$RE/PkgArrayTypes.vhd
add_files -force -copy_to RE_Vertex $SRCDIR/$RE/DataPipe.vhd
add_files -force -copy_to RE_Vertex $SRCDIR/$RE/DataRam.vhd
add_files -force -copy_to RE_Vertex $SRCDIR/$RE/Debugger.vhd
add_files -force -copy_to RE_Vertex $SRCDIR/$RE/DistributionServer.vhd

set_property library Vertex [get_files -regexp {.*PkgVertex.vhd}]
set_property library Vertex [get_files -regexp {.*/RE_Vertex/PkgArrayTypes.vhd}]
set_property library Vertex [get_files -regexp {.*/RE_Vertex/DataPipe.vhd}]
set_property library Vertex [get_files -regexp {.*/RE_Vertex/DataRam.vhd}]
set_property library Vertex [get_files -regexp {.*/RE_Vertex/DistributionServer.vhd}]
set_property library Vertex [get_files -regexp {.*/RE_Vertex/Debugger.vhd}]

file mkdir RE
add_files -force -copy_to RE $SRCDIR/$RE/PkgUtilities.vhd
add_files -force -copy_to RE $SRCDIR/$RE/PkgDebug.vhd

set_property library Utilities [get_files -regexp {.*/RE/PkgUtilities.vhd}]
set_property library Utilities [get_files -regexp {.*/RE/PkgDebug.vhd}]

file mkdir RE_Track
add_files -force -copy_to RE_Track $SRCDIR/$RE/PkgArrayTypes.vhd
add_files -force -copy_to RE_Track $SRCDIR/$RE/DataPipe.vhd
#add_files -force -copy_to RE_Track $SRCDIR/$RE/Debugger.vhd

set_property library Track [get_files -regexp {.*PkgTrack.vhd}]
set_property library Track [get_files -regexp {.*/RE_Track/PkgArrayTypes.vhd}]
set_property library Track [get_files -regexp {.*/RE_Track/DataPipe.vhd}]
#set_property library Track [get_files -regexp {.*/RE_Track/Debugger.vhd}]

# Import the reusable elements into the project
#import_files -force $SRCDIR/$RE
set_property library VertexFinder [get_files -regexp {.*Histogram.vhd}]
set_property library VertexFinder [get_files -regexp {.*LinksToTracks.vhd}]
set_property library VertexFinder [get_files -regexp {.*MaximaFinder.vhd}]
set_property library VertexFinder [get_files -regexp {.*TracksToVertices.vhd}]
set_property library VertexFinder [get_files -regexp {.*VertexDistribution.vhd}]
set_property library VertexFinder [get_files -regexp {.*VertexFinderProcessor.vhd}]
set_property library VertexFinder [get_files -regexp {.*Fanout.vhd}]
set_property library VertexFinder [get_files -regexp {.*VertexToLink.vhd}]
set_property library VertexFinder [get_files -regexp {.*PkgConstants.vhd}]

# Add mp7_data_types to Interfaces
file mkdir TLI
add_files -force -copy_to TLI $SRCDIR/$RE/../TopLevelInterfaces/mp7_data_types.vhd
add_files -force -copy_to TLI $SRCDIR/$RE/../TopLevelInterfaces/PkgInterfaces.vhd
add_files -force -copy_to TLI $SRCDIR/$RE/PkgArrayTypes.vhd
add_files -force -copy_to TLI $SRCDIR/$RE/Debugger.vhd
set_property library Interfaces [get_files -regexp {.*/TLI/mp7_data_types.vhd}]
set_property library Interfaces [get_files -regexp {.*/TLI/PkgInterfaces.vhd}]
set_property library Interfaces [get_files -regexp {.*/TLI/PkgArrayTypes.vhd}]
set_property library Interfaces [get_files -regexp {.*/TLI/Debugger.vhd}]

# Make all VHDL files VHDL 2008
set_property file_type {VHDL 2008} [get_files -regexp {.*\.vhd}]
