-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library Track
-- .include ReuseableElements/PkgUtilities.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

LIBRARY Utilities;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE DataType IS
-- -------------------------------------------------------------------------

--  For now, a track has the same type as a vertex
  TYPE tData IS RECORD
  Z0           : UNSIGNED( 7 DOWNTO 0 );
  Weight       : UNSIGNED( 15 DOWNTO 0 );
-- Utility field, used for the key in the distribution server and the row index in the histogram
  SortKey      : INTEGER RANGE 0 TO 255;
-- -------------------------------------------------------------------------       
  MaximaOffset : INTEGER RANGE -7 TO 7;
-- -------------------------------------------------------------------------       
  DataValid    : BOOLEAN;
  FrameValid   : BOOLEAN;
END RECORD;

-- -------------------------------------------------------------------------       

-- A record to encode the bit location of each field of the tData record
-- when packaged into a std_logic_vector
  TYPE tFieldLocations IS RECORD
    z0l     : INTEGER;
    z0h     : INTEGER;
    Weightl : INTEGER;
    Weighth : INTEGER;
--SortKeyl : integer;
--SortKeyh : integer;
--MaximaOffsetl : integer;
--MaximaOffseth : integer;
--DV : integer;
--FV : integer;
  END RECORD;

  CONSTANT bitloc                      : tFieldLocations := ( 0 , 7 , 8 , 23 ); --, 24, 31);
  CONSTANT cNull                       : tData           := ( ( OTHERS => '0' ) , ( OTHERS => '0' ) , 0 , -7 , false , false );

  FUNCTION ToStdLogicVector( aData     : tData ) RETURN STD_LOGIC_VECTOR;
  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData;

  FUNCTION WriteHeader RETURN STRING;
  FUNCTION WriteData( aData : tData ) RETURN STRING;

-- ATTRIBUTE SIZE : NATURAL;
-- ATTRIBUTE SIZE OF tData : TYPE IS 72;  
-- -------------------------------------------------------------------------       

END DataType;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
PACKAGE BODY DataType IS

  FUNCTION ToStdLogicVector( aData : tData ) RETURN STD_LOGIC_VECTOR IS
    VARIABLE lRet                  : STD_LOGIC_VECTOR( 71 DOWNTO 0 ) := ( OTHERS => '0' );
  BEGIN
    lRet( bitloc.z0h DOWNTO bitloc.z0l )         := STD_LOGIC_VECTOR( aData.z0 );
    lRet( bitloc.weighth DOWNTO bitloc.weightl ) := STD_LOGIC_VECTOR( aData.Weight );
    RETURN lRet;
  END FUNCTION;

  FUNCTION ToDataType( aStdLogicVector : STD_LOGIC_VECTOR ) RETURN tData IS
    VARIABLE lRet                      : tData := cNull;
  BEGIN
    lRet.z0     := UNSIGNED( aStdLogicVector( bitloc.z0h DOWNTO bitloc.z0l ) );
    lRet.Weight := UNSIGNED( aStdLogicVector( bitloc.weighth DOWNTO bitloc.weightl ) );
    RETURN lRet;
  END FUNCTION;

  FUNCTION WriteHeader RETURN STRING IS
    VARIABLE aLine : LINE;
  BEGIN
    WRITE( aLine , STRING' ( "Z0" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "Weight" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "SortKey" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "MaximaOffset" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "FrameValid" ) , RIGHT , 15 );
    WRITE( aLine , STRING' ( "DataValid" ) , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteHeader;

  FUNCTION WriteData( aData : tData ) RETURN STRING IS
    VARIABLE aLine          : LINE;
  BEGIN
    WRITE( aLine , TO_INTEGER( aData.z0 ) , RIGHT , 15 );
    WRITE( aLine , TO_INTEGER( aData.Weight ) , RIGHT , 15 );
    WRITE( aLine , aData.SortKey , RIGHT , 15 );
    WRITE( aLine , aData.MaximaOffset , RIGHT , 15 );
    WRITE( aLine , aData.FrameValid , RIGHT , 15 );
    WRITE( aLine , aData.DataValid , RIGHT , 15 );
    RETURN aLine.ALL;
  END WriteData;



END DataType;
