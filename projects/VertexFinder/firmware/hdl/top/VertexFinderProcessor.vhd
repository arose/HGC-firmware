-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library VertexFinder
-- .include components/PkgTrack.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in Track
-- .include components/PkgVertex.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in Vertex
-- .include TopLevelInterfaces/mp7_data_types.vhd

-- .include modules/LinksToTracks.vhd
-- .include modules/TracksToVertices.vhd
-- .include modules/VertexDistribution.vhd
-- .include modules/Histogram.vhd
-- .include modules/ExponentialSmearingKernel.vhd
-- .include modules/MaximaFinder.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

-- synthesis translate_off
LIBRARY Interfaces;
USE Interfaces.mp7_data_types.ALL;
-- synthesis translate_on
-- synthesis read_comments_as_HDL on
--library xil_defaultlib;
--use xil_defaultlib.emp_data_types.all;
-- synthesis read_comments_as_HDL off

LIBRARY Track;
USE Track.DataType;
USE Track.ArrayTypes;

LIBRARY Vertex;
USE Vertex.DataType;
USE Vertex.ArrayTypes;

LIBRARY VertexFinder;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY VertexFinderProcessorTop IS
  PORT(
    clk             : IN STD_LOGIC; -- The algorithm clock
    LinksIn         : IN ldata( 71 DOWNTO 0 )                := ( OTHERS => LWORD_NULL );
    LinksOut        : OUT ldata( 71 DOWNTO 0 )               := ( OTHERS => LWORD_NULL );
-- Prevent all the logic being synthesized away when running standalone
    DebuggingOutput : OUT Vertex.ArrayTypes.Vector( 0 TO 0 ) := Vertex.ArrayTypes.NullVector( 1 )
  );
END VertexFinderProcessorTop;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF VertexFinderProcessorTop IS

  SUBTYPE tTrackPipe IS Track.ArrayTypes.VectorPipe( 0 TO 9 )( 0 TO 26 );
  CONSTANT NullTrackPipe : tTrackPipe := Track.ArrayTypes.NullVectorPipe( 10 , 27 );

  SUBTYPE tVertexPipe27 IS Vertex.ArrayTypes.VectorPipe( 0 TO 9 )( 0 TO 26 );
  CONSTANT NullVertexPipe27 : tVertexPipe27 := Vertex.ArrayTypes.NullVectorPipe( 10 , 27 );

  SUBTYPE tVertexPipe64 IS Vertex.ArrayTypes.VectorPipe( 0 TO 9 )( 0 TO 63 );
  CONSTANT NullVertexPipe64 : tVertexPipe64 := Vertex.ArrayTypes.NullVectorPipe( 10 , 64 );

  SUBTYPE tVertexPipe IS Vertex.ArrayTypes.VectorPipe( 0 TO 9 )( 0 TO 255 );
  CONSTANT NullVertexPipe : tVertexPipe := Vertex.ArrayTypes.NullVectorPipe( 10 , 256 );

  SUBTYPE tMaximaPipe IS Vertex.ArrayTypes.VectorPipe( 0 TO 9 )( 0 TO 0 );
  CONSTANT NullMaximaPipe      : tMaximaPipe   := Vertex.ArrayTypes.NullVectorPipe( 10 , 1 );


  SIGNAL InputPipe             : tTrackPipe    := NullTrackPipe;
  SIGNAL VertexPipe            : tVertexPipe27 := NullVertexPipe27;
  SIGNAL DistributedPipe       : tVertexPipe64 := NullVertexPipe64;
  SIGNAL DistributedFannedPipe : tVertexPipe   := NullVertexPipe;
  SIGNAL HistogramPipe         : tVertexPipe   := NullVertexPipe;
  SIGNAL SmearedVertexPipe     : tVertexPipe   := NullVertexPipe;
  SIGNAL MaximaPipe            : tMaximaPipe   := NullMaximaPipe;

BEGIN

-- -------------------------------------------------------------------------
  LinksToTracksInstance : ENTITY VertexFinder.LinksToTracks
  PORT MAP(
    clk          => clk ,
    linksIn      => LinksIn ,
    TrackPipeOut => InputPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  TracksToVerticesInstance : ENTITY VertexFinder.TracksToVertices
  PORT MAP(
    clk           => clk ,
    TrackPipeIn   => InputPipe ,
    VertexPipeOut => VertexPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  VertexDistributionInstance : ENTITY VertexFinder.VertexDistribution
  PORT MAP(
    clk           => clk ,
    VertexPipeIn  => VertexPipe ,
    VertexPipeOut => DistributedPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  VertexFanoutInstance : ENTITY VertexFinder.Fanout64_256
  PORT MAP(
    clk           => clk ,
    VertexPipeIn  => DistributedPipe ,
    VertexPipeOut => DistributedFannedPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  HistogramInstance : ENTITY VertexFinder.Histogram
  PORT MAP(
    clk           => clk ,
    VertexPipeIn  => DistributedFannedPipe ,
    VertexPipeOut => HistogramPipe
  );
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
--  ExponentialSmearingKernelInstance : ENTITY VertexFinder.ExponentialSmearingKernel
--  PORT MAP(
--    clk         => clk ,
--    VertexPipeIn  => HistogramPipe ,
--    VertexPipeOut => SmearedVertexPipe
--  );
SmearedVertexPipe <= HistogramPipe;
-- -------------------------------------------------------------------------
-- -------------------------------------------------------------------------
  MaximaFinderInstance : ENTITY VertexFinder.MaximaFinder
  PORT MAP(
    clk           => clk ,
    VertexPipeIn  => SmearedVertexPipe ,
    VertexPipeOut => MaximaPipe
  );
-- -------------------------------------------------------------------------

  LinksOutInstance : ENTITY VertexFinder.VertexToLink
  PORT MAP(
    clk => clk,
    VertexPipeIn => MaximaPipe,
    linksOut => LinksOut
  );

-- MAKE SURE THIS IS THE LAST ENTRY IN THE CHAIN FOR STANDALONE SYNTHESIS
  DebuggingOutput <= MaximaPipe( 0 );

END rtl;
