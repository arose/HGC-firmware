-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library VertexFinder
-- .include components/PkgTrack.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in Track
-- .include ReuseableElements/DataPipe.vhd in Track
-- .include ReuseableElements/Debugger.vhd in Track
-- .include ReuseableElements/PkgDebug.vhd
-- .include ReuseableElements/PkgUtilities.vhd
-- .include TopLevelInterfaces/mp7_data_types.vhd

-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

-- synthesis translate_off
LIBRARY Interfaces;
USE Interfaces.mp7_data_types.ALL;
-- synthesis translate_on
-- synthesis read_comments_as_HDL on
--library xil_defaultlib;
--use xil_defaultlib.emp_data_types.all;
-- synthesis read_comments_as_HDL off

LIBRARY Track;
USE Track.DataType.ALL;
USE Track.ArrayTypes.ALL;

LIBRARY Utilities;
USE Utilities.debugging.ALL;
USE Utilities.Utilities.ALL;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY LinksToTracks IS
  PORT(
    clk          : IN STD_LOGIC            := '0'; -- The algorithm clock
    linksIn      : IN ldata( 71 DOWNTO 0 ) := ( OTHERS => LWORD_NULL );
    TrackPipeOut : OUT VectorPipe
  );
END LinksToTracks;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF LinksToTracks IS
  SIGNAL Output : Vector( 0 TO 26 ) := NullVector( 27 );

BEGIN

-- -------------------------------------------------------------------------
  g1 : FOR i IN 0 TO 26 GENERATE
  BEGIN

    PROCESS( clk )
      VARIABLE LocalLayer , LutOutput , LocalPhi : INTEGER;
    BEGIN
      IF RISING_EDGE( clk ) THEN

        Output( i ) .z0         <= UNSIGNED( linksIn( i ) .data( bitloc.z0h DOWNTO bitloc.z0l ) );
        Output( i ) .Weight     <= UNSIGNED( linksIn( i ) .data( bitloc.weighth DOWNTO bitloc.weightl ) );
        Output( i ) .SortKey    <= TO_INTEGER( UNSIGNED( linksIn( i ) .data( bitloc.z0h DOWNTO bitloc.z0l ) ) );
        Output( i ) .DataValid  <= to_boolean( linksIn( i ) .data( 24 ) );
        Output( i ) .FrameValid <= to_boolean( linksIn( i ) .valid );

      END IF;
    END PROCESS;

  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline  
  OutputPipeInstance : ENTITY Track.DataPipe
  PORT MAP( clk , Output , TrackPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  -- DebugInstance : ENTITY Track.Debug
  --GENERIC MAP( "LinksToTrack" )
  --PORT MAP( clk , Output ) ;
-- -------------------------------------------------------------------------

END rtl;
