-- #########################################################################
-- #########################################################################
-- ###                                                                   ###
-- ###   Use of this code, whether in its current form or modified,      ###
-- ###   implies that you consent to the terms and conditions, namely:   ###
-- ###    - You acknowledge my contribution                              ###
-- ###    - This copyright notification remains intact                   ###
-- ###                                                                   ###
-- ###   Many thanks,                                                    ###
-- ###     Dr. Andrew W. Rose, Imperial College London, 2018             ###
-- ###                                                                   ###
-- #########################################################################
-- #########################################################################

-- .library VertexFinder

-- .include components/PkgTrack.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in Track

-- .include components/PkgVertex.vhd
-- .include ReuseableElements/PkgArrayTypes.vhd in Vertex
-- .include ReuseableElements/DataPipe.vhd in Vertex
-- .include ReuseableElements/Debugger.vhd in Vertex


-- -------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_MISC.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY Track;
USE Track.DataType;
USE Track.ArrayTypes;

LIBRARY Vertex;
USE Vertex.DataType;
USE Vertex.ArrayTypes;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ENTITY TracksToVertices IS
  GENERIC(
    PipeOffset : INTEGER := 0
  );
  PORT(
    clk           : IN STD_LOGIC := '0'; -- The algorithm clock
    TrackPipeIn   : IN Track.ArrayTypes.VectorPipe;
    VertexPipeOut : OUT Vertex.ArrayTypes.VectorPipe
  );
END TracksToVertices;
-- -------------------------------------------------------------------------


-- -------------------------------------------------------------------------
ARCHITECTURE rtl OF TracksToVertices IS
  SIGNAL Output : Vertex.ArrayTypes.Vector( 0 TO 26 ) := Vertex.ArrayTypes.NullVector( 27 );
BEGIN

-- -------------------------------------------------------------------------
  g1              : FOR i IN 0 TO 26 GENERATE
    SIGNAL lTrack : Track.DataType.tData := Track.DataType.cNull;
  BEGIN

    lTrack <= TrackPipeIn( PipeOffset )( i );

    PROCESS( clk )
    BEGIN
      IF RISING_EDGE( clk ) THEN
        Output( i ) .z0         <= lTrack.z0;
        Output( i ) .Weight     <= lTrack.Weight;
        Output( i ) .SortKey    <= TO_INTEGER( lTrack.z0 );
        Output( i ) .DataValid  <= lTrack.DataValid;
        Output( i ) .FrameValid <= lTrack.FrameValid;
      END IF;
    END PROCESS;
  END GENERATE;
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Store the result in a pipeline
  OutputPipeInstance : ENTITY Vertex.DataPipe
  PORT MAP( clk , Output , VertexPipeOut );
-- -------------------------------------------------------------------------

-- -------------------------------------------------------------------------
-- Write the debug information to file
  --DebugInstance : ENTITY Vertex.Debug
  --GENERIC MAP( "TracksToVertices" )
  --PORT MAP( clk , Output ) ;
-- -------------------------------------------------------------------------

END rtl;
