library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- synthesis translate_off
LIBRARY Interfaces;
USE Interfaces.mp7_data_types.ALL;
-- synthesis translate_on
-- synthesis read_comments_as_HDL on
--library xil_defaultlib;
--use xil_defaultlib.emp_data_types.all;
-- synthesis read_comments_as_HDL off

library Interfaces;
use Interfaces.DataType;
use Interfaces.ArrayTypes;

library Vertex;
use Vertex.DataType.all;
use Vertex.ArrayTypes;

library Utilities;
use Utilities.Utilities.all;

entity VertexToLink is
port(
  clk : in std_logic := '0';
  VertexPipeIn : in Vertex.ArrayTypes.VectorPipe;
  linksOut : out ldata(71 downto 0) := (others => LWORD_NULL)  
);
end VertexToLink;

architecture Behavioral of VertexToLink is

  -- A second copy of the links in the Debug compatible format
  signal linksOutInt : Interfaces.ArrayTypes.Vector(0 downto 0) := Interfaces.ArrayTypes.NullVector(1);
  
begin

-- If the data is valid then first send a header (data valid, empty data)
-- On the next cycle send the valid data
process(clk)
begin
  if VertexPipeIn(0)(0).DataValid then
    linksOut(0).data(bitloc.z0h downto bitloc.z0l) <= std_logic_vector(VertexPipeIn(0)(0).Z0);
    linksOut(0).data(bitloc.weighth downto bitloc.weightl) <= std_logic_vector(VertexPipeIn(0)(0).Weight);
    linksOut(0).data( 24 ) <= to_std_logic(VertexPipeIn(0)(0).DataValid);
    linksOut(0).valid <= '1';
    linksOut(0).start <= '0';
    linksOut(0).strobe <= '1';
  else
    linksOut(0).data <= (others => '0');
    linksOut(0).start <= '0';
    linksOut(0).strobe <= '1';
    linksOut(0).valid <= '0';
  end if;
end process;

-- synthesis translate_off
linksOutInt(0).data <= linksOut(0);
linksOutInt(0).DataValid <= to_boolean(linksOut(0).valid);
-- synthesis translate_on

-- Write the debug information to file
DebugInstance : ENTITY Interfaces.Debug
GENERIC MAP( "LinksOut" )
PORT MAP( clk, linksOutInt );

end Behavioral;
