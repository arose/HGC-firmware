library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

library utilities;
use utilities.utilities.all;

package DataType is

  type tData is record
    ID : integer range 0 to 255;
    SortKey : integer range 0 to 255;
    DataValid : boolean;
  end record;

  constant cNull : tData := (0, 0, false);

  function "<" (x, y : tData) return boolean;
  function ">" (x, y : tData) return boolean;
  function WriteHeader return string;
  function WriteData(x : tData) return string;
end package;

package body DataType is

  function "<" (x, y : tData) return boolean is
    variable ret : boolean;
  begin
    if x.SortKey < y.SortKey then
      ret := true;
    else
      ret := false;
    end if; 
    return ret;
  end "<";

  function ">" (x, y : tData) return boolean is
    variable ret : boolean;
  begin
    if x.SortKey > y.SortKey then
      ret := true;
    else
      ret := false;
    end if; 
    return ret;
  end ">";

  function WriteHeader return string is
    variable aLine : line;
  begin
    write(aLine, string'("ID"), right, 15);
    write(aLine, string'("SortKey"), right, 15);
    write(aLine, string'("DataValid"), right, 15);
    return aLine.all;
  end WriteHeader;

  function WriteData(x : tData) return string is
    variable aLine : line;
  begin
    write(aLine, x.ID, right, 15);
    write(aLine, x.SortKey, right,  15);
    write(aLine, x.DataValid, right, 15);
    return aLine.all;
  end WriteData;
end DataType;
