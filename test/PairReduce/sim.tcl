
vlib modelsim_lib/work
vlib modelsim_lib/msim
vlib modelsim_lib/msim/Utilities
vlib modelsim_lib/msim/Example
vlib modelsim_lib/msim/xil_defaultlib

vmap Utilities modelsim_lib/msim/Utilities
vmap Example modelsim_lib/msim/Example

vcom -64 -2008 -work Utilities \
"../../projects/Common/firmware/hdl/ReuseableElements/PkgUtilities.vhd" \
"../../projects/Common/firmware/hdl/ReuseableElements/PkgDebug.vhd" \

vcom -64 -2008 -work Example \
"PkgExample.vhd" \
"../../projects/Common/firmware/hdl/ReuseableElements/PkgArrayTypes.vhd" \
"../../projects/Common/firmware/hdl/ReuseableElements/Debugger.vhd" \
"../../projects/Common/firmware/hdl/ReuseableElements/PairReduceMax.vhd" \

vcom -64 -2008 -work xil_defaultlib \
"testbench.vhd" \

vsim -voptargs="+acc" -L Utilities -L Example -lib xil_defaultlib xil_defaultlib.top
set NumericStdNoWarnings 1
set StdArithNoWarnings 1

run 500 ns
quit -f
