library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library Example;
use Example.DataType.all;
use Example.ArrayTypes.all;

library Utilities;
use Utilities.Utilities.all;
use Utilities.Debugging.all;

entity top is
end top;

architecture rtl of top is

  constant inSize : integer := 16;
  signal clk : std_logic := '0';
  signal dataIn : Vector(0 to inSize - 1) := NullVector(inSize);
  signal dataOut : Vector(0 to 0) := NullVector(1);
begin
  clk <= not clk after 2.5 ns;

  Stimulus : 
  process(clk)
    variable randomSeed : positive := 12345;
    variable randomVector : std_logic_vector(7 downto 0);
  begin
    if rising_edge(clk) then
      simulationClockCounter <= simulationClockCounter + 1;
      for i in 0 to inSize - 1 loop
        set_random_var(randomSeed, randomSeed, randomVector);
        dataIn(i).ID <= i;
        dataIn(i).SortKey <= to_integer(unsigned(randomVector));
        dataIn(i).DataValid <= true;
      end loop; 
    end if;
  end process;

  Reduce:
  entity Example.PairReduceMax
  port map(clk, dataIn, dataOut);

  DebugIn:
  entity Example.Debug
  generic map("ReduceIn", "./")
  port map(clk, dataIn);

  DebugOut:
  entity Example.Debug
  generic map("ReduceOut", "./")
  port map(clk, dataOut);
  
end rtl;
